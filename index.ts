#!/usr/bin/env node

/**
 * index.ts
 *
 * Handles all cli commands
 */
'use strict'

import { join } from 'path';
import { program } from 'commander';
import { readFileSync } from 'fs-extra';

require('./helpers/hbs');


/* ---------------------------- Version asigning ---------------------------- */
const CLI_VERSION: string = JSON.parse(readFileSync(join(__dirname, '../package.json'), 'utf-8')).version;
program.version(CLI_VERSION, '-v, --vers', 'output the current version');

/* -------------------------------------------------------------------------- */
/*                           New project generating                           */
/* -------------------------------------------------------------------------- */

import { NewProjectCommand } from './commands/new/new';

program
  .command('new')
  .alias('n')
  .description('Generates new handy project')
  .action(() => {

    let CommandHandler = new NewProjectCommand();
    CommandHandler.command();

  })

/* -------------------------------------------------------------------------- */
/*                             Development running                            */
/* -------------------------------------------------------------------------- */

import { RunCommand } from './commands/run/run';

program
  .command('run')
  .alias('r')
  .description('Runs Handy project in development base on selected option')
  .action(() => {

    let CommandHandler = new RunCommand();
    CommandHandler.command();

  })

/* -------------------------------------------------------------------------- */
/*                             Development running                            */
/* -------------------------------------------------------------------------- */

import { GenerateCommand } from './commands/generate/generate';

program
  .command('generate')
  .alias('g')
  .option('--v --verify', 'Shows files to generate and asks for verifications before generating. Simmilar to ng "--dry-run"')
  .description('Generates handy project files, like modules, models, services, validators....')
  .action(params => {

    let { verify = false } = params;

    let CommandHandler = new GenerateCommand(verify);
    CommandHandler.command();

  })


/* -------------------------------------------------------------------------- */
/*                             Login                                          */
/* -------------------------------------------------------------------------- */

import { ConfigHelper } from './helpers/config';

program
  .command('login')
  .description('Log to handy hub')
  .action(() => {


    ConfigHelper.hubLogin();

  })

/* -------------------------------------------------------------------------- */
/*                             Logout                                         */
/* -------------------------------------------------------------------------- */

program
  .command('logout')
  .description('Log out from handy hub')
  .action(() => {

    ConfigHelper.hubLogout();

  })

/* -------------------------------------------------------------------------- */
/*                             Deploy                                         */
/* -------------------------------------------------------------------------- */

import { DeployCommand } from './commands/deploy/deploy';

program
  .command('deploy')
  .alias('d')
  .description('Deploys app to server')
  .action(() => {


    let CommandHandler = new DeployCommand();
    CommandHandler.command();

  })


/* -------------------------------------------------------------------------- */
/*                             Update                                         */
/* -------------------------------------------------------------------------- */

import { UpdateProjectCommand } from './commands/update-project/update';
import { execSync } from 'child_process';

program
  .command('update-project')
  .alias('u-p')
  .description('Updates handy project to latest version')
  .action(() => {


    let CommandHandler = new UpdateProjectCommand();
    CommandHandler.command();

  })

/* -------------------------------------------------------------------------- */
/*                             Update -cli                                         */
/* -------------------------------------------------------------------------- */

import { UpdateCliCommand } from './commands/update-cli/update';

program
  .command('update')
  .alias('u')
  .description('Updates handy cli')
  .action(() => {

    let CommandHandler = new UpdateCliCommand();
    CommandHandler.command();

  })

import { SaveExactDepsCommand } from './commands/save-exact-dependencies/save-exact-deps';

program
  .command('save-exact-deps')
  .description('Pulls installed dependencies versions and updates the package.json accordingly')
  .action(() => {

    let CommandHandler = new SaveExactDepsCommand();
    CommandHandler.command();

  })


/* -------------------------- Listening for command ------------------------- */
program.parse(process.argv);