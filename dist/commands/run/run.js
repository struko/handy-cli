"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RunCommand = void 0;
var child_process_1 = require("child_process");
var inquirer_1 = require("inquirer");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var RunCommand = /** @class */ (function () {
    function RunCommand() {
        this._questions = [
            {
                message: 'What to run',
                type: 'list',
                name: 'whatToRun',
                choices: ['Server', 'Client', 'Both'],
                default: 'Both'
            }
        ];
    }
    RunCommand.prototype.command = function () {
        var _this = this;
        if (!config_1.ConfigHelper.isInHandyProject) {
            logger_1.Logger.notHandyProject();
            return;
        }
        if (config_1.ConfigHelper.projectEnv !== 'dev') {
            logger_1.Logger.error('Command "handy run" is supported on development enviroment only');
            return;
        }
        this._prompt()
            .then(function () {
            var clientCommand = "cd " + config_1.ConfigHelper.projectRootDir + " && concurrently -r \"npm run watch-ng\" \"npm run serve-build\"";
            var serverCommand = "cd " + config_1.ConfigHelper.projectRootDir + " && nodemon --exec npm run " + ((config_1.ConfigHelper.nodeMainVersion > 10) ? 'dev-start-12' : 'dev-start');
            var both = "cd " + config_1.ConfigHelper.projectRootDir + " && npm run " + ((config_1.ConfigHelper.nodeMainVersion > 10) ? 'dev-12' : 'dev');
            switch (_this._answers.whatToRun) {
                case 'Client':
                    logger_1.Logger.info('Building and running client');
                    _this._execute(clientCommand);
                    break;
                case 'Server':
                    logger_1.Logger.info('Building and running server');
                    _this._execute(serverCommand);
                    break;
                default:
                    logger_1.Logger.info('Building and running server with client');
                    _this._execute(both);
                    // this._execute(clientCommand);
                    break;
            }
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    RunCommand.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._questions)
                .then(function (answers) {
                _this._answers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    RunCommand.prototype._execute = function (command) {
        var execution = child_process_1.exec(command);
        // @ts-ignore
        execution.stdout.on('data', function (data) {
            console.log(data.toString());
        });
        // @ts-ignore
        execution.stderr.on('data', function (data) {
            logger_1.Logger.error(data.toString());
        });
        // @ts-ignore
        execution.on('exit', function (code) {
            logger_1.Logger.info('Handy client process exited with code ' + code);
            return Promise.resolve();
        });
    };
    return RunCommand;
}());
exports.RunCommand = RunCommand;
