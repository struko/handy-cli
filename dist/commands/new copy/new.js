"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NewProjectCommand = void 0;
var child_process_1 = require("child_process");
var inquirer_1 = require("inquirer");
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var NewProjectCommand = /** @class */ (function () {
    function NewProjectCommand() {
        this._baseRepo = 'https://bitbucket.org/struko/handy-base.git';
        this._tempPath = path_1.join(__dirname, 'tmp');
        this._tempHandyJsonPath = path_1.join(this._tempPath, 'handy.json');
        this._tempPackageJsonPath = path_1.join(this._tempPath, 'package.json');
        this._cwd = path_1.join(process.cwd() + '/');
        this._questions = [
            {
                message: 'Project name',
                type: 'input',
                name: 'projName',
            },
            {
                message: 'Super Admin email',
                type: 'input',
                name: 'adminEmail',
                default: (config_1.ConfigHelper.isLoggedIn()) ? config_1.ConfigHelper.getHubCreds().email : 'devs@handyapps.dev'
            },
            {
                message: 'Author',
                type: 'input',
                name: 'author',
                default: 'HandyApps'
            },
            {
                message: 'Description',
                type: 'input',
                name: 'description',
                default: ''
            },
            {
                message: 'Install dependencies',
                type: 'confirm',
                name: 'installDependencies',
                default: true
            }
        ];
    }
    NewProjectCommand.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            return _this._cloneBase();
        })
            .then(function () {
            _this._updateHandyJsonFile();
            _this._updatePackageJsonFile();
            return Promise.resolve();
        })
            .then(function () {
            return _this._copyProjectFiles();
        })
            .then(function () {
            return _this._clearGit();
        })
            .then(function () {
            return _this._initGit();
        })
            .then(function () {
            return _this._installDependencies();
        })
            .then(function () {
            logger_1.Logger.success('New project generated');
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    NewProjectCommand.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._questions)
                .then(function (answers) {
                _this._answers = answers;
                if (!answers.projName) {
                    return Promise.reject('Invalid project name');
                }
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    NewProjectCommand.prototype._cloneBase = function () {
        var _this = this;
        var spinner = logger_1.Logger.spinner('Cloning Handy base...');
        return new Promise(function (resolve, reject) {
            spinner.start();
            var command = "git clone " + _this._baseRepo + " " + _this._tempPath;
            if (fs_extra_1.existsSync(_this._tempHandyJsonPath)) {
                command = "cd " + _this._tempPath + " && git stash && git pull";
            }
            child_process_1.exec(command, function (err, stdout, stderr) {
                spinner.stop(true);
                if (err) {
                    reject(err);
                    return;
                }
                logger_1.Logger.success('Handy base cloned');
                return resolve();
            });
        });
    };
    NewProjectCommand.prototype._copyProjectFiles = function () {
        if (fs_extra_1.existsSync(this._getFinalProjectPath())) {
            return Promise.reject('This project already exists in current folder');
        }
        return fs_extra_1.copy(this._tempPath, this._getFinalProjectPath(), { recursive: true });
    };
    NewProjectCommand.prototype._updateHandyJsonFile = function () {
        var baseSettings = JSON.parse(fs_extra_1.readFileSync(this._tempHandyJsonPath, { encoding: 'utf-8' }));
        baseSettings.projectName.val = this._capitalizedProjectName();
        baseSettings.projectName.stagVal = this._capitalizedProjectName() + ' - Staging';
        baseSettings.projectName.devVal = this._capitalizedProjectName() + ' - Development';
        baseSettings.domain.devVal = 'localhost';
        baseSettings.domain.val = '';
        baseSettings.domain.stagVal = '';
        baseSettings.superAdminEmail.val = this._answers.adminEmail;
        baseSettings.errors.val.emailTo = this._answers.adminEmail;
        baseSettings.deviceIdCookieHash.val = this._randomHash();
        baseSettings.secret.val = this._randomHash();
        baseSettings.secret.stagVal = this._randomHash();
        baseSettings.secret.devVal = this._randomHash();
        baseSettings.cookieSecret.val = this._randomHash();
        baseSettings.cookieSecret.stagVal = this._randomHash();
        baseSettings.cookieSecret.devVal = this._randomHash();
        baseSettings.userPassSufixFlag.val = this._randomHash();
        baseSettings.userPassSufixFlag.stagVal = this._randomHash();
        baseSettings.userPassSufixFlag.devVal = this._randomHash();
        var tokensList = Object.keys(baseSettings.jwt.val.types);
        var tokensLen = tokensList.length;
        for (var i = 0; i < tokensLen; i++) {
            var tokenName = tokensList[i];
            baseSettings.jwt.val.types[tokenName].secret = this._randomHash();
        }
        fs_extra_1.writeFileSync(this._tempHandyJsonPath, JSON.stringify(baseSettings, null, 2));
    };
    NewProjectCommand.prototype._updatePackageJsonFile = function () {
        var packageJson = JSON.parse(fs_extra_1.readFileSync(this._tempPackageJsonPath, { encoding: 'utf-8' }));
        packageJson.name = this._answers.projName.toLowerCase().replace(/\s/g, '_');
        packageJson.author = this._answers.author;
        packageJson.repository = {};
        packageJson.description = this._answers.description;
        packageJson.homepage = '';
        fs_extra_1.writeFileSync(this._tempPackageJsonPath, JSON.stringify(packageJson, null, 2));
    };
    NewProjectCommand.prototype._randomHash = function (len) {
        if (len === void 0) { len = 8; }
        return Math.random().toString(36).substring(2, 2 + len);
    };
    NewProjectCommand.prototype._clearGit = function () {
        var _this = this;
        var spinner = logger_1.Logger.spinner('Removing base repo...');
        return new Promise(function (resolve, reject) {
            spinner.start();
            var command = "cd " + _this._getFinalProjectPath() + " && rm -rf .git";
            child_process_1.exec(command, function (err, stdout, stderr) {
                spinner.stop(true);
                if (err) {
                    reject(err);
                    return;
                }
                logger_1.Logger.success('Handy base repository removed');
                return resolve();
            });
        });
    };
    NewProjectCommand.prototype._initGit = function () {
        var _this = this;
        var spinner = logger_1.Logger.spinner('Initializing new project repository...');
        return new Promise(function (resolve, reject) {
            spinner.start();
            var command = "cd " + _this._getFinalProjectPath() + " && git init";
            child_process_1.exec(command, function (err, stdout, stderr) {
                spinner.stop(true);
                if (err) {
                    reject(err);
                    return;
                }
                logger_1.Logger.success('New project repository initialized');
                return resolve();
            });
        });
    };
    NewProjectCommand.prototype._installDependencies = function () {
        var _this = this;
        if (!this._answers.installDependencies) {
            logger_1.Logger.info('Skipping dependencies install');
            return Promise.resolve();
        }
        logger_1.Logger.info('Installing dependencies ...');
        return new Promise(function (resolve, reject) {
            var command = "cd " + _this._getFinalProjectPath() + " && npm run install-all";
            var child = child_process_1.exec(command);
            // @ts-ignore
            child.stdout.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.stderr.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.on('exit', function (code) {
                logger_1.Logger.info('Finished installing dependencies');
                return resolve();
            });
        });
    };
    NewProjectCommand.prototype._getFinalProjectPath = function () {
        return path_1.join(this._cwd, this._answers.projName);
    };
    NewProjectCommand.prototype._capitalizedProjectName = function () {
        return this._answers.projName.charAt(0).toUpperCase() + this._answers.projName.slice(1);
    };
    return NewProjectCommand;
}());
exports.NewProjectCommand = NewProjectCommand;
