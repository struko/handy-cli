"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.srcPathsList = exports.srcPaths = void 0;
exports.srcPaths = {
    serverCore: 'src/server/handy/core',
    serverTypes: 'src/server/handy/types',
    clientCore: 'src/client/web/src/app/handy/core',
    chartsModule: 'src/client/web/src/app/handy/modules/handy-charts',
    trackingModule: 'src/client/web/src/app/handy/modules/handy-tracking',
    formsModule: 'src/client/web/src/app/handy/modules/handy-form',
    dataTableModule: 'src/client/web/src/app/handy/modules/handy-table',
    btns: 'src/client/web/src/app/modules/shared/components/buttons',
    confirmClickDialog: 'src/client/web/src/app/modules/shared/components/confirm-click-dialog',
    fileThumb: 'src/client/web/src/app/modules/shared/components/file-thumb',
    formActionsBar: 'src/client/web/src/app/modules/shared/components/form-actions-bar',
    globalLoader: 'src/client/web/src/app/modules/shared/components/global-loader',
    handyExpander: 'src/client/web/src/app/modules/shared/components/handy-expander',
    handyIcon: 'src/client/web/src/app/modules/shared/components/handy-icon',
    handyNavlayot: 'src/client/web/src/app/modules/shared/components/handy-nav-layout',
    notifications: 'src/client/web/src/app/modules/shared/components/notifications',
    trackingClientService: 'src/client/web/src/app/handy/services/handy-ng-tracking.service.ts',
    trackingServerNonDefaultService: 'src/server/services/handy-services/handy-tracking/handy-tracking.service.ts',
};
exports.srcPathsList = Object.keys(exports.srcPaths);
