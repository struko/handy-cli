"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateProjectCommand = void 0;
var child_process_1 = require("child_process");
var inquirer_1 = require("inquirer");
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var paths_1 = require("./paths");
var UpdateProjectCommand = /** @class */ (function () {
    function UpdateProjectCommand() {
        this._baseRepo = 'https://bitbucket.org/struko/handy-base.git';
        this._tempPath = path_1.join(__dirname, 'tmp');
        this._tempHandyJsonPath = path_1.join(this._tempPath, 'handy.json');
        this._questions = [
            {
                message: 'What would you like to update',
                type: 'checkbox',
                name: 'toUpdate',
                choices: [
                    {
                        name: 'Server Core',
                        value: 'serverCore',
                        checked: true,
                    },
                    {
                        name: 'Server Types',
                        value: 'serverTypes',
                        checked: true,
                    },
                    {
                        name: 'Client Core',
                        value: 'clientCore',
                        checked: true,
                    },
                    {
                        name: 'Charts module (client)',
                        value: 'chartsModule',
                        checked: true,
                    },
                    {
                        name: 'Forms module (client)',
                        value: 'formsModule',
                        checked: true,
                    },
                    {
                        name: 'DataTale module (client)',
                        value: 'dataTableModule',
                        checked: true,
                    },
                    {
                        name: 'Tracking module (client)',
                        value: 'trackingModule',
                        checked: true,
                    },
                ]
            }
        ];
        this._questionsComponents = [
            {
                message: 'Which components would you like to update',
                type: 'checkbox',
                name: 'toUpdate',
                choices: [
                    {
                        name: 'Buttons',
                        value: 'btns',
                        checked: true,
                    },
                    {
                        name: 'Confirm click dialog',
                        value: 'confirmClickDialog',
                        checked: true,
                    },
                    {
                        name: 'File thumb',
                        value: 'fileThumb',
                        checked: true,
                    },
                    {
                        name: 'Form actions bar',
                        value: 'formActionsBar',
                        checked: true,
                    },
                    {
                        name: 'Global loader',
                        value: 'globalLoader',
                        checked: true,
                    },
                    {
                        name: 'Handy expander',
                        value: 'handyExpander',
                        checked: true,
                    },
                    {
                        name: 'Handy icon',
                        value: 'handyIcon',
                        checked: true,
                    },
                    {
                        name: 'Notifications',
                        value: 'notifications',
                        checked: true,
                    },
                ]
            }
        ];
        this._checkedUpdates = [];
    }
    UpdateProjectCommand.prototype.command = function () {
        var _this = this;
        if (!config_1.ConfigHelper.isInHandyProject) {
            logger_1.Logger.error("You are not in Handy project");
            return;
        }
        this._prompt()
            .then(function () {
            return _this._prompt('components');
        })
            .then(function () {
            _this._checkedUpdates = __spreadArrays(_this._answers.toUpdate, _this._answersComponents.toUpdate);
            if (_this._checkedUpdates.length < 1) {
                return Promise.reject('Nothing selected to update');
            }
            return _this._commitGit();
        })
            .then(function () {
            return _this._cloneBase();
        })
            .then(function () {
            return _this._runUpdate();
        })
            .then(function () {
            // Just to add empty space in log
            console.log();
            logger_1.Logger.success('Project updated');
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    UpdateProjectCommand.prototype._prompt = function (type) {
        var _this = this;
        if (type === void 0) { type = 'base'; }
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(type === 'base' ? _this._questions : _this._questionsComponents)
                .then(function (answers) {
                if (type === 'base') {
                    _this._answers = answers;
                }
                else {
                    _this._answersComponents = answers;
                }
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    UpdateProjectCommand.prototype._cloneBase = function () {
        var _this = this;
        var spinner = logger_1.Logger.spinner('Cloning Handy base...');
        return new Promise(function (resolve, reject) {
            spinner.start();
            var command = "git clone " + _this._baseRepo + " " + _this._tempPath;
            if (fs_extra_1.existsSync(_this._tempHandyJsonPath)) {
                command = "cd " + _this._tempPath + " && git stash && git pull";
            }
            child_process_1.exec(command, function (err, stdout, stderr) {
                spinner.stop(true);
                if (err) {
                    reject(err);
                    return;
                }
                logger_1.Logger.success('Handy base cloned');
                return resolve();
            });
        });
    };
    UpdateProjectCommand.prototype._commitGit = function () {
        var spinner = logger_1.Logger.spinner('Making repo "Pre HANDY update" comit...');
        var notesFilePath = path_1.join(config_1.ConfigHelper.projectRootDir, 'notes');
        var notesContent = fs_extra_1.readFileSync(notesFilePath, { encoding: 'utf-8' }) + ("\n\nProject updated via CLI at " + new Date().toUTCString());
        fs_extra_1.writeFileSync(notesFilePath, notesContent, { encoding: 'utf-8' });
        return new Promise(function (resolve, reject) {
            spinner.start();
            var command = "cd " + config_1.ConfigHelper.projectRootDir + " && git add . && git commit -m \"Pre HANDY update\"";
            child_process_1.exec(command, function (err, stdout, stderr) {
                spinner.stop(true);
                if (err) {
                    reject(err);
                    return;
                }
                logger_1.Logger.success("\"Pre HANDY update\" comitted");
                return resolve();
            });
        });
    };
    UpdateProjectCommand.prototype._runUpdate = function () {
        // Just to add empty space in log
        console.log();
        var promisesArr = [];
        var updatesLen = this._checkedUpdates.length;
        for (var i = 0; i < updatesLen; i++) {
            var singleUpdate = this._checkedUpdates[i];
            promisesArr.push(this._updateSingleEntity(singleUpdate));
            if (singleUpdate === 'trackingModule') {
                promisesArr.push(this.__sortTrackingService());
            }
        }
        return Promise.all(promisesArr);
    };
    UpdateProjectCommand.prototype.__sortTrackingService = function () {
        var _this = this;
        logger_1.Logger.warning('Make sure to update NON default handy layout client service and import Tracking server service to handy module on server');
        return new Promise(function (resolve, reject) {
            var promisesArr = [];
            promisesArr.push(_this._updateSingleEntity('trackingClientService', false));
            promisesArr.push(_this._updateSingleEntity('trackingServerNonDefaultService', false));
            promisesArr.push(_this.__ensureIndexFilesExportsForTracking());
            promisesArr.push(_this.__ensureHandyConfigTrackingEntries());
            Promise.all(promisesArr).then(function () {
                resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    UpdateProjectCommand.prototype.__ensureIndexFilesExportsForTracking = function () {
        return new Promise(function (resolve, reject) {
            try {
                var servicesIndexStringPath = 'src/client/web/src/app/handy/services/index.ts';
                var clientServicesPath = path_1.join(config_1.ConfigHelper.projectRootDir, servicesIndexStringPath);
                var searchLine = "export * from './handy-ng-tracking.service';";
                var clientIndexContent = fs_extra_1.readFileSync(clientServicesPath, { encoding: 'utf-8' });
                if (!clientIndexContent.includes(searchLine)) {
                    clientIndexContent += "\n\n" + searchLine;
                    fs_extra_1.writeFileSync(clientServicesPath, clientIndexContent, { encoding: 'utf-8' });
                    logger_1.Logger.success("Updated: " + clientServicesPath.replace(config_1.ConfigHelper.projectRootDir, ''));
                }
                resolve();
            }
            catch (error) {
                reject(error);
            }
        });
    };
    UpdateProjectCommand.prototype.__ensureHandyConfigTrackingEntries = function () {
        return new Promise(function (resolve, reject) {
            try {
                var handyJsonPath = path_1.join(config_1.ConfigHelper.projectRootDir, 'handy.json');
                var handyJsonData = JSON.parse(fs_extra_1.readFileSync(handyJsonPath, { encoding: 'utf-8' }));
                if (!handyJsonData['clientAnalytics']) {
                    handyJsonData['clientAnalytics'] = {
                        val: {
                            tracking: true,
                            abTesting: true
                        },
                        devVal: {
                            tracking: true,
                            abTesting: true
                        },
                        public: true
                    };
                    fs_extra_1.writeFileSync(handyJsonPath, JSON.stringify(handyJsonData, null, 4), { encoding: 'utf-8' });
                    logger_1.Logger.success("Updated: " + handyJsonPath.replace(config_1.ConfigHelper.projectRootDir, ''));
                }
                resolve();
            }
            catch (error) {
                reject(error);
            }
        });
    };
    UpdateProjectCommand.prototype._updateSingleEntity = function (entity, overwrite) {
        if (overwrite === void 0) { overwrite = true; }
        var relativePath = paths_1.srcPaths[entity];
        var srcPath = path_1.join(this._tempPath, relativePath);
        var destPath = path_1.join(config_1.ConfigHelper.projectRootDir, relativePath);
        return new Promise(function (resolve, reject) {
            try {
                fs_extra_1.copySync(srcPath, destPath, { recursive: true, overwrite: overwrite });
                logger_1.Logger.success("Updated: " + destPath.replace(config_1.ConfigHelper.projectRootDir, ''));
                return resolve();
            }
            catch (error) {
                logger_1.Logger.error("Update failed: " + destPath.replace(config_1.ConfigHelper.projectRootDir, ''));
                return reject(error);
            }
        });
    };
    return UpdateProjectCommand;
}());
exports.UpdateProjectCommand = UpdateProjectCommand;
