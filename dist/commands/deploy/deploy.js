"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeployCommand = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var DeployCommand = /** @class */ (function () {
    function DeployCommand() {
    }
    DeployCommand.prototype.command = function () {
        var _this = this;
        if (!config_1.ConfigHelper.isInHandyProject) {
            logger_1.Logger.notHandyProject();
            return;
        }
        if (config_1.ConfigHelper.projectEnv !== 'dev') {
            logger_1.Logger.error('Command "handy deploy" is supported on development enviroment only');
            return;
        }
        var accessToken;
        var predeployData;
        logger_1.Logger.info('Getting developer creds');
        config_1.ConfigHelper.getAccessToken()
            .then(function (token) {
            return _this.__deploy(token);
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
            return;
        });
    };
    DeployCommand.prototype.__deploy = function (accessToken) {
        return new Promise(function (resolve, reject) {
            var deployData = config_1.ConfigHelper.getPredeployData();
            var _a = deployData.domains, domains = _a === void 0 ? {} : _a, _b = deployData.repo, repo = _b === void 0 ? null : _b, branch = deployData.branch;
            if (!domains.prod || !domains.stag) {
                return reject('Wrong domains setting in handy.json');
            }
            if (!repo) {
                return reject('Missing remote repository');
            }
            if (branch !== 'master') {
                return reject('You are not on a master branch');
            }
            var socketInstance = socket_io_client_1.default(config_1.ConfigHelper.hubUrl + "cli", {
                query: { accessToken: accessToken },
            });
            var started = false;
            var initialDeploy = false;
            socketInstance.on('saveHandyAppId', function (handyAppId) {
                config_1.ConfigHelper.saveHandyAppId(handyAppId);
            });
            socketInstance.on('chooseServer', function (serversToChooseFrom) {
                var choices = [];
                var serversToChooseFromLen = serversToChooseFrom.length;
                for (var i = 0; i < serversToChooseFromLen; i++) {
                    var ip = serversToChooseFrom[i].ip;
                    if (ip) {
                        choices.push(ip);
                    }
                }
                initialDeploy = true;
                logger_1.Logger.info('Initial deploy');
                var serverChoiceQuestion = {
                    message: 'Choose server',
                    type: 'list',
                    name: 'choosenServerIp',
                    choices: choices,
                };
                inquirer_1.prompt(serverChoiceQuestion)
                    .then(function (result) {
                    var selectedServerToSend = {};
                    for (var i = 0; i < serversToChooseFromLen; i++) {
                        var _a = serversToChooseFrom[i], ip = _a.ip, _id = _a._id;
                        if (result.choosenServerIp === ip) {
                            selectedServerToSend = {
                                selectedIp: ip,
                                selectedId: _id,
                            };
                            break;
                        }
                    }
                    socketInstance.emit('selectedServer', {
                        accessToken: accessToken,
                        eventData: selectedServerToSend
                    });
                });
            });
            var handyAppId;
            socketInstance.on('preparePush', function (appDbdata) {
                var stagPort = appDbdata.stagPort, prodPort = appDbdata.prodPort, repo = appDbdata.repo, _id = appDbdata._id;
                console.log('preparePush', appDbdata);
                handyAppId = _id;
                config_1.ConfigHelper.updatePorts(stagPort, prodPort);
                config_1.ConfigHelper.updateNginxConf(appDbdata);
                config_1.ConfigHelper.addRepoToJsons(repo);
                config_1.ConfigHelper.buildApp().then(function () {
                    return config_1.ConfigHelper.simpleYesNoPrompt('Was build successfull?', true);
                })
                    .then(function (buildResult) {
                    if (!buildResult) {
                        return Promise.reject('Build failed');
                    }
                    return config_1.ConfigHelper.pushRepo();
                })
                    .then(function () {
                    return config_1.ConfigHelper.simpleYesNoPrompt('Was push successfull?', true);
                })
                    .then(function (pushResult) {
                    if (!pushResult) {
                        return Promise.reject('Push failed');
                    }
                    if (initialDeploy) {
                        return Promise.resolve({ env: 'both' });
                    }
                    var envChoiceQuestion = {
                        message: 'Choose enviroment',
                        type: 'list',
                        name: 'env',
                        choices: [
                            {
                                value: 'both',
                                name: 'Both'
                            },
                            {
                                value: 'stag',
                                name: 'Staging'
                            },
                            {
                                value: 'prod',
                                name: 'Production'
                            }
                        ],
                        default: 'stag'
                    };
                    return inquirer_1.prompt(envChoiceQuestion);
                })
                    .then(function (envToDeploy) {
                    var builtAndPushedEvent = {
                        accessToken: accessToken,
                        eventData: { env: envToDeploy.env, initialDeploy: initialDeploy, handyAppId: handyAppId }
                    };
                    console.log({ builtAndPushedEvent: builtAndPushedEvent });
                    socketInstance.emit('builtAndPushed', builtAndPushedEvent);
                })
                    // .then(() => {
                    //   Logger.info('Command finished');
                    //   socketInstance.disconnect();
                    //   return resolve();
                    // })
                    .catch((function (err) {
                    logger_1.Logger.error(err);
                    // socketInstance.disconnect();
                    return resolve();
                }));
            });
            socketInstance.on('connect_error', function () {
                logger_1.Logger.error('Connection error');
                // socketInstance.disconnect();
                return resolve();
            });
            socketInstance.on('error', function (data) {
                logger_1.Logger.error('Connection error');
                console.log(data);
                // socketInstance.disconnect();
                return resolve();
            });
            socketInstance.on('reconnect', function (data) {
                logger_1.Logger.info('Reconnected');
                // console.log(data);
                // socketInstance.disconnect();
                // return resolve();
            });
            socketInstance.on('commandProgress', function (data) {
                console.log(data);
            });
            socketInstance.on('commandError', function (data) {
                logger_1.Logger.error('Command error');
                console.log(data);
                socketInstance.disconnect();
                return resolve();
            });
            socketInstance.on('commandFinish', function () {
                logger_1.Logger.success('Command finsihed');
                socketInstance.disconnect();
                return resolve();
            });
            socketInstance.on('connect', function () {
                if (started) {
                    return;
                }
                started = true;
                var deployEvent = {
                    accessToken: accessToken,
                    eventData: deployData
                };
                socketInstance.emit('deploy', deployEvent);
            });
        });
    };
    return DeployCommand;
}());
exports.DeployCommand = DeployCommand;
