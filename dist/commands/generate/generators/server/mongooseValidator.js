"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerMongooseValidatorGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var ServerMongooseValidatorGenerator = /** @class */ (function () {
    function ServerMongooseValidatorGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'server/validator');
        this._projectTemplatesPaths = {
            validator: path_1.join(this._projectTemplatesDir, 'validatorName.validator.hbs'),
        };
        this._hbsTemplates = {
            validator: '',
        };
        this._extraEmptyFilesToGenerate = [];
        this._indexFileToUpdate = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/server/validators/mongoose/index.ts');
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Where would you like to generate validator',
                type: 'list',
                name: 'generateInValidatorsFolder',
                choices: [
                    {
                        name: "Default Handy validators directory -> ~project/handy/src/server/validators/mongoose/NEW_VALIDATOR",
                        value: true
                    },
                    {
                        name: "Current directory -> " + config_1.ConfigHelper.cwd + "NEW_VALIDATOR",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Validator name',
                type: 'input',
                name: 'validatorName',
            }
        ];
        this._existingFiles = [];
    }
    ServerMongooseValidatorGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.validatorName.trim() === '') {
                return Promise.reject('Invalid validator name');
            }
            _this._defaultAnswers.validatorName = _this._getPrintableValidatorName();
            return Promise.resolve();
        })
            .then(function () {
            return _this._filesVerifyPrompt();
        })
            .then(function () {
            return _this._checkFilesExistency();
        })
            .then(function () {
            return _this._overWritePrompt();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._defaultAnswers.validatorName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    ServerMongooseValidatorGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMongooseValidatorGenerator.prototype._filesVerifyPrompt = function () {
        if (!this._verifyFiles) {
            return Promise.resolve();
        }
        this._logFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Generate files',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Yes',
                            value: true
                        },
                        {
                            name: 'No',
                            value: false
                        },
                    ],
                    default: true
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled validator generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMongooseValidatorGenerator.prototype._overWritePrompt = function () {
        if (this._existingFiles.length === 0) {
            return Promise.resolve();
        }
        this._logExistingFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Overwrite files anyhow',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Owerwrite',
                            value: true
                        },
                        {
                            name: 'Cancel',
                            value: false
                        },
                    ],
                    default: false
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled validator generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMongooseValidatorGenerator.prototype._logFilesToGenerate = function () {
        var filesList = Object.keys(this._getFilesPaths());
        var filesLen = filesList.length;
        console.log();
        logger_1.Logger.info('Files to be generated:');
        console.log();
        for (var i = 0; i < filesLen; i++) {
            var fileName = filesList[i];
            logger_1.Logger.info(this._getFilesPaths()[fileName]);
        }
        if (this._defaultAnswers.generateInValidatorsFolder) {
            console.log();
            logger_1.Logger.info('Files to be updated:');
            console.log();
            logger_1.Logger.info(this._indexFileToUpdate);
        }
        console.log();
    };
    ServerMongooseValidatorGenerator.prototype._getPrintableValidatorName = function () {
        if (this._savedPrintableValidatorName) {
            return this._savedPrintableValidatorName;
        }
        var validatorName = this._defaultAnswers.validatorName.trim();
        var splitted = validatorName.split(' ');
        var splittedLen = splitted.length;
        var result = '';
        for (var i = 0; i < splittedLen; i++) {
            if (splitted[i] === 'Validator' || splitted[i] === 'validator') {
                continue;
            }
            result += config_1.ConfigHelper.UcFirst(splitted[i]).trim();
        }
        this._savedPrintableValidatorName = result;
        return this._savedPrintableValidatorName;
    };
    ServerMongooseValidatorGenerator.prototype._getFilesPaths = function () {
        if (this._savedFilesPath) {
            return this._savedFilesPath;
        }
        var location = path_1.join((this._defaultAnswers.generateInValidatorsFolder) ? config_1.ConfigHelper.projectRootDir + 'src/server/validators/mongoose' : config_1.ConfigHelper.cwd, config_1.ConfigHelper.LcFirst(this._getPrintableValidatorName()));
        this._foldersToGenerate.push(location);
        return {
            validator: path_1.join(location, config_1.ConfigHelper.LcFirst(this._getPrintableValidatorName()) + '.validator.ts')
        };
    };
    ServerMongooseValidatorGenerator.prototype._generateFolders = function () {
        var foldersLen = this._foldersToGenerate.length;
        for (var i = 0; i < foldersLen; i++) {
            fs_extra_1.ensureDirSync(this._foldersToGenerate[i]);
        }
    };
    ServerMongooseValidatorGenerator.prototype._generateEmptyFiles = function () {
        var filesLen = this._extraEmptyFilesToGenerate.length;
        for (var i = 0; i < filesLen; i++) {
            fs_extra_1.writeFileSync(this._extraEmptyFilesToGenerate[i], '', { encoding: 'utf-8' });
        }
    };
    ServerMongooseValidatorGenerator.prototype._checkFilesExistency = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            for (var i = 0; i < templatesLen; i++) {
                var templateName = templatesList[i];
                var pathToCheck = _this._getFilesPaths()[templateName];
                if (fs_extra_1.existsSync(pathToCheck) && pathToCheck !== _this._indexFileToUpdate) {
                    _this._existingFiles.push(pathToCheck);
                }
            }
            return resolve();
        });
    };
    ServerMongooseValidatorGenerator.prototype._logExistingFilesToGenerate = function () {
        var filesLen = this._existingFiles.length;
        console.log();
        logger_1.Logger.warning('It seems like some of the to be generated files already exists:');
        console.log();
        var msg = '';
        for (var i = 0; i < filesLen; i++) {
            msg += this._existingFiles[i] + '\n';
        }
        logger_1.Logger.warning(msg);
    };
    ServerMongooseValidatorGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            try {
                _this._generateFolders();
                _this._generateEmptyFiles();
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesList[i];
                    var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
                    _this._hbsTemplates[templateName] = handlebars_1.compile(source)(_this._defaultAnswers);
                    fs_extra_1.writeFileSync(_this._getFilesPaths()[templateName], _this._hbsTemplates[templateName], { encoding: 'utf-8' });
                }
                if (_this._defaultAnswers.generateInValidatorsFolder) {
                    var originalIndex = fs_extra_1.readFileSync(_this._indexFileToUpdate, { encoding: 'utf-8' });
                    var printableValidatorName = config_1.ConfigHelper.LcFirst(_this._getPrintableValidatorName());
                    var exportToAdd = "export * from './" + printableValidatorName + "/" + printableValidatorName + ".validator';";
                    if (!originalIndex.includes(exportToAdd)) {
                        fs_extra_1.writeFileSync(_this._indexFileToUpdate, originalIndex + "\n" + exportToAdd, { encoding: 'utf-8' });
                    }
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return ServerMongooseValidatorGenerator;
}());
exports.ServerMongooseValidatorGenerator = ServerMongooseValidatorGenerator;
