"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerControllersGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var route_1 = require("./controllers/route");
var middleware_1 = require("./controllers/middleware");
var socket_1 = require("./controllers/socket");
var ServerControllersGenerator = /** @class */ (function () {
    function ServerControllersGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._defaultQuestions = [
            {
                message: 'What king of controller woyuld you like to generate',
                type: 'list',
                name: 'controller',
                choices: [
                    {
                        name: "Route controller",
                        value: 'route'
                    },
                    {
                        name: "Middleware controller",
                        value: 'middleware'
                    },
                    {
                        name: "Socket controller",
                        value: 'socket'
                    },
                ],
                default: true
            }
        ];
    }
    ServerControllersGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            var Generator = undefined;
            switch (_this._defaultAnswers.controller) {
                case 'route':
                    Generator = new route_1.ServerRouteControllerGenerator(_this._verifyFiles);
                    break;
                case 'middleware':
                    Generator = new middleware_1.ServerMiddlewareGenerator(_this._verifyFiles);
                    break;
                case 'socket':
                    Generator = new socket_1.ServerSocketControllerGenerator(_this._verifyFiles);
                    break;
                default:
                    break;
            }
            if (Generator !== undefined) {
                Generator.command();
                return;
            }
            return Promise.reject('No matching controller type found');
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    ServerControllersGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    return ServerControllersGenerator;
}());
exports.ServerControllersGenerator = ServerControllersGenerator;
