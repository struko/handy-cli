"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerMiddlewareGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../../helpers/logger");
var config_1 = require("../../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var ServerMiddlewareGenerator = /** @class */ (function () {
    function ServerMiddlewareGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'server/middleware-controller');
        this._projectTemplatesPaths = {
            controller: path_1.join(this._projectTemplatesDir, 'controllerName.middleware.hbs'),
        };
        this._hbsTemplates = {
            controller: '',
        };
        this._indexFileToUpdate = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/server/controllers/middlewares/index.ts');
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Where would you like to generate middleware',
                type: 'list',
                name: 'generateInControllersFolder',
                choices: [
                    {
                        name: "Default Handy controllers directory -> ~project/handy/src/server/controllers/middlewares/NEW_MIDDLEWARE",
                        value: true
                    },
                    {
                        name: "Current directory -> " + config_1.ConfigHelper.cwd + "NEW_MIDDLEWARE",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Middleware name',
                type: 'input',
                name: 'controllerName',
            },
            {
                message: 'Is this an error handling middleware',
                type: 'list',
                name: 'errorMiddleware',
                choices: [
                    {
                        name: "Yes",
                        value: true
                    },
                    {
                        name: "No",
                        value: false
                    },
                ],
                default: false
            },
        ];
        this._existingFiles = [];
    }
    ServerMiddlewareGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.controllerName.trim() === '') {
                return Promise.reject('Invalid controller name');
            }
            _this._defaultAnswers.controllerName = _this._getPrintableControllerName();
            return _this._filesVerifyPrompt();
        })
            .then(function () {
            return _this._checkFilesExistency();
        })
            .then(function () {
            return _this._overWritePrompt();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._defaultAnswers.controllerName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    ServerMiddlewareGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMiddlewareGenerator.prototype._filesVerifyPrompt = function () {
        if (!this._verifyFiles) {
            return Promise.resolve();
        }
        this._logFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Generate files',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Yes',
                            value: true
                        },
                        {
                            name: 'No',
                            value: false
                        },
                    ],
                    default: true
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled middleware generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMiddlewareGenerator.prototype._overWritePrompt = function () {
        if (this._existingFiles.length === 0) {
            return Promise.resolve();
        }
        this._logExistingFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Overwrite files anyhow',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Owerwrite',
                            value: true
                        },
                        {
                            name: 'Cancel',
                            value: false
                        },
                    ],
                    default: false
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled middleware generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerMiddlewareGenerator.prototype._logFilesToGenerate = function () {
        var filesList = Object.keys(this._getFilesPaths());
        var filesLen = filesList.length;
        console.log();
        logger_1.Logger.info('Files to be generated:');
        console.log();
        for (var i = 0; i < filesLen; i++) {
            var fileName = filesList[i];
            logger_1.Logger.info(this._getFilesPaths()[fileName]);
        }
        if (this._defaultAnswers.generateInControllersFolder) {
            console.log();
            logger_1.Logger.info('Files to be updated:');
            console.log();
            logger_1.Logger.info(this._indexFileToUpdate);
        }
        console.log();
    };
    ServerMiddlewareGenerator.prototype._getPrintableControllerName = function () {
        if (this._savedPrintableControllerName) {
            return this._savedPrintableControllerName;
        }
        var controllerName = this._defaultAnswers.controllerName.trim();
        var splitted = controllerName.split(' ');
        var splittedLen = splitted.length;
        var result = '';
        for (var i = 0; i < splittedLen; i++) {
            if (splitted[i] === 'Middleware' || splitted[i] === 'middleware') {
                continue;
            }
            result += config_1.ConfigHelper.UcFirst(splitted[i]).trim();
        }
        this._savedPrintableControllerName = result;
        return this._savedPrintableControllerName;
    };
    ServerMiddlewareGenerator.prototype._getFilesPaths = function () {
        if (this._savedFilesPath) {
            return this._savedFilesPath;
        }
        var location = path_1.join((this._defaultAnswers.generateInControllersFolder) ? config_1.ConfigHelper.projectRootDir + 'src/server/controllers/middlewares' : config_1.ConfigHelper.cwd, config_1.ConfigHelper.LcFirst(this._getPrintableControllerName()));
        this._foldersToGenerate.push(location);
        return {
            controller: path_1.join(location, config_1.ConfigHelper.LcFirst(this._getPrintableControllerName()) + '.middleware.ts')
        };
    };
    ServerMiddlewareGenerator.prototype._generateFolders = function () {
        var foldersLen = this._foldersToGenerate.length;
        for (var i = 0; i < foldersLen; i++) {
            fs_extra_1.ensureDirSync(this._foldersToGenerate[i]);
        }
    };
    ServerMiddlewareGenerator.prototype._checkFilesExistency = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            for (var i = 0; i < templatesLen; i++) {
                var templateName = templatesList[i];
                var pathToCheck = _this._getFilesPaths()[templateName];
                if (fs_extra_1.existsSync(pathToCheck) && pathToCheck !== _this._indexFileToUpdate) {
                    _this._existingFiles.push(pathToCheck);
                }
            }
            return resolve();
        });
    };
    ServerMiddlewareGenerator.prototype._logExistingFilesToGenerate = function () {
        var filesLen = this._existingFiles.length;
        console.log();
        logger_1.Logger.warning('It seems like some of the to be generated files already exists:');
        console.log();
        var msg = '';
        for (var i = 0; i < filesLen; i++) {
            msg += this._existingFiles[i] + '\n';
        }
        logger_1.Logger.warning(msg);
    };
    ServerMiddlewareGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            try {
                _this._generateFolders();
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesList[i];
                    var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
                    _this._hbsTemplates[templateName] = handlebars_1.compile(source)(_this._defaultAnswers);
                    fs_extra_1.writeFileSync(_this._getFilesPaths()[templateName], _this._hbsTemplates[templateName], { encoding: 'utf-8' });
                }
                if (_this._defaultAnswers.generateInControllersFolder) {
                    var originalIndex = fs_extra_1.readFileSync(_this._indexFileToUpdate, { encoding: 'utf-8' });
                    var printableControllerName = config_1.ConfigHelper.LcFirst(_this._getPrintableControllerName());
                    var exportToAdd = "export * from './" + printableControllerName + "/" + printableControllerName + ".middleware';";
                    if (!originalIndex.includes(exportToAdd)) {
                        fs_extra_1.writeFileSync(_this._indexFileToUpdate, originalIndex + "\n" + exportToAdd, { encoding: 'utf-8' });
                    }
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return ServerMiddlewareGenerator;
}());
exports.ServerMiddlewareGenerator = ServerMiddlewareGenerator;
