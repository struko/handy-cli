"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../helpers/logger");
var config_1 = require("../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var ServerModelGenerator = /** @class */ (function () {
    function ServerModelGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'server-model');
        this._projectTemplatesPaths = {
            interface: path_1.join(this._projectTemplatesDir, 'model.interface.hbs'),
            declaration: path_1.join(this._projectTemplatesDir, 'model.declaration.hbs'),
            model: path_1.join(this._projectTemplatesDir, 'modelName.model.hbs'),
            index: path_1.join(this._projectTemplatesDir, 'index.hbs'),
        };
        this._hbsTemplates = {
            interface: '',
            declaration: '',
            model: '',
            index: ''
        };
        this._indexFileToUpdate = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/server/models/index.ts');
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Where would you like to generate model',
                type: 'list',
                name: 'generateInModelsFolder',
                choices: [
                    {
                        name: "Default Handy models directory -> ~project/handy/src/server/models/NEW_MODEL",
                        value: true
                    },
                    {
                        name: "Current directory -> " + config_1.ConfigHelper.cwd + "NEW_MODEL",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Model name',
                type: 'input',
                name: 'modelName',
            },
            {
                message: 'Use autoincrement',
                type: 'list',
                name: 'autoIncrement',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Use soft delete',
                type: 'list',
                name: 'softDelete',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Created at',
                type: 'list',
                name: 'createdAt',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Created by',
                type: 'list',
                name: 'createdBy',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Changes history',
                type: 'list',
                name: 'changesHistory',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Access model via api',
                type: 'list',
                name: 'routable',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
        ];
        this._publicRoutableQuestion = [
            {
                message: 'Access model via PUBLIC api',
                type: 'list',
                name: 'publicRoutable',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
        ];
        this._publicRoutableAnswers = { publicRoutable: false };
        this._existingFiles = [];
    }
    ServerModelGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.modelName.trim() === '') {
                return Promise.reject('Invalid model name');
            }
            if (_this._defaultAnswers.routable) {
                return _this._publicRoutablePrompt();
            }
            return Promise.resolve();
        })
            .then(function () {
            _this._completeAnswers = __assign(__assign({}, _this._defaultAnswers), _this._publicRoutableAnswers);
            _this._completeAnswers.modelName = _this._getPrintableModelName();
            return _this._filesVerifyPrompt();
        })
            .then(function () {
            return _this._checkFilesExistency();
        })
            .then(function () {
            return _this._overWritePrompt();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._completeAnswers.modelName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    ServerModelGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelGenerator.prototype._publicRoutablePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._publicRoutableQuestion)
                .then(function (answers) {
                _this._publicRoutableAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelGenerator.prototype._filesVerifyPrompt = function () {
        if (!this._verifyFiles) {
            return Promise.resolve();
        }
        this._logFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Generate files',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Yes',
                            value: true
                        },
                        {
                            name: 'No',
                            value: false
                        },
                    ],
                    default: true
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled model generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelGenerator.prototype._overWritePrompt = function () {
        if (this._existingFiles.length === 0) {
            return Promise.resolve();
        }
        this._logExistingFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Overwrite files anyhow',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Owerwrite',
                            value: true
                        },
                        {
                            name: 'Cancel',
                            value: false
                        },
                    ],
                    default: false
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled model generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelGenerator.prototype._logFilesToGenerate = function () {
        var filesList = Object.keys(this._getFilesPaths());
        var filesLen = filesList.length;
        console.log();
        logger_1.Logger.info('Files to be generated:');
        console.log();
        for (var i = 0; i < filesLen; i++) {
            var fileName = filesList[i];
            logger_1.Logger.info(this._getFilesPaths()[fileName]);
        }
        if (this._defaultAnswers.generateInModelsFolder) {
            console.log();
            logger_1.Logger.info('Files to be updated:');
            console.log();
            logger_1.Logger.info(this._indexFileToUpdate);
        }
        console.log();
    };
    ServerModelGenerator.prototype._getPrintableModelName = function () {
        if (this._savedPrintableModelName) {
            return this._savedPrintableModelName;
        }
        var modelName = this._completeAnswers.modelName.trim();
        var splitted = modelName.split(' ');
        var splittedLen = splitted.length;
        var result = '';
        for (var i = 0; i < splittedLen; i++) {
            if (splitted[i] === 'Model' || splitted[i] === 'model') {
                continue;
            }
            result += config_1.ConfigHelper.UcFirst(splitted[i]).trim();
        }
        this._savedPrintableModelName = result;
        return this._savedPrintableModelName;
    };
    ServerModelGenerator.prototype._getFilesPaths = function () {
        if (this._savedFilesPath) {
            return this._savedFilesPath;
        }
        var location = path_1.join((this._defaultAnswers.generateInModelsFolder) ? config_1.ConfigHelper.projectRootDir + 'src/server/models' : config_1.ConfigHelper.cwd, config_1.ConfigHelper.LcFirst(this._getPrintableModelName()));
        this._foldersToGenerate.push(location);
        return {
            interface: path_1.join(location, 'model.interface.ts'),
            declaration: path_1.join(location, 'model.declaration.ts'),
            model: path_1.join(location, config_1.ConfigHelper.LcFirst(this._getPrintableModelName()) + '.model.ts'),
            index: path_1.join(location, 'index.ts'),
        };
    };
    ServerModelGenerator.prototype._generateFolders = function () {
        var foldersLen = this._foldersToGenerate.length;
        for (var i = 0; i < foldersLen; i++) {
            fs_extra_1.ensureDirSync(this._foldersToGenerate[i]);
        }
    };
    ServerModelGenerator.prototype._checkFilesExistency = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            for (var i = 0; i < templatesLen; i++) {
                var templateName = templatesList[i];
                var pathToCheck = _this._getFilesPaths()[templateName];
                if (fs_extra_1.existsSync(pathToCheck) && pathToCheck !== _this._indexFileToUpdate) {
                    _this._existingFiles.push(pathToCheck);
                }
            }
            return resolve();
        });
    };
    ServerModelGenerator.prototype._logExistingFilesToGenerate = function () {
        var filesLen = this._existingFiles.length;
        console.log();
        logger_1.Logger.warning('It seems like some of the to be generated files already exists:');
        console.log();
        var msg = '';
        for (var i = 0; i < filesLen; i++) {
            msg += this._existingFiles[i] + '\n';
        }
        logger_1.Logger.warning(msg);
    };
    ServerModelGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var templatesList = Object.keys(_this._getFilesPaths());
            var templatesLen = templatesList.length;
            try {
                _this._generateFolders();
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesList[i];
                    var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
                    _this._hbsTemplates[templateName] = handlebars_1.compile(source)(_this._completeAnswers);
                    fs_extra_1.writeFileSync(_this._getFilesPaths()[templateName], _this._hbsTemplates[templateName], { encoding: 'utf-8' });
                }
                if (_this._defaultAnswers.generateInModelsFolder) {
                    var originalIndex = fs_extra_1.readFileSync(_this._indexFileToUpdate, { encoding: 'utf-8' });
                    var printableModelName = config_1.ConfigHelper.LcFirst(_this._getPrintableModelName());
                    var exportToAdd = "export * from './" + printableModelName + "';";
                    if (!originalIndex.includes(exportToAdd)) {
                        fs_extra_1.writeFileSync(_this._indexFileToUpdate, originalIndex + "\n" + exportToAdd, { encoding: 'utf-8' });
                    }
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return ServerModelGenerator;
}());
exports.ServerModelGenerator = ServerModelGenerator;
