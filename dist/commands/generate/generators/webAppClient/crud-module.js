"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CrudModuleGenerator = void 0;
var logger_1 = require("../../../../helpers/logger");
var server_models_helper_1 = require("../../../../helpers/server.models.helper");
var CrudModuleGenerator = /** @class */ (function () {
    function CrudModuleGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
    }
    CrudModuleGenerator.prototype.command = function (asPromise) {
        var _this = this;
        if (asPromise === void 0) { asPromise = false; }
        return new Promise(function (resolve, reject) {
            server_models_helper_1.ServerModelHelper.selectModelPrompt()
                // Module
                .then(function () {
                return server_models_helper_1.ServerModelHelper.moduleNamePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.moduleEntryTypePrompt();
            })
                // Table
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableOptionalDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableSearchColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableFilterColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.rememberTableStatePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.tableNamePrompt();
            })
                // Form
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectedFormFieldsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.rememberFormStatePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.formNamePrompt();
            })
                // Files generating
                // .then(() => {
                //   // TODO not yet
                //   return ServerModelHelper.generateFormComponentFiles();
                // })
                .then(function () {
                return _this._generateModule();
            })
                .then(function () {
                // console.log(ServerModelHelper.getStandAloneFormGenerationData())
                // console.log(ServerModelHelper.getStandAloneTableGenerationData())
                return resolve();
            })
                .catch(function (err) {
                if (!asPromise) {
                    logger_1.Logger.error(err);
                }
                return reject(err);
            });
        });
    };
    CrudModuleGenerator.prototype._generateModule = function () {
        return new Promise(function (resolve, reject) {
            server_models_helper_1.ServerModelHelper.generateModuleFiles();
            server_models_helper_1.ServerModelHelper.generateFormComponentFiles();
            server_models_helper_1.ServerModelHelper.generateTableComponentFiles();
            resolve();
        });
    };
    return CrudModuleGenerator;
}());
exports.CrudModuleGenerator = CrudModuleGenerator;
