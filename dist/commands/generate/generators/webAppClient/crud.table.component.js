"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CrudTableComponentGenerator = void 0;
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var server_models_helper_1 = require("../../../../helpers/server.models.helper");
var CrudTableComponentGenerator = /** @class */ (function () {
    function CrudTableComponentGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-table-component');
        this._projectTemplatesPaths = {
            styles: path_1.join(this._projectTemplatesDir, 'tableName.styles.hbs'),
            html: path_1.join(this._projectTemplatesDir, 'tableName.html.hbs'),
            logic: path_1.join(this._projectTemplatesDir, 'tableName.logic.hbs'),
        };
        this._hbsTemplates = {
            styles: '',
            html: '',
            logic: '',
        };
        this._foldersToGenerate = [];
    }
    CrudTableComponentGenerator.prototype.command = function (asPromise) {
        if (asPromise === void 0) { asPromise = false; }
        return new Promise(function (resolve, reject) {
            server_models_helper_1.ServerModelHelper.selectModelPrompt()
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableOptionalDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableSearchColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableFilterColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.rememberTableStatePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.tableNamePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.generateTableComponentFiles();
            })
                .then(function () {
                return resolve();
            })
                .catch(function (err) {
                if (!asPromise) {
                    logger_1.Logger.error(err);
                }
                return reject(err);
            });
        });
    };
    return CrudTableComponentGenerator;
}());
exports.CrudTableComponentGenerator = CrudTableComponentGenerator;
