"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebAppEmptyFormComponentGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var WebAppEmptyFormComponentGenerator = /** @class */ (function () {
    function WebAppEmptyFormComponentGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/empty-form-component');
        this._projectTemplatesPaths = {
            ts: path_1.join(this._projectTemplatesDir, 'formName.logic.hbs'),
            scss: path_1.join(this._projectTemplatesDir, 'formName.styles.hbs'),
            html: path_1.join(this._projectTemplatesDir, 'formName.html.hbs'),
        };
        this._hbsTemplates = {
            ts: '',
            scss: '',
            html: '',
        };
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Form name',
                type: 'input',
                name: 'formName',
            },
            {
                message: 'Would you like to remember this form state for user',
                type: 'list',
                name: 'rememberFormState',
                choices: [
                    {
                        name: "Yes",
                        value: true
                    },
                    {
                        name: "No",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Would you like to generate resolver for this form',
                type: 'list',
                name: 'includeResolver',
                choices: [
                    {
                        name: "Yes",
                        value: true
                    },
                    {
                        name: "No",
                        value: false
                    },
                ],
                default: true
            },
        ];
    }
    WebAppEmptyFormComponentGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.formName.trim() === '') {
                return Promise.reject('Invalid form name');
            }
            return Promise.resolve();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._defaultAnswers.formName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    WebAppEmptyFormComponentGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                _this._defaultAnswers.formName = config_1.ConfigHelper.getPrintableString(_this._defaultAnswers.formName);
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppEmptyFormComponentGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                config_1.ConfigHelper.generateFolders([config_1.ConfigHelper.magicBoxPath]);
                var componentFileName = config_1.ConfigHelper.getFileNameString(_this._defaultAnswers.formName);
                var resultDir = path_1.join(config_1.ConfigHelper.magicBoxPath, componentFileName);
                fs_extra_1.ensureDirSync(resultDir);
                var templatesKeys = Object.keys(_this._hbsTemplates);
                var templatesLen = templatesKeys.length;
                for (var i = 0; i < templatesLen; i++) {
                    var fileType = templatesKeys[i];
                    var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[fileType], { encoding: 'utf-8' });
                    _this._hbsTemplates[fileType] = handlebars_1.compile(source)(_this._defaultAnswers);
                    var finalFilePath = path_1.join(resultDir, componentFileName + ".component." + fileType);
                    fs_extra_1.writeFileSync(finalFilePath, _this._hbsTemplates[fileType], { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return WebAppEmptyFormComponentGenerator;
}());
exports.WebAppEmptyFormComponentGenerator = WebAppEmptyFormComponentGenerator;
