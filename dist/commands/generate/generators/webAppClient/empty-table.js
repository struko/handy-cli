"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebAppEmptyTableComponentGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var WebAppEmptyTableComponentGenerator = /** @class */ (function () {
    function WebAppEmptyTableComponentGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/empty-table-component');
        this._projectTemplatesPaths = {
            ts: path_1.join(this._projectTemplatesDir, 'tableName.logic.hbs'),
            scss: path_1.join(this._projectTemplatesDir, 'tableName.styles.hbs'),
            html: path_1.join(this._projectTemplatesDir, 'tableName.html.hbs'),
        };
        this._hbsTemplates = {
            ts: '',
            scss: '',
            html: '',
        };
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Table name',
                type: 'input',
                name: 'tableName',
            },
            {
                message: 'Would you like to remember this table state for user',
                type: 'list',
                name: 'rememberTableState',
                choices: [
                    {
                        name: "Yes",
                        value: true
                    },
                    {
                        name: "No",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Would you like to generate resolver for this table',
                type: 'list',
                name: 'includeResolver',
                choices: [
                    {
                        name: "Yes",
                        value: true
                    },
                    {
                        name: "No",
                        value: false
                    },
                ],
                default: true
            },
        ];
    }
    WebAppEmptyTableComponentGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.tableName.trim() === '') {
                return Promise.reject('Invalid table name');
            }
            return Promise.resolve();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._defaultAnswers.tableName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    WebAppEmptyTableComponentGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                _this._defaultAnswers.tableName = config_1.ConfigHelper.getPrintableString(_this._defaultAnswers.tableName);
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppEmptyTableComponentGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                config_1.ConfigHelper.generateFolders([config_1.ConfigHelper.magicBoxPath]);
                var componentFileName = config_1.ConfigHelper.getFileNameString(_this._defaultAnswers.tableName);
                var resultDir = path_1.join(config_1.ConfigHelper.magicBoxPath, componentFileName);
                fs_extra_1.ensureDirSync(resultDir);
                var templatesKeys = Object.keys(_this._hbsTemplates);
                var templatesLen = templatesKeys.length;
                for (var i = 0; i < templatesLen; i++) {
                    var fileType = templatesKeys[i];
                    var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[fileType], { encoding: 'utf-8' });
                    _this._hbsTemplates[fileType] = handlebars_1.compile(source)(_this._defaultAnswers);
                    var finalFilePath = path_1.join(resultDir, componentFileName + ".component." + fileType);
                    fs_extra_1.writeFileSync(finalFilePath, _this._hbsTemplates[fileType], { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return WebAppEmptyTableComponentGenerator;
}());
exports.WebAppEmptyTableComponentGenerator = WebAppEmptyTableComponentGenerator;
