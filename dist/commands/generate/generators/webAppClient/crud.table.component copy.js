"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var server_models_helper_1 = require("../../../../helpers/server.models.helper");
var CrudTableComponentGenerator = /** @class */ (function () {
    function CrudTableComponentGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-table-component');
        this._projectTemplatesPaths = {
            styles: path_1.join(this._projectTemplatesDir, 'tableName.styles.hbs'),
            html: path_1.join(this._projectTemplatesDir, 'tableName.html.hbs'),
            logic: path_1.join(this._projectTemplatesDir, 'tableName.logic.hbs'),
        };
        this._hbsTemplates = {
            styles: '',
            html: '',
            logic: '',
        };
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'Where would you like to generate model',
                type: 'list',
                name: 'generateInModelsFolder',
                choices: [
                    {
                        name: "Default Handy models directory -> ~project/handy/src/server/models/NEW_MODEL",
                        value: true
                    },
                    {
                        name: "Current directory -> " + config_1.ConfigHelper.cwd + "NEW_MODEL",
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Model name',
                type: 'input',
                name: 'modelName',
            },
            {
                message: 'Use autoincrement',
                type: 'list',
                name: 'autoIncrement',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Use soft delete',
                type: 'list',
                name: 'softDelete',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Created at',
                type: 'list',
                name: 'createdAt',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Created by',
                type: 'list',
                name: 'createdBy',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Track document Changes history',
                type: 'list',
                name: 'changesHistory',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
            {
                message: 'Access model via api',
                type: 'list',
                name: 'routable',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    },
                ],
                default: true
            },
        ];
    }
    CrudTableComponentGenerator.prototype.command = function (asPromise) {
        if (asPromise === void 0) { asPromise = false; }
        return new Promise(function (resolve, reject) {
            server_models_helper_1.ServerModelHelper.selectModelPrompt()
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableOptionalDisplayColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableSearchColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectTableFilterColumnsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.rememberTableStatePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.tableNamePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.generateTableComponentFiles();
            })
                .then(function () {
                return resolve();
            })
                .catch(function (err) {
                if (!asPromise) {
                    logger_1.Logger.error(err);
                }
                return reject(err);
            });
        });
    };
    return CrudTableComponentGenerator;
}());
exports.CrudTableComponentGenerator = CrudTableComponentGenerator;
// C:\Projects\handy\src\server\handy\core\defaults\cli\templates\webApp\modelName.ng-model.hbs
