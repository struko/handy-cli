"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CrudFormComponentGenerator = void 0;
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var server_models_helper_1 = require("../../../../helpers/server.models.helper");
var CrudFormComponentGenerator = /** @class */ (function () {
    function CrudFormComponentGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-form-component');
        this._projectTemplatesPaths = {
            styles: path_1.join(this._projectTemplatesDir, 'formName.styles.hbs'),
            html: path_1.join(this._projectTemplatesDir, 'formName.html.hbs'),
            logic: path_1.join(this._projectTemplatesDir, 'formName.logic.hbs'),
        };
        this._hbsTemplates = {
            styles: '',
            html: '',
            logic: '',
        };
    }
    CrudFormComponentGenerator.prototype.command = function (asPromise) {
        if (asPromise === void 0) { asPromise = false; }
        return new Promise(function (resolve, reject) {
            server_models_helper_1.ServerModelHelper.selectModelPrompt()
                .then(function () {
                return server_models_helper_1.ServerModelHelper.formTypePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.includeResolver();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.selectedFormFieldsPrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.rememberFormStatePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.formNamePrompt();
            })
                .then(function () {
                return server_models_helper_1.ServerModelHelper.generateFormComponentFiles();
            })
                .then(function () {
                return resolve();
            })
                .catch(function (err) {
                if (!asPromise) {
                    logger_1.Logger.error(err);
                }
                return reject(err);
            });
        });
    };
    return CrudFormComponentGenerator;
}());
exports.CrudFormComponentGenerator = CrudFormComponentGenerator;
