"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebAppFormValidatorGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var WebAppFormValidatorGenerator = /** @class */ (function () {
    function WebAppFormValidatorGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/form-validators');
        this._projectTemplatesPaths = {
            sync: path_1.join(this._projectTemplatesDir, 'sync.hbs'),
            async: path_1.join(this._projectTemplatesDir, 'async.hbs'),
        };
        this._hbsTemplates = {
            sync: '',
            async: '',
        };
        this._indexFileToUpdate = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/client/web/src/app/modules/shared/form-validators/index.ts');
        this._foldersToGenerate = [];
        this._defaultQuestions = [
            {
                message: 'What type of validator would you like to generate',
                type: 'list',
                name: 'type',
                choices: [
                    {
                        name: "Synchronous",
                        value: 'sync'
                    },
                    {
                        name: "Asynchronous",
                        value: 'async'
                    },
                ],
                default: 'sync'
            },
            {
                message: 'Validator name',
                type: 'input',
                name: 'validatorName',
            }
        ];
        this._existingFiles = [];
    }
    WebAppFormValidatorGenerator.prototype.command = function () {
        var _this = this;
        this._prompt()
            .then(function () {
            if (_this._defaultAnswers.validatorName.trim() === '') {
                return Promise.reject('Invalid validator name');
            }
            return Promise.resolve();
        })
            .then(function () {
            return _this._filesVerifyPrompt();
        })
            .then(function () {
            return _this._checkFilesExistency();
        })
            .then(function () {
            return _this._overWritePrompt();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success(config_1.ConfigHelper.UcFirst(_this._defaultAnswers.validatorName) + " was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    WebAppFormValidatorGenerator.prototype._prompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._defaultQuestions)
                .then(function (answers) {
                _this._defaultAnswers = answers;
                _this._defaultAnswers.validatorName = config_1.ConfigHelper.getPrintableString(_this._defaultAnswers.validatorName);
                _this._getFilesPaths();
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppFormValidatorGenerator.prototype._filesVerifyPrompt = function () {
        if (!this._verifyFiles) {
            return Promise.resolve();
        }
        this._logFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Generate files',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Yes',
                            value: true
                        },
                        {
                            name: 'No',
                            value: false
                        },
                    ],
                    default: true
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled validator generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppFormValidatorGenerator.prototype._overWritePrompt = function () {
        if (this._existingFiles.length === 0) {
            return Promise.resolve();
        }
        this._logExistingFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Overwrite files anyhow',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Owerwrite',
                            value: true
                        },
                        {
                            name: 'Cancel',
                            value: false
                        },
                    ],
                    default: false
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled validator generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppFormValidatorGenerator.prototype._logFilesToGenerate = function () {
        var filesList = Object.keys(this._getFilesPaths());
        var filesLen = filesList.length;
        console.log();
        logger_1.Logger.info('Files to be generated:');
        console.log();
        logger_1.Logger.info(this._savedFilesPath);
        console.log();
        logger_1.Logger.info('Files to be updated:');
        console.log();
        logger_1.Logger.info(this._indexFileToUpdate);
        console.log();
    };
    WebAppFormValidatorGenerator.prototype._getFilesPaths = function () {
        if (this._savedFilesPath) {
            return this._savedFilesPath;
        }
        this._savedFilesPath = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/client/web/src/app/modules/shared/form-validators/', config_1.ConfigHelper.getFileNameString(this._defaultAnswers.validatorName) + '.validator.ts');
        return this._savedFilesPath;
    };
    WebAppFormValidatorGenerator.prototype._checkFilesExistency = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (fs_extra_1.existsSync(_this._savedFilesPath)) {
                _this._existingFiles.push(_this._savedFilesPath);
            }
            return resolve();
        });
    };
    WebAppFormValidatorGenerator.prototype._logExistingFilesToGenerate = function () {
        var filesLen = this._existingFiles.length;
        console.log();
        logger_1.Logger.warning('It seems like some of the to be generated files already exists:');
        console.log();
        var msg = '';
        for (var i = 0; i < filesLen; i++) {
            msg += this._existingFiles[i] + '\n';
        }
        logger_1.Logger.warning(msg);
    };
    WebAppFormValidatorGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                var type = (_this._defaultAnswers.type === 'sync') ? 'sync' : 'async';
                var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths[type], { encoding: 'utf-8' });
                _this._hbsTemplates[type] = handlebars_1.compile(source)(_this._defaultAnswers);
                fs_extra_1.writeFileSync(_this._savedFilesPath, _this._hbsTemplates[type], { encoding: 'utf-8' });
                var originalIndex = fs_extra_1.readFileSync(_this._indexFileToUpdate, { encoding: 'utf-8' });
                var validatorName = config_1.ConfigHelper.getFileNameString(_this._defaultAnswers.validatorName);
                var exportToAdd = "export * from './" + validatorName + ".validator';";
                if (!originalIndex.includes(exportToAdd)) {
                    fs_extra_1.writeFileSync(_this._indexFileToUpdate, originalIndex + "\n" + exportToAdd, { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return WebAppFormValidatorGenerator;
}());
exports.WebAppFormValidatorGenerator = WebAppFormValidatorGenerator;
