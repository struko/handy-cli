"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebAppModelGenerator = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../../../helpers/logger");
var config_1 = require("../../../../helpers/config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var fs_extra_1 = require("fs-extra");
var server_models_helper_1 = require("../../../../helpers/server.models.helper");
var WebAppModelGenerator = /** @class */ (function () {
    function WebAppModelGenerator(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._projectTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/model-service');
        this._projectTemplatesPaths = {
            model: path_1.join(this._projectTemplatesDir, 'modelName.ng-model.hbs'),
        };
        this._destinationFolder = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/client/web/src/app/handy/models');
        this._existingFiles = [];
    }
    WebAppModelGenerator.prototype.command = function () {
        var _this = this;
        server_models_helper_1.ServerModelHelper.selectModelPrompt()
            .then(function () {
            _this._modelName = config_1.ConfigHelper.LcFirst(server_models_helper_1.ServerModelHelper.getSelectedModelData().modelName);
            _this._modelFileDestinationPath = path_1.join(_this._destinationFolder, _this._modelName + '.ng-model.ts');
            return _this._filesVerifyPrompt();
        })
            .then(function () {
            _this._checkFilesExistency();
            return _this._overWritePrompt();
        })
            .then(function () {
            return _this._generateFiles();
        })
            .then(function () {
            logger_1.Logger.success("Ng model service was generated");
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    WebAppModelGenerator.prototype._filesVerifyPrompt = function () {
        if (!this._verifyFiles) {
            return Promise.resolve();
        }
        this._logFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Generate files',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Yes',
                            value: true
                        },
                        {
                            name: 'No',
                            value: false
                        },
                    ],
                    default: true
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled model generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppModelGenerator.prototype._overWritePrompt = function () {
        if (this._existingFiles.length === 0) {
            return Promise.resolve();
        }
        this._logExistingFilesToGenerate();
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    message: 'Overwrite files anyhow',
                    type: 'list',
                    name: 'generate',
                    choices: [
                        {
                            name: 'Owerwrite',
                            value: true
                        },
                        {
                            name: 'Cancel',
                            value: false
                        },
                    ],
                    default: false
                },
            ])
                .then(function (answers) {
                if (answers.generate) {
                    return resolve();
                }
                return reject('User canceled model generation');
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    WebAppModelGenerator.prototype._logFilesToGenerate = function () {
        console.log();
        logger_1.Logger.info('Files to be generated:');
        console.log();
        logger_1.Logger.info(this._modelFileDestinationPath);
        console.log();
    };
    WebAppModelGenerator.prototype._checkFilesExistency = function () {
        if (fs_extra_1.existsSync(this._modelFileDestinationPath)) {
            this._existingFiles.push(this._modelFileDestinationPath);
        }
    };
    WebAppModelGenerator.prototype._logExistingFilesToGenerate = function () {
        var filesLen = this._existingFiles.length;
        console.log();
        logger_1.Logger.warning('It seems like some of the to be generated files already exists:');
        console.log();
        var msg = '';
        for (var i = 0; i < filesLen; i++) {
            msg += this._existingFiles[i] + '\n';
        }
        logger_1.Logger.warning(msg);
    };
    WebAppModelGenerator.prototype._generateFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                var source = fs_extra_1.readFileSync(_this._projectTemplatesPaths.model, { encoding: 'utf-8' });
                var compiledModle = handlebars_1.compile(source)({ modelName: _this._modelName });
                fs_extra_1.writeFileSync(_this._modelFileDestinationPath, compiledModle, { encoding: 'utf-8' });
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return WebAppModelGenerator;
}());
exports.WebAppModelGenerator = WebAppModelGenerator;
