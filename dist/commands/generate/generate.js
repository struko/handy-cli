"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateCommand = void 0;
var inquirer_1 = require("inquirer");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var model_1 = require("./generators/server/model");
var service_1 = require("./generators/server/service");
var module_1 = require("./generators/server/module");
var controllers_1 = require("./generators/server/controllers");
var mongooseValidator_1 = require("./generators/server/mongooseValidator");
var crud_table_component_1 = require("./generators/webAppClient/crud.table.component");
var model_service_1 = require("./generators/webAppClient/model.service");
var crud_form_component_1 = require("./generators/webAppClient/crud.form.component");
var crud_module_1 = require("./generators/webAppClient/crud-module");
var form_validator_1 = require("./generators/webAppClient/form-validator");
var empty_form_1 = require("./generators/webAppClient/empty-form");
var GenerateCommand = /** @class */ (function () {
    function GenerateCommand(_verifyFiles) {
        this._verifyFiles = _verifyFiles;
        this._platformQuestions = [
            {
                message: 'What would you like to generate for',
                type: 'list',
                name: 'whatToGen',
                choices: [
                    {
                        name: 'Server',
                        value: 'server'
                    },
                    {
                        name: 'Web client',
                        value: 'webApp'
                    },
                ],
            }
        ];
        this._serverQuestions = [
            {
                message: 'What would you like to generate',
                type: 'list',
                name: 'whatToGen',
                choices: [
                    {
                        name: 'Model',
                        value: 'serverModel'
                    },
                    {
                        name: 'Service',
                        value: 'serverService'
                    },
                    {
                        name: 'Module',
                        value: 'serverModule'
                    },
                    {
                        name: 'Controller',
                        value: 'serverController'
                    },
                    {
                        name: 'Mongoose validator',
                        value: 'serverMongooseValidator'
                    },
                ],
            }
        ];
        this._webAppQuestions = [
            {
                message: 'What would you like to generate',
                type: 'list',
                name: 'whatToGen',
                choices: [
                    {
                        name: 'Crud table component',
                        value: 'crudTable'
                    },
                    {
                        name: 'Crud form component',
                        value: 'crudForm'
                    },
                    {
                        name: 'Crud module',
                        value: 'crudModule'
                    },
                    {
                        name: 'Ng Model service',
                        value: 'modelService'
                    },
                    {
                        name: 'Empty form component',
                        value: 'emptyForm'
                    },
                    {
                        name: 'Form validator',
                        value: 'formValidator'
                    },
                ],
            }
        ];
    }
    GenerateCommand.prototype.command = function () {
        var _this = this;
        if (!config_1.ConfigHelper.isInHandyProject) {
            logger_1.Logger.notHandyProject();
            return;
        }
        if (config_1.ConfigHelper.projectEnv !== 'dev') {
            logger_1.Logger.error('Command "handy run" is supported on development enviroment only');
            return;
        }
        this._platformPrompt()
            .then(function () {
            switch (_this._platformAnswers.whatToGen) {
                case 'server':
                    return _this._serverPrompt();
                case 'webApp':
                    return _this._webAppClientPrompt();
                default:
                    break;
            }
            return Promise.reject('No matching generator found');
        })
            .then(function () {
        })
            .catch(function (err) {
            logger_1.Logger.error(err);
        });
    };
    GenerateCommand.prototype._platformPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._platformQuestions)
                .then(function (answers) {
                _this._platformAnswers = answers;
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    GenerateCommand.prototype._serverPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._serverQuestions)
                .then(function (answers) {
                _this._serverAnswers = answers;
                _this._handleServerGenerators();
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    GenerateCommand.prototype._handleServerGenerators = function () {
        var Generator = undefined;
        switch (this._serverAnswers.whatToGen) {
            case 'serverModel':
                Generator = new model_1.ServerModelGenerator(this._verifyFiles);
                break;
            case 'serverService':
                Generator = new service_1.ServerServiceGenerator(this._verifyFiles);
                break;
            case 'serverModule':
                Generator = new module_1.ServerModuleGenerator(this._verifyFiles);
                break;
            case 'serverController':
                Generator = new controllers_1.ServerControllersGenerator(this._verifyFiles);
                break;
            case 'serverMongooseValidator':
                Generator = new mongooseValidator_1.ServerMongooseValidatorGenerator(this._verifyFiles);
                break;
            default:
                break;
        }
        if (Generator !== undefined) {
            Generator.command();
            return;
        }
    };
    GenerateCommand.prototype._webAppClientPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._webAppQuestions)
                .then(function (answers) {
                _this._webAppAnswers = answers;
                _this._handleWebAppGenerators();
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    GenerateCommand.prototype._handleWebAppGenerators = function () {
        var Generator = undefined;
        switch (this._webAppAnswers.whatToGen) {
            case 'crudTable':
                Generator = new crud_table_component_1.CrudTableComponentGenerator(this._verifyFiles);
                break;
            case 'modelService':
                Generator = new model_service_1.WebAppModelGenerator(this._verifyFiles);
                break;
            case 'crudForm':
                Generator = new crud_form_component_1.CrudFormComponentGenerator(this._verifyFiles);
                break;
            case 'crudModule':
                Generator = new crud_module_1.CrudModuleGenerator(this._verifyFiles);
                break;
            case 'formValidator':
                Generator = new form_validator_1.WebAppFormValidatorGenerator(this._verifyFiles);
                break;
            case 'emptyForm':
                Generator = new empty_form_1.WebAppEmptyFormComponentGenerator(this._verifyFiles);
                break;
            default:
                break;
        }
        if (Generator !== undefined) {
            Generator.command();
            return;
        }
    };
    return GenerateCommand;
}());
exports.GenerateCommand = GenerateCommand;
