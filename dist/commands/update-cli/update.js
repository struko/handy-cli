"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateCliCommand = void 0;
var child_process_1 = require("child_process");
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var logger_1 = require("../../helpers/logger");
var UpdateCliCommand = /** @class */ (function () {
    function UpdateCliCommand() {
    }
    UpdateCliCommand.prototype.command = function () {
        var path = path_1.join(__dirname, '../../../');
        child_process_1.execSync("cd " + path + " && git stash");
        child_process_1.execSync("cd " + path + " && git pull");
        child_process_1.execSync("cd " + path + " && npm install");
        var CLI_VERSION = JSON.parse(fs_extra_1.readFileSync(path_1.join(path, 'package.json'), 'utf-8')).version;
        logger_1.Logger.success("Handy cli updated to v" + CLI_VERSION);
    };
    return UpdateCliCommand;
}());
exports.UpdateCliCommand = UpdateCliCommand;
