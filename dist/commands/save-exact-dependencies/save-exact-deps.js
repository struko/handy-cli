"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SaveExactDepsCommand = void 0;
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var logger_1 = require("../../helpers/logger");
var config_1 = require("../../helpers/config");
var SaveExactDepsCommand = /** @class */ (function () {
    function SaveExactDepsCommand() {
    }
    SaveExactDepsCommand.prototype.command = function () {
        var extecPath = config_1.ConfigHelper.cwd;
        var lockFile = path_1.join(extecPath, 'package-lock.json');
        var packageFile = path_1.join(extecPath, 'package.json');
        if (!fs_extra_1.existsSync(lockFile)) {
            logger_1.Logger.error('No package-lock.json file is present, make sure you are running this command in a root folder of node project and all dependencies are installed.');
            return;
        }
        if (!fs_extra_1.existsSync(packageFile)) {
            logger_1.Logger.error('No package.json file is present, make sure you are running this command in a root folder of node project.');
            return;
        }
        var packageContent = JSON.parse(fs_extra_1.readFileSync(packageFile, { encoding: 'utf-8' }));
        var lockContent = JSON.parse(fs_extra_1.readFileSync(lockFile, { encoding: 'utf-8' }));
        var deps = Object.keys(packageContent.dependencies);
        var devDeps = Object.keys(packageContent.devDependencies);
        var depsLen = deps.length;
        var devDepsLen = devDeps.length;
        for (var i = 0; i < depsLen; i++) {
            var depName = deps[i];
            var version = lockContent.dependencies[depName].version;
            packageContent.dependencies[depName] = version;
        }
        for (var i = 0; i < devDepsLen; i++) {
            var depName = devDeps[i];
            var version = lockContent.dependencies[depName].version;
            packageContent.devDependencies[depName] = version;
        }
        fs_extra_1.writeFileSync(packageFile, JSON.stringify(packageContent, null, 1));
        logger_1.Logger.info('Done');
    };
    return SaveExactDepsCommand;
}());
exports.SaveExactDepsCommand = SaveExactDepsCommand;
;
;
