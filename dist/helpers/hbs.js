"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var handlebars_1 = require("handlebars");
var config_1 = require("./config");
var form_field_hbs_helper_1 = require("./form.field.hbs.helper");
handlebars_1.registerHelper('LcFirst', function (value) {
    return config_1.ConfigHelper.LcFirst(value);
});
handlebars_1.registerHelper('UcFirst', function (value) {
    return config_1.ConfigHelper.UcFirst(value);
});
handlebars_1.registerHelper('LcAll', function (value) {
    return value.toLowerCase();
});
handlebars_1.registerHelper('filePath', function (value) {
    return config_1.ConfigHelper.getFileNameString(value);
});
handlebars_1.registerHelper('moduleTableActions', function (value, type) {
    var result = '';
    if (value.name !== 'actions') {
        result += "<mat-header-cell *matHeaderCellDef cdkDrag cdkDragLockAxis=\"x\" mat-sort-header><span *cdkDragPreview><basic-btn icon=\"swap_horiz\">" + value.publicName + "</basic-btn></span>" + value.publicName + "</mat-header-cell>\n\n";
        result += "          <mat-cell *matCellDef=\"let element\">{{element." + value.name + "}}</mat-cell>";
    }
    else {
        result += "<mat-header-cell *matHeaderCellDef class=\"crud-table-actions-col\" cdkDrag cdkDragLockAxis=\"x\" mat-sort-header><span *cdkDragPreview><basic-btn icon=\"swap_horiz\">" + value.publicName + "</basic-btn></span>" + value.publicName + "</mat-header-cell>\n\n";
        result += "          <mat-cell class=\"crud-table-actions-col\" *matCellDef=\"let element\">\n\n";
        result += "            <div fxLayout=\"row\" fxLayoutAlign=\"flex-end center\" fxLayoutGap=\"8px\">\n";
        result += "              <stroked-btn icon=\"edit\" color=\"primary\" " + ((type === 'page') ? '[routerLink]="[\'./edit/\' + element._id]"' : '(click)="editOrCreateAction(element._id)"') + ">Edit</stroked-btn>\n";
        result += "              <stroked-btn icon=\"delete_otl\" color=\"warn\" (confirmClick)=\"removeEntryAction(element._id)\">Remove</stroked-btn>\n\n";
        result += "            </div>\n\n";
        result += "          </mat-cell>";
    }
    return result;
});
handlebars_1.registerHelper('TableFilterDataObjDestroy', function (value) {
    var result = '';
    var valLen = value.length;
    for (var i = 0; i < valLen; i++) {
        var filterName = value[i].name;
        result += " " + filterName + ",";
    }
    if (result !== '') {
        result = "let {" + result + " } = filterData;";
    }
    return result;
});
handlebars_1.registerHelper('TableMarkupElement', function (value) {
    return "{{element." + value + "}}";
});
handlebars_1.registerHelper('TableFiltersToggleBtn', function (value) {
    return "<stroked-btn #filtersToggle color=\"primary\" icon=\"filter_list\">{{ (filtersToggleState) ? 'Hide' : 'Show'}} filters</stroked-btn>  ";
});
handlebars_1.registerHelper('TableMarkupElement', function (value) {
    return "{{element." + value + "}}";
});
handlebars_1.registerHelper('FormHTMLField', function (value, formName) {
    return form_field_hbs_helper_1.fieldValueRenderer(value, formName);
});
handlebars_1.registerHelper('FormFieldControl', function (value) {
    return form_field_hbs_helper_1.fieldFormControlRenderer(value);
});
