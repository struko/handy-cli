"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerModelHelper = void 0;
var inquirer_1 = require("inquirer");
var fs_extra_1 = require("fs-extra");
var config_1 = require("./config");
var path_1 = require("path");
var handlebars_1 = require("handlebars");
var InnerHelper;
var ServerModelHelper = /** @class */ (function () {
    function ServerModelHelper() {
        this._modelCliDataPath = path_1.join(config_1.ConfigHelper.projectRootDir, 'src/cli/server-ref-files/models');
        this._projectMagicBoxPath = path_1.join(config_1.ConfigHelper.projectRootDir, 'magic-box');
        this._generatingForModule = false;
        this._modelsList = [];
        /* ---------------------------------- Table --------------------------------- */
        this._projectTableTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-table-component');
        this._projectTableTemplatesPaths = {
            styles: path_1.join(this._projectTableTemplatesDir, 'tableName.styles.hbs'),
            html: path_1.join(this._projectTableTemplatesDir, 'tableName.html.hbs'),
            logic: path_1.join(this._projectTableTemplatesDir, 'tableName.logic.hbs'),
        };
        /* ---------------------------------- Form ---------------------------------- */
        this._projectFormTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-form-component');
        this._projectFormTemplatesPaths = {
            styles: path_1.join(this._projectFormTemplatesDir, 'formName.styles.hbs'),
            html: path_1.join(this._projectFormTemplatesDir, 'formName.html.hbs'),
            logic: path_1.join(this._projectFormTemplatesDir, 'formName.logic.hbs'),
        };
        this._hbsFormTemplates = {
            styles: '',
            html: '',
            logic: '',
        };
        /* --------------------------------- Module --------------------------------- */
        this._projectModuleTemplatesDir = path_1.join(config_1.ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-module');
        this._projectModuleTemplatesPaths = {
            styles: path_1.join(this._projectModuleTemplatesDir, 'moduleName.component.styles.hbs'),
            html: path_1.join(this._projectModuleTemplatesDir, 'moduleName.component.html.hbs'),
            logic: path_1.join(this._projectModuleTemplatesDir, 'moduleName.component.logic.hbs'),
            module: path_1.join(this._projectModuleTemplatesDir, 'moduleName.module.hbs'),
            routing: path_1.join(this._projectModuleTemplatesDir, 'moduleName.routing.hbs'),
        };
        this._hbsModuleTemplates = {
            styles: '',
            html: '',
            logic: '',
            module: '',
            routing: ''
        };
        /* -------------------------------------------------------------------------- */
        /*                             Crud table related                             */
        /* -------------------------------------------------------------------------- */
        /* --------------------- Table display columns selecting -------------------- */
        this._selectedTableDisplayColumns = [];
        this._selectedTableDisplayColumnsUnionStr = '';
        this._selectedTableDisplayColumnsArrStr = '';
        this._selectedTableFieldsToQueryArrStr = '';
        /* -------------------- Optional dislay columns selecting ------------------- */
        this._selectedTableOptionalDisplayColumns = [];
        this._selectedTableOptionalDisplayColumnsUnionStr = '';
        /* ------------------------- Table searchable fields ------------------------ */
        this._selectedTableSearchColumns = [];
        this._selectedTableSearchColumnsUnionStr = '';
        /* ------------------------- Table searchable fields ------------------------ */
        this._selectedTableFilterColumns = [];
        this._selectedTableFilterColumnsUnionStr = '';
        /* -------------------------------------------------------------------------- */
        /*                              Crud form related                             */
        /* -------------------------------------------------------------------------- */
        /* -------------------------- Form fields selecting ------------------------- */
        this._selectedFormFields = [];
        this._selectedFormFieldsUnionStr = '';
        this._selectedFormFieldsArrStr = '';
        /* -------------------------------- Form type ------------------------------- */
        this._formType = 'both';
        /* ---------------------------- Include resolver ---------------------------- */
        this._includeResolver = true;
        /* -------------------------- Entry type model/page ------------------------- */
        this._moduleEntryType = 'page';
        this._getModelsList();
    }
    ServerModelHelper.prototype._getModelsList = function () {
        var modelsFilesList = fs_extra_1.readdirSync(this._modelCliDataPath);
        var filesLen = modelsFilesList.length;
        for (var i = 0; i < filesLen; i++) {
            var fileName = modelsFilesList[i];
            if (!fileName.endsWith('modelData.json')) {
                continue;
            }
            this._modelsList.push(fileName.split('.')[0]);
        }
        var choices = [];
        var modelsLen = this._modelsList.length;
        for (var i = 0; i < modelsLen; i++) {
            var modelName = this._modelsList[i];
            choices.push({
                name: config_1.ConfigHelper.UcFirst(modelName),
                value: modelName,
            });
        }
        this._modelsSelectionQuestion = [
            {
                message: 'Select model',
                type: 'list',
                name: 'modelName',
                choices: choices
            }
        ];
    };
    ServerModelHelper.prototype._parseSelectedModelData = function () {
        var modelDataFileContent = fs_extra_1.readFileSync(path_1.join(this._modelCliDataPath, this._selectedModelName + ".modelData.json"), { encoding: 'utf-8' });
        this._selectedModelData = JSON.parse(modelDataFileContent);
    };
    ServerModelHelper.getIntance = function () {
        if (!InnerHelper) {
            InnerHelper = new ServerModelHelper();
        }
        return InnerHelper;
    };
    /* ----------------------------- Model selecting ---------------------------- */
    ServerModelHelper.prototype._selectModelPrompt = function () {
        var _this = this;
        if (this._selectedModelName) {
            return Promise.resolve();
        }
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt(_this._modelsSelectionQuestion)
                .then(function (answers) {
                _this._selectedModelName = answers.modelName;
                _this._parseSelectedModelData();
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelHelper.selectModelPrompt = function () {
        return ServerModelHelper.getIntance()._selectModelPrompt();
    };
    ServerModelHelper.prototype._selectTableDisplayColumnsPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var actionsField = {
                name: 'actions',
                inputType: 'none',
                publicName: 'Actions',
                unique: false,
                required: false
            };
            var choices = [];
            var fieldsLen = _this._selectedModelData.fields.length;
            for (var i = 0; i < fieldsLen; i++) {
                var field = _this._selectedModelData.fields[i];
                choices.push({
                    value: field,
                    name: field.publicName,
                    checked: true
                });
            }
            choices.push({
                value: actionsField,
                name: 'Actions',
                checked: true
            });
            var promtpQuestion = [
                {
                    message: 'Select table fields to include as a display columns',
                    name: 'columns',
                    type: 'checkbox',
                    choices: choices,
                    default: []
                }
            ];
            inquirer_1.prompt(promtpQuestion)
                .then(function (answers) {
                _this._selectedTableDisplayColumns = answers.columns;
                var selecteColumnsLen = _this._selectedTableDisplayColumns.length;
                for (var i = 0; i < selecteColumnsLen; i++) {
                    var element = _this._selectedTableDisplayColumns[i];
                    _this._selectedTableDisplayColumnsUnionStr += "'" + element.name + "'";
                    _this._selectedTableDisplayColumnsArrStr += "'" + element.name + "'";
                    if (element.name !== 'actions') {
                        _this._selectedTableFieldsToQueryArrStr += "'" + element.name + "'";
                    }
                    if (i + 1 < selecteColumnsLen) {
                        _this._selectedTableDisplayColumnsUnionStr += " | ";
                        _this._selectedTableDisplayColumnsArrStr += ", ";
                        if (element.name !== 'actions') {
                            _this._selectedTableFieldsToQueryArrStr += ", ";
                        }
                    }
                }
                // this._selectedTableDisplayColumnsUnionStr = this._selectedTableDisplayColumnsUnionStr.trim();
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelHelper.selectTableDisplayColumnsPrompt = function () {
        return ServerModelHelper.getIntance()._selectTableDisplayColumnsPrompt();
    };
    ServerModelHelper.prototype._selectTableOptionalDisplayColumnsPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Make some table columns optional')
                .then(function (result) {
                _this._hasTableOptionalDisplayColumns = result;
                if (!result) {
                    resolve();
                    return;
                }
                var choices = [];
                var fieldsLen = _this._selectedTableDisplayColumns.length;
                for (var i = 0; i < fieldsLen; i++) {
                    var _a = _this._selectedTableDisplayColumns[i], name_1 = _a.name, publicName = _a.publicName;
                    choices.push({
                        value: _this._selectedTableDisplayColumns[i],
                        name: publicName,
                        checked: (name_1 !== 'actions' && name_1 !== '_id')
                    });
                }
                var promtpQuestion = [
                    {
                        message: 'Select table columns that can be optiona',
                        name: 'columns',
                        type: 'checkbox',
                        choices: choices,
                        default: []
                    }
                ];
                inquirer_1.prompt(promtpQuestion)
                    .then(function (answers) {
                    _this._selectedTableOptionalDisplayColumns = answers.columns;
                    var selecteColumnsLen = _this._selectedTableOptionalDisplayColumns.length;
                    for (var i = 0; i < selecteColumnsLen; i++) {
                        var element = _this._selectedTableOptionalDisplayColumns[i];
                        _this._selectedTableOptionalDisplayColumnsUnionStr += "'" + element.name + "'";
                        if (i + 1 < selecteColumnsLen) {
                            _this._selectedTableOptionalDisplayColumnsUnionStr += " | ";
                        }
                    }
                    _this._selectedTableOptionalDisplayColumnsUnionStr = _this._selectedTableOptionalDisplayColumnsUnionStr.trim();
                    return resolve();
                })
                    .catch(function (err) {
                    reject(err);
                });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelHelper.selectTableOptionalDisplayColumnsPrompt = function () {
        return ServerModelHelper.getIntance()._selectTableOptionalDisplayColumnsPrompt();
    };
    ServerModelHelper.prototype._selectTableSearchColumnsPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Include search in table')
                .then(function (result) {
                _this._includeSearch = result;
                if (!result) {
                    _this._selectedTableSearchColumns = [];
                    resolve();
                    return;
                }
                var choices = [];
                var fieldsLen = _this._selectedModelData.searchableFields.length;
                for (var i = 0; i < fieldsLen; i++) {
                    var field = _this._selectedModelData.searchableFields[i];
                    choices.push({
                        value: field,
                        name: field.displayValue,
                        checked: true
                    });
                }
                var promtpQuestion = [
                    {
                        message: 'Select fields to search through while using table search',
                        name: 'columns',
                        type: 'checkbox',
                        choices: choices,
                        default: []
                    }
                ];
                inquirer_1.prompt(promtpQuestion)
                    .then(function (answers) {
                    _this._selectedTableSearchColumns = answers.columns;
                    resolve();
                    return;
                })
                    .catch(function (err) {
                    reject(err);
                    return;
                });
            })
                .catch(function (err) {
                reject(err);
                return;
            });
        });
    };
    ServerModelHelper.selectTableSearchColumnsPrompt = function () {
        return ServerModelHelper.getIntance()._selectTableSearchColumnsPrompt();
    };
    ServerModelHelper.prototype._selectTableFilterColumnsPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Include extra table filters')
                .then(function (result) {
                _this._includeFilter = result;
                if (!result) {
                    _this._selectedTableFilterColumns = [];
                    resolve();
                    return;
                }
                var choices = [];
                var fieldsLen = _this._selectedModelData.fields.length;
                for (var i = 0; i < fieldsLen; i++) {
                    var field = _this._selectedModelData.fields[i];
                    choices.push({
                        value: field,
                        name: field.publicName,
                        checked: true
                    });
                }
                var promtpQuestion = [
                    {
                        message: 'Select fields for additional table filters',
                        name: 'columns',
                        type: 'checkbox',
                        choices: choices,
                        default: []
                    }
                ];
                inquirer_1.prompt(promtpQuestion)
                    .then(function (answers) {
                    _this._selectedTableFilterColumns = answers.columns;
                    resolve();
                    return;
                })
                    .catch(function (err) {
                    reject(err);
                    return;
                });
            })
                .catch(function (err) {
                reject(err);
                return;
            });
        });
    };
    ServerModelHelper.selectTableFilterColumnsPrompt = function () {
        return ServerModelHelper.getIntance()._selectTableFilterColumnsPrompt();
    };
    ServerModelHelper.prototype._tableNamePrompt = function (defaultVal) {
        var _this = this;
        if (!defaultVal) {
            defaultVal = config_1.ConfigHelper.UcFirst(this._selectedModelName) + " crud table";
        }
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpletInputPrompt('Table name', defaultVal)
                .then(function (result) {
                _this._tableName = config_1.ConfigHelper.getPrintableString(result);
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.tableNamePrompt = function (defaultVal) {
        return ServerModelHelper.getIntance()._tableNamePrompt(defaultVal);
    };
    ServerModelHelper.prototype._rememberTableStatePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Remember table in user state')
                .then(function (result) {
                _this._rememberTableState = result;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.rememberTableStatePrompt = function () {
        return ServerModelHelper.getIntance()._rememberTableStatePrompt();
    };
    ServerModelHelper.getStandAloneTableGenerationData = function () {
        var instance = ServerModelHelper.getIntance();
        var _tableName = instance._tableName, _selectedModelName = instance._selectedModelName, _rememberTableState = instance._rememberTableState, _selectedTableDisplayColumnsArrStr = instance._selectedTableDisplayColumnsArrStr, _selectedTableFieldsToQueryArrStr = instance._selectedTableFieldsToQueryArrStr, _hasTableOptionalDisplayColumns = instance._hasTableOptionalDisplayColumns, _selectedTableOptionalDisplayColumns = instance._selectedTableOptionalDisplayColumns, _includeSearch = instance._includeSearch, _selectedTableSearchColumns = instance._selectedTableSearchColumns, _includeFilter = instance._includeFilter, _selectedTableFilterColumns = instance._selectedTableFilterColumns, _selectedTableDisplayColumnsUnionStr = instance._selectedTableDisplayColumnsUnionStr, _selectedTableDisplayColumns = instance._selectedTableDisplayColumns, _generatingForModule = instance._generatingForModule, _moduleName = instance._moduleName, _moduleEntryType = instance._moduleEntryType, _selectedModelData = instance._selectedModelData, _formName = instance._formName;
        return {
            modelName: _selectedModelName,
            tableName: (_tableName === '') ? config_1.ConfigHelper.getPrintableString(_selectedModelName + 's table') : config_1.ConfigHelper.getPrintableString(_tableName),
            forModule: _generatingForModule,
            moduleName: _moduleName,
            formName: config_1.ConfigHelper.getPrintableString(_formName),
            entryType: _moduleEntryType,
            pageEntry: _moduleEntryType === 'page',
            modalEntry: _moduleEntryType === 'modal',
            autoIncrement: _selectedModelData.autoIncrement,
            rememberTableState: _rememberTableState,
            fieldsToQueryStr: _selectedTableFieldsToQueryArrStr,
            displayedCols: _selectedTableDisplayColumns,
            displayedColsStr: _selectedTableDisplayColumnsArrStr,
            displayColumnsUnionStr: _selectedTableDisplayColumnsUnionStr,
            hasOptionalCols: _hasTableOptionalDisplayColumns,
            optionalCols: _selectedTableOptionalDisplayColumns,
            hasSearch: _includeSearch,
            searchableFields: _selectedTableSearchColumns,
            hasAdditionalFilters: _includeFilter,
            aditionalFilters: _selectedTableFilterColumns,
            hasActionsCol: _selectedTableDisplayColumnsArrStr.includes("'actions'"),
            hasSearchOrFilter: _includeSearch || _includeFilter || _hasTableOptionalDisplayColumns,
            hasOptionalColsOrFilter: _includeFilter || _hasTableOptionalDisplayColumns
        };
    };
    ServerModelHelper.getSelectedModelData = function () {
        return ServerModelHelper.getIntance()._selectedModelData;
    };
    ServerModelHelper.generateTableComponentFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                config_1.ConfigHelper.generateFolders([config_1.ConfigHelper.magicBoxPath]);
                var instance = _this.getIntance();
                var printableTableName = (instance._tableName !== '') ? config_1.ConfigHelper.getPrintableString(instance._tableName) : config_1.ConfigHelper.getPrintableString(instance._selectedModelName + 's table');
                var tableNameFilePathStr = config_1.ConfigHelper.getFileNameString(printableTableName);
                var folders = [];
                if (instance._generatingForModule) {
                    var generatedModulePath = path_1.join(instance._projectMagicBoxPath, config_1.ConfigHelper.getFileNameString(instance._moduleName));
                    folders = [
                        generatedModulePath,
                        path_1.join(generatedModulePath, tableNameFilePathStr),
                    ];
                }
                else {
                    folders = [
                        path_1.join(instance._projectMagicBoxPath, tableNameFilePathStr)
                    ];
                }
                config_1.ConfigHelper.generateFolders(folders);
                var templatesNames = Object.keys(instance._projectTableTemplatesPaths);
                var templatesLen = templatesNames.length;
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesNames[i];
                    var source = fs_extra_1.readFileSync(instance._projectTableTemplatesPaths[templateName], { encoding: 'utf-8' });
                    var compiledTemplate = handlebars_1.compile(source)(_this.getStandAloneTableGenerationData());
                    var filePath = void 0;
                    var rootPath = (instance._generatingForModule) ? path_1.join(instance._projectMagicBoxPath, config_1.ConfigHelper.getFileNameString(instance._moduleName)) : instance._projectMagicBoxPath;
                    switch (templateName) {
                        case 'styles':
                            filePath = path_1.join(rootPath, tableNameFilePathStr, tableNameFilePathStr + ".component.scss");
                            break;
                        case 'html':
                            filePath = path_1.join(rootPath, tableNameFilePathStr, tableNameFilePathStr + ".component.html");
                            break;
                        default:
                            filePath = path_1.join(rootPath, tableNameFilePathStr, tableNameFilePathStr + ".component.ts");
                            break;
                    }
                    // console.log(filePath)
                    fs_extra_1.writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    ServerModelHelper.prototype._selectedFormFieldsPrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var choices = [];
            var fieldsLen = _this._selectedModelData.fields.length;
            for (var i = 0; i < fieldsLen; i++) {
                var field = _this._selectedModelData.fields[i];
                choices.push({
                    value: field,
                    name: field.publicName,
                    checked: (field.name !== '_id')
                });
            }
            var promtpQuestion = [
                {
                    message: 'Select fields to include in crud form',
                    name: 'fields',
                    type: 'checkbox',
                    choices: choices,
                    default: []
                }
            ];
            inquirer_1.prompt(promtpQuestion)
                .then(function (answers) {
                _this._selectedFormFields = answers.fields;
                var selectedFormFieldsLen = _this._selectedFormFields.length;
                for (var i = 0; i < selectedFormFieldsLen; i++) {
                    var element = _this._selectedFormFields[i];
                    _this._selectedFormFieldsUnionStr += "'" + element.name + "'";
                    _this._selectedFormFieldsArrStr += "'" + element.name + "'";
                    if (i + 1 < selectedFormFieldsLen) {
                        _this._selectedFormFieldsUnionStr += " | ";
                        _this._selectedFormFieldsArrStr += ", ";
                    }
                }
                return resolve();
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ServerModelHelper.selectedFormFieldsPrompt = function () {
        return ServerModelHelper.getIntance()._selectedFormFieldsPrompt();
    };
    ServerModelHelper.prototype._rememberFormStatePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Remember form in user state')
                .then(function (result) {
                _this._rememberFormState = result;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.rememberFormStatePrompt = function () {
        return ServerModelHelper.getIntance()._rememberFormStatePrompt();
    };
    ServerModelHelper.prototype._formNamePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpletInputPrompt('Form name', config_1.ConfigHelper.UcFirst(_this._selectedModelName + ' crud form'))
                .then(function (result) {
                if (!result) {
                    result = _this._selectedModelName + ' form';
                }
                _this._formName = config_1.ConfigHelper.getPrintableString(result);
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.formNamePrompt = function () {
        return ServerModelHelper.getIntance()._formNamePrompt();
    };
    ServerModelHelper.prototype._formTypePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt({
                name: 'formType',
                message: 'What type of form would you like to create',
                type: 'list',
                choices: [
                    {
                        name: 'Create',
                        value: 'create'
                    },
                    {
                        name: 'Update',
                        value: 'update'
                    },
                    {
                        name: 'Both',
                        value: 'both'
                    }
                ],
                default: 'both'
            })
                .then(function (result) {
                _this._formType = result.formType;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.formTypePrompt = function () {
        return ServerModelHelper.getIntance()._formTypePrompt();
    };
    ServerModelHelper.prototype._includeResolverPrompt = function () {
        var _this = this;
        if (this._formType === 'create') {
            return Promise.resolve();
        }
        return new Promise(function (resolve, reject) {
            config_1.ConfigHelper.simpleYesNoPrompt('Would you like to create a resolver for update form', true)
                .then(function (result) {
                _this._includeResolver = result;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.includeResolver = function () {
        return ServerModelHelper.getIntance()._includeResolverPrompt();
    };
    ServerModelHelper.getStandAloneFormGenerationData = function () {
        var instance = ServerModelHelper.getIntance();
        var _selectedModelName = instance._selectedModelName, _formName = instance._formName, _rememberFormState = instance._rememberFormState, _selectedFormFieldsUnionStr = instance._selectedFormFieldsUnionStr, _selectedFormFieldsArrStr = instance._selectedFormFieldsArrStr, _selectedFormFields = instance._selectedFormFields, _formType = instance._formType, _includeResolver = instance._includeResolver, _selectedModelData = instance._selectedModelData, _generatingForModule = instance._generatingForModule, _moduleName = instance._moduleName, _moduleEntryType = instance._moduleEntryType;
        var formArraysFields = [];
        var hasFormArraysFields = false;
        var selectOptionsFields = [];
        var hasSelectOptionsFields = false;
        var radioGroupOptionsFields = [];
        var hasRadioGroupOptionsFields = false;
        var fieldsLen = _selectedFormFields.length;
        for (var index = 0; index < fieldsLen; index++) {
            var _a = _selectedFormFields[index], name_2 = _a.name, inputType = _a.inputType;
            if (inputType.includes('[') && inputType !== '[Files]') {
                hasFormArraysFields = true;
                formArraysFields.push(name_2);
            }
            if (inputType.includes('radio-group')) {
                hasRadioGroupOptionsFields = true;
                radioGroupOptionsFields.push(name_2);
            }
            if (inputType.includes('select')) {
                hasSelectOptionsFields = true;
                selectOptionsFields.push(name_2);
            }
        }
        return {
            modelName: _selectedModelName,
            formName: _formName,
            forModule: _generatingForModule,
            moduleName: _moduleName,
            rememberFormState: _rememberFormState,
            fieldsUnionStr: _selectedFormFieldsUnionStr,
            fieldsArrStr: _selectedFormFieldsArrStr,
            fields: _selectedFormFields,
            formType: _formType,
            includeResolver: _includeResolver,
            includeCreate: (_formType !== 'update'),
            includeUpdate: (_formType !== 'create'),
            formArraysFields: formArraysFields,
            hasFormArraysFields: hasFormArraysFields,
            selectOptionsFields: selectOptionsFields,
            hasSelectOptionsFields: hasSelectOptionsFields,
            radioGroupOptionsFields: radioGroupOptionsFields,
            hasRadioGroupOptionsFields: hasRadioGroupOptionsFields,
            autoIncrement: _selectedModelData.autoIncrement,
            separateCrudMethods: (_formType === 'both'),
            updateOnly: (_formType === 'update'),
            createOnly: (_formType === 'create'),
            entryType: _moduleEntryType,
            pageEntry: _moduleEntryType === 'page',
            modalEntry: _moduleEntryType === 'modal',
        };
    };
    ServerModelHelper.generateFormComponentFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                console.log(config_1.ConfigHelper.magicBoxPath);
                config_1.ConfigHelper.generateFolders([config_1.ConfigHelper.magicBoxPath]);
                var instance = _this.getIntance();
                var printableFormName = config_1.ConfigHelper.getPrintableString(instance._formName);
                var formNameFilePathStr = config_1.ConfigHelper.getFileNameString(printableFormName);
                var folders = [];
                if (instance._generatingForModule) {
                    var generatedModulePath = path_1.join(instance._projectMagicBoxPath, config_1.ConfigHelper.getFileNameString(instance._moduleName));
                    folders = [
                        generatedModulePath,
                        path_1.join(generatedModulePath, formNameFilePathStr),
                    ];
                }
                else {
                    folders = [
                        path_1.join(instance._projectMagicBoxPath, formNameFilePathStr)
                    ];
                }
                config_1.ConfigHelper.generateFolders(folders);
                var templatesNames = Object.keys(instance._projectFormTemplatesPaths);
                var templatesLen = templatesNames.length;
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesNames[i];
                    var source = fs_extra_1.readFileSync(instance._projectFormTemplatesPaths[templateName], { encoding: 'utf-8' });
                    var compiledTemplate = handlebars_1.compile(source)(_this.getStandAloneFormGenerationData());
                    var filePath = void 0;
                    var rootPath = (instance._generatingForModule) ? path_1.join(instance._projectMagicBoxPath, config_1.ConfigHelper.getFileNameString(instance._moduleName)) : instance._projectMagicBoxPath;
                    switch (templateName) {
                        case 'styles':
                            filePath = path_1.join(rootPath, formNameFilePathStr, formNameFilePathStr + ".component.scss");
                            break;
                        case 'html':
                            filePath = path_1.join(rootPath, formNameFilePathStr, formNameFilePathStr + ".component.html");
                            break;
                        default:
                            filePath = path_1.join(rootPath, formNameFilePathStr, formNameFilePathStr + ".component.ts");
                            break;
                    }
                    fs_extra_1.writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    /* -------------------------------------------------------------------------- */
    /*                             Crud Module related                            */
    /* -------------------------------------------------------------------------- */
    ServerModelHelper.prototype._moduleNamePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var defaultName = config_1.ConfigHelper.UcFirst(_this._selectedModelName + ' crud');
            config_1.ConfigHelper.simpletInputPrompt('Module name', defaultName)
                .then(function (result) {
                if (!result) {
                    result = defaultName;
                }
                _this._moduleName = config_1.ConfigHelper.getPrintableString(result);
                _this._generatingForModule = true;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.moduleNamePrompt = function () {
        return ServerModelHelper.getIntance()._moduleNamePrompt();
    };
    ServerModelHelper.prototype._moduleEntryTypePrompt = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt([
                {
                    name: 'type',
                    message: 'What kind of data entry would you like to have',
                    type: 'list',
                    choices: [
                        {
                            value: 'page',
                            name: 'Dedicated pages for update and creating entries'
                        },
                        {
                            value: 'modal',
                            name: 'Modal dialog for update and creating entries'
                        },
                    ]
                }
            ])
                .then(function (result) {
                _this._moduleEntryType = result.type;
                return resolve();
            })
                .catch(function (err) {
                return reject(err);
            });
        });
    };
    ServerModelHelper.moduleEntryTypePrompt = function () {
        return ServerModelHelper.getIntance()._moduleEntryTypePrompt();
    };
    ServerModelHelper.getStandAloneModuleGenerationData = function () {
        var instance = this.getIntance();
        var _selectedModelName = instance._selectedModelName, _formName = instance._formName, _moduleEntryType = instance._moduleEntryType, _moduleName = instance._moduleName, _tableName = instance._tableName;
        return {
            modelName: _selectedModelName,
            formName: _formName,
            tableName: _tableName,
            moduleName: _moduleName,
            entryType: _moduleEntryType,
            pageEntry: (_moduleEntryType == 'page'),
            modalEntry: (_moduleEntryType == 'modal'),
        };
    };
    ServerModelHelper.generateModuleFiles = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                var instance = _this.getIntance();
                var moduleNameFilePathStr = config_1.ConfigHelper.getFileNameString(instance._moduleName);
                var generatedModulePath = path_1.join(instance._projectMagicBoxPath, moduleNameFilePathStr);
                config_1.ConfigHelper.generateFolders([generatedModulePath]);
                var templatesNames = Object.keys(instance._projectModuleTemplatesPaths);
                var templatesLen = templatesNames.length;
                for (var i = 0; i < templatesLen; i++) {
                    var templateName = templatesNames[i];
                    var source = fs_extra_1.readFileSync(instance._projectModuleTemplatesPaths[templateName], { encoding: 'utf-8' });
                    var compiledTemplate = handlebars_1.compile(source)(_this.getStandAloneModuleGenerationData());
                    var filePath = void 0;
                    switch (templateName) {
                        case 'styles':
                            filePath = path_1.join(generatedModulePath, moduleNameFilePathStr + ".component.scss");
                            break;
                        case 'html':
                            filePath = path_1.join(generatedModulePath, moduleNameFilePathStr + ".component.html");
                            break;
                        case 'module':
                            filePath = path_1.join(generatedModulePath, moduleNameFilePathStr + ".module.ts");
                            break;
                        case 'routing':
                            filePath = path_1.join(generatedModulePath, moduleNameFilePathStr + "-routing.module.ts");
                            break;
                        default:
                            filePath = path_1.join(generatedModulePath, moduleNameFilePathStr + ".component.ts");
                            break;
                    }
                    fs_extra_1.writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });
                }
                return resolve();
            }
            catch (error) {
                return reject(error);
            }
        });
    };
    return ServerModelHelper;
}());
exports.ServerModelHelper = ServerModelHelper;
