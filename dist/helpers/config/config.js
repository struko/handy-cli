"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var InnerHelper;
var ConfigHelper = /** @class */ (function () {
    function ConfigHelper() {
        this._cwd = path_1.join(process.cwd() + '/');
        this._resolveRoot();
        this._resolveNodeVersion();
    }
    Object.defineProperty(ConfigHelper, "isInHandyProject", {
        get: function () {
            return this.getIntance()._isInHandyProject;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "cwd", {
        get: function () {
            return this.getIntance()._cwd;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectRootDir", {
        get: function () {
            return this.getIntance()._projectRootDir;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectClientWebAppDir", {
        get: function () {
            return this.getIntance()._projectClientWebAppDir;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectEnv", {
        get: function () {
            return this.getIntance()._projectEnv;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "nodeVersion", {
        get: function () {
            return this.getIntance()._nodeVersion;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "nodeMainVersion", {
        get: function () {
            return this.getIntance()._nodeMainVersion;
        },
        enumerable: true,
        configurable: true
    });
    ConfigHelper.getIntance = function () {
        if (!InnerHelper) {
            InnerHelper = new ConfigHelper();
        }
        return InnerHelper;
    };
    ConfigHelper.UcFirst = function (str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    ConfigHelper.LcFirst = function (str) {
        return str.charAt(0).toLowerCase() + str.slice(1);
    };
    ConfigHelper.projectCliTemplatesPath = function () {
        return path_1.join(this.getIntance()._projectRootDir, '/src/server/handy/core/defaults/cli/templates');
    };
    ConfigHelper.prototype._resolveRoot = function (path) {
        if (path === void 0) { path = this._cwd; }
        if (path === this._lastCheckedPath) {
            this._isInHandyProject = false;
            return;
        }
        this._lastCheckedPath = path;
        var content = fs_extra_1.readdirSync(path);
        var contentLen = content.length;
        var isRoot = false;
        for (var i = 0; i < contentLen; i++) {
            var entityName = content[i];
            if (entityName === 'handy.json') {
                isRoot = true;
                this._isInHandyProject = true;
                this._projectRootDir = path;
                this._projectClientWebAppDir = path_1.join(path, 'src/client/web');
                this._projectEnv = 'dev';
                if (content.includes('prod.flag')) {
                    this._projectEnv = 'prod';
                    break;
                }
                if (content.includes('stag.flag')) {
                    this._projectEnv = 'stag';
                    break;
                }
                break;
            }
        }
        if (!isRoot) {
            this._resolveRoot(path_1.join(path, '../'));
        }
    };
    ConfigHelper.prototype._resolveNodeVersion = function () {
        var version = child_process_1.execSync('node -v').toString();
        this._nodeVersion = version;
        this._nodeMainVersion = parseInt(version.split('.')[0]);
    };
    return ConfigHelper;
}());
exports.ConfigHelper = ConfigHelper;
