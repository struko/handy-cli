"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
var chalk_1 = require("chalk");
var orange = chalk_1.keyword('orange');
var cli_spinner_1 = require("cli-spinner");
var Logger = /** @class */ (function () {
    function Logger() {
    }
    Logger.info = function (msg) {
        console.log(chalk_1.blue(msg));
    };
    Logger.success = function (msg) {
        console.log(chalk_1.green(msg));
    };
    Logger.error = function (msg) {
        console.log(chalk_1.red.bold("! Error"));
        console.log(chalk_1.red(msg));
    };
    Logger.warning = function (msg) {
        console.log(orange.bold("! Warning"));
        console.log(orange(msg));
    };
    Logger.spinner = function (msg) {
        var spin = new cli_spinner_1.Spinner({
            text: "%s " + msg,
            stream: process.stderr,
        });
        spin.setSpinnerString('|/-\\');
        return spin;
    };
    Logger.notHandyProject = function () {
        return this.error('Looks like you are not executing this command in handy project directory');
    };
    return Logger;
}());
exports.Logger = Logger;
