"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigHelper = void 0;
var child_process_1 = require("child_process");
var path_1 = require("path");
var fs_extra_1 = require("fs-extra");
var inquirer_1 = require("inquirer");
var logger_1 = require("./logger");
var https_1 = require("https");
var querystring_1 = require("querystring");
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var InnerHelper;
var ConfigHelper = /** @class */ (function () {
    function ConfigHelper() {
        this._cwd = path_1.join(process.cwd() + '/');
        this._resolveRoot();
        this._resolveNodeVersion();
    }
    Object.defineProperty(ConfigHelper, "isInHandyProject", {
        get: function () {
            return this.getIntance()._isInHandyProject;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "cwd", {
        get: function () {
            return this.getIntance()._cwd;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectRootDir", {
        get: function () {
            return this.getIntance()._projectRootDir;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectClientWebAppDir", {
        get: function () {
            return this.getIntance()._projectClientWebAppDir;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "projectEnv", {
        get: function () {
            return this.getIntance()._projectEnv;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "nodeVersion", {
        get: function () {
            return this.getIntance()._nodeVersion;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "nodeMainVersion", {
        get: function () {
            return this.getIntance()._nodeMainVersion;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "magicBoxPath", {
        get: function () {
            return path_1.join(this.projectRootDir, 'magic-box');
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(ConfigHelper, "crdFile", {
        get: function () {
            return path_1.join(__dirname, '../../', 'cr.json');
        },
        enumerable: false,
        configurable: true
    });
    ConfigHelper.getHubCreds = function () {
        var creds = fs_extra_1.readJSONSync(ConfigHelper.crdFile);
        return creds;
    };
    ConfigHelper.isLoggedIn = function () {
        return ConfigHelper.getHubCreds().loggedIn;
    };
    ConfigHelper.hubLogin = function () {
        var _this = this;
        inquirer_1.prompt([
            {
                message: 'Email',
                name: 'email',
                type: 'input',
            },
            {
                message: 'Password',
                name: 'password',
                type: 'password',
            },
        ])
            .then(function (answers) {
            var email = answers.email, password = answers.password;
            if (email) {
                email = email.toLowerCase().trim();
            }
            https_1.get(_this.hubUrl + "api/v1/service/cliAuth/login?" + querystring_1.stringify({ email: email, password: password }), function (response) {
                var data = '';
                // A chunk of data has been recieved.
                response.on('data', function (chunk) {
                    data += chunk;
                });
                // The whole response has been received. Print out the result.
                response.on('end', function () {
                    var resResult = JSON.parse(data);
                    if (!resResult.success) {
                        console.error(resResult);
                        return;
                    }
                    var finalData = { act: resResult.data.accessTokenData, rft: resResult.data.refreshTokenData, email: email, loggedIn: true };
                    fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
                    logger_1.Logger.success('Logged in');
                    return;
                });
            }).on('error', function (reqErr) {
                var finalData = { loggedIn: false };
                fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
                console.error(reqErr);
                return;
            });
        })
            .catch(function (err) {
            var finalData = { loggedIn: false };
            fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
            console.error(err);
            return;
        });
    };
    ConfigHelper.hubLogout = function () {
        var finalData = { loggedIn: false };
        fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
        logger_1.Logger.success('Logged out');
    };
    ConfigHelper.getAccessToken = function () {
        if (!ConfigHelper.isLoggedIn()) {
            return Promise.reject('You have to login first');
        }
        var now = new Date().getTime() + (1000 * 60 * 60);
        var credsData = ConfigHelper.getHubCreds();
        var _a = credsData.act, token = _a.token, expiryMoment = _a.expiryMoment;
        if (expiryMoment > now) {
            return Promise.resolve(token);
        }
        return ConfigHelper.refreshToken();
    };
    ConfigHelper.refreshToken = function () {
        if (!ConfigHelper.isLoggedIn()) {
            return Promise.reject('You have to login first');
        }
        var now = new Date().getTime() + (1000 * 60 * 60);
        var credsData = ConfigHelper.getHubCreds();
        var _a = credsData.rft, token = _a.token, expiryMoment = _a.expiryMoment;
        if (expiryMoment < now) {
            fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
            return Promise.reject('Refresh token expired. Log in again');
        }
        return new Promise(function (resolve, reject) {
            var socketInstance = socket_io_client_1.default("" + ConfigHelper.hubUrl);
            socketInstance.once("cli_token_refresh", function (payload) {
                socketInstance.disconnect();
                if (payload.success) {
                    var finalData = { act: payload.accessTokenData, rft: payload.refreshTokenData, loggedIn: true };
                    fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
                    return resolve(payload.accessTokenData.token);
                }
                else {
                    fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
                    return reject('Token refreshing error. Log in again');
                }
            });
            socketInstance.once("cli_token_refresh_failed", function (payload) {
                socketInstance.disconnect();
                fs_extra_1.writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
                return reject('Token refreshing error. Log in again');
            });
            var event = {
                accessToken: null,
                eventData: { email: credsData.email, rft: token }
            };
            socketInstance.emit('cliTokenRefresh', event);
        });
    };
    ConfigHelper.testRefresh = function () {
        ConfigHelper.getAccessToken().then(function (result) {
            console.log({ result: result });
        })
            .catch(function (err) {
            console.log({ err: err });
        });
    };
    ConfigHelper.getIntance = function () {
        if (!InnerHelper) {
            InnerHelper = new ConfigHelper();
        }
        return InnerHelper;
    };
    ConfigHelper.UcFirst = function (str) {
        if (!str) {
            return 'undefined';
        }
        return str.charAt(0).toUpperCase() + str.slice(1);
    };
    ConfigHelper.LcFirst = function (str) {
        if (!str) {
            return 'undefined';
        }
        return str.charAt(0).toLowerCase() + str.slice(1);
    };
    ConfigHelper.getFileNameString = function (str) {
        var result = '';
        var chars = this.LcFirst(str).split('');
        var charsLen = chars.length;
        for (var i = 0; i < charsLen; i++) {
            var singleChar = chars[i];
            if (singleChar.toUpperCase() === singleChar) {
                result += "-" + singleChar.toLowerCase();
            }
            else {
                result += singleChar;
            }
        }
        return this.LcFirst(result.replace(/ /g, ''));
    };
    ConfigHelper.projectCliTemplatesPath = function () {
        return path_1.join(this.getIntance()._projectRootDir, '/src/cli/templates');
    };
    ConfigHelper.simpleYesNoPrompt = function (message, defaultVal) {
        if (defaultVal === void 0) { defaultVal = true; }
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt({
                message: message,
                name: 'result',
                type: 'list',
                choices: [
                    {
                        name: 'Yes',
                        value: true
                    },
                    {
                        name: 'No',
                        value: false
                    }
                ],
                default: defaultVal
            })
                .then(function (answers) {
                return resolve(answers.result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ConfigHelper.simpletInputPrompt = function (message, defaultVal) {
        return new Promise(function (resolve, reject) {
            inquirer_1.prompt({
                message: message,
                name: 'result',
                type: 'input',
                default: defaultVal
            })
                .then(function (answers) {
                return resolve(answers.result);
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    ConfigHelper.getPrintableString = function (value) {
        if (value === void 0) { value = 'unknown'; }
        value = value.trim();
        var splitted = value.split(' ');
        var splittedLen = splitted.length;
        var result = '';
        for (var i = 0; i < splittedLen; i++) {
            result += this.UcFirst(splitted[i]).trim();
        }
        return this.LcFirst(result.replace(/ /g, ''));
    };
    ConfigHelper.generateFolders = function (foldersPaths) {
        var foldersLen = foldersPaths.length;
        for (var i = 0; i < foldersLen; i++) {
            fs_extra_1.ensureDirSync(foldersPaths[i]);
        }
    };
    ConfigHelper.getPredeployData = function () {
        var rootPath = this.getIntance()._projectRootDir;
        var deployFilePath = path_1.join(rootPath, 'src/cli/deploy/deploy.config.json');
        var deployData = (fs_extra_1.existsSync(deployFilePath)) ? fs_extra_1.readJSONSync(deployFilePath) : { handyAppId: null };
        var handyConfigFilePath = path_1.join(rootPath, 'handy.json');
        var handyConfigData = fs_extra_1.readJSONSync(handyConfigFilePath);
        var _a = handyConfigData.domain, domain = _a === void 0 ? {} : _a, _b = handyConfigData.serverPort, serverPort = _b === void 0 ? {} : _b, projectName = handyConfigData.projectName, superAdminEmail = handyConfigData.superAdminEmail;
        var repo = child_process_1.execSync("cd " + path_1.join(rootPath) + " && git config --get remote.origin.url").toString('utf-8');
        var repoExtractReg = new RegExp(/https:\/\/(.*).git/);
        // @ts-ignore 
        var extract = repo.match(repoExtractReg)[1];
        // @ts-ignore 
        repo = "https://" + repo.match(repoExtractReg)[1] + ".git";
        if (repo.includes('@') && !extract.includes(':')) {
            var splitted = repo.split('@');
            splitted.shift();
            repo = "https://" + splitted.join('@');
        }
        console.log(repo);
        var branch = child_process_1.execSync("cd " + path_1.join(rootPath) + " && git branch").toString('utf-8');
        var branches = branch.split('\n');
        var branchesLen = branches.length;
        for (var i = 0; i < branchesLen; i++) {
            var singleBranch = branches[i];
            if (singleBranch.includes('*')) {
                var splittedBranch = singleBranch.split(' ');
                branch = splittedBranch[1];
                break;
            }
        }
        var result = {
            name: projectName.val,
            superAdminEmail: {
                prod: superAdminEmail.val,
                stag: (superAdminEmail.stagVal) ? superAdminEmail.stagVal : superAdminEmail.val,
            },
            handyAppId: deployData.handyAppId,
            domains: {
                prod: domain.val,
                stag: domain.stagVal,
            },
            ports: {
                prod: serverPort.val,
                stag: serverPort.stagVal,
            },
            repo: repo,
            branch: branch
        };
        return result;
    };
    ConfigHelper.saveHandyAppId = function (handyAppId) {
        var rootPath = this.getIntance()._projectRootDir;
        var deployFilePath = path_1.join(rootPath, 'src/cli/deploy/deploy.config.json');
        fs_extra_1.ensureDirSync(path_1.join(rootPath, 'src/cli/deploy/'));
        fs_extra_1.writeFile(deployFilePath, JSON.stringify({ handyAppId: handyAppId }, null, 2))
            .then(function (result) {
            logger_1.Logger.success('Handy app id was saved');
        })
            .catch(function (err) {
            logger_1.Logger.error('Handy app id saving failed');
            console.log(err);
        });
    };
    ConfigHelper.prototype._resolveRoot = function (path) {
        if (path === void 0) { path = this._cwd; }
        if (path === this._lastCheckedPath) {
            this._isInHandyProject = false;
            return;
        }
        this._lastCheckedPath = path;
        var content = fs_extra_1.readdirSync(path);
        var contentLen = content.length;
        var isRoot = false;
        for (var i = 0; i < contentLen; i++) {
            var entityName = content[i];
            if (entityName === 'handy.json') {
                isRoot = true;
                this._isInHandyProject = true;
                this._projectRootDir = path;
                this._projectClientWebAppDir = path_1.join(path, 'src/client/web');
                this._projectEnv = 'dev';
                if (content.includes('prod.flag')) {
                    this._projectEnv = 'prod';
                    break;
                }
                if (content.includes('stag.flag')) {
                    this._projectEnv = 'stag';
                    break;
                }
                break;
            }
        }
        if (!isRoot) {
            this._resolveRoot(path_1.join(path, '../'));
        }
    };
    ConfigHelper.prototype._resolveNodeVersion = function () {
        var version = child_process_1.execSync('node -v').toString();
        this._nodeVersion = version;
        this._nodeMainVersion = parseInt(version.split('.')[0].replace('v', ''));
    };
    ConfigHelper.updatePorts = function (stag, prod) {
        logger_1.Logger.info('Updating app ports');
        var handyJsonPath = path_1.join(this.projectRootDir, 'handy.json');
        var configData = fs_extra_1.readJSONSync(handyJsonPath);
        configData.serverPort.val = prod;
        configData.serverPort.stagVal = stag;
        fs_extra_1.writeFileSync(handyJsonPath, JSON.stringify(configData, null, 2));
        return;
    };
    ConfigHelper.updateNginxConf = function (appData) {
        logger_1.Logger.info('Updating Nginx conf files');
        var stagConfPath = path_1.join(this.projectRootDir, 'stag.nginx.conf');
        var prodConfPath = path_1.join(this.projectRootDir, 'prod.nginx.conf');
        var stagConf = fs_extra_1.readFileSync(stagConfPath, { encoding: 'utf-8' });
        var prodConf = fs_extra_1.readFileSync(prodConfPath, { encoding: 'utf-8' });
        var handyJsonPath = path_1.join(this.projectRootDir, 'handy.json');
        var maxUploadData = fs_extra_1.readJSONSync(handyJsonPath).fileUpload;
        var prodMaxUpload = maxUploadData.val.maxFileSizeInMB;
        var stagMaxUpload = maxUploadData.val.maxFileSizeInMB;
        if (maxUploadData.stagVal && maxUploadData.stagVal.maxFileSizeInMB) {
            stagMaxUpload = maxUploadData.stagVal.maxFileSizeInMB;
        }
        var stagDomain = appData.stagDomain, prodDomain = appData.prodDomain, stagPort = appData.stagPort, prodPort = appData.prodPort;
        var domainReplacerReg = new RegExp(/server_name(.*);#domain/);
        var portReplacerReg = new RegExp(/localhost:(.*);#port/);
        var uploadReplacerReg = new RegExp(/client_max_body_size(.*);#maxUpload/);
        var wwwDomainReplacerReg = new RegExp(/server_name(.*);#wwwDomain/);
        var wwwSchemeReplacerReg = new RegExp(/\$scheme(.*);#domainScheme/);
        stagConf = stagConf.replace(domainReplacerReg, "server_name " + stagDomain + ";#domain");
        stagConf = stagConf.replace(portReplacerReg, "localhost:" + stagPort + ";#port");
        stagConf = stagConf.replace(uploadReplacerReg, "client_max_body_size " + stagMaxUpload + "M;#maxUpload");
        stagConf = stagConf.replace(wwwDomainReplacerReg, "server_name www." + stagDomain + ";#wwwDomain");
        stagConf = stagConf.replace(wwwSchemeReplacerReg, "$scheme://" + stagDomain + "$request_uri;#domainScheme");
        fs_extra_1.writeFileSync(stagConfPath, stagConf, { encoding: 'utf-8' });
        prodConf = prodConf.replace(domainReplacerReg, "server_name " + prodDomain + ";#domain");
        prodConf = prodConf.replace(portReplacerReg, "localhost:" + prodPort + ";#port");
        prodConf = prodConf.replace(uploadReplacerReg, "client_max_body_size " + prodMaxUpload + "M;#maxUpload");
        prodConf = prodConf.replace(wwwDomainReplacerReg, "server_name www." + prodDomain + ";#wwwDomain");
        prodConf = prodConf.replace(wwwSchemeReplacerReg, "$scheme://" + prodDomain + "$request_uri;#domainScheme");
        fs_extra_1.writeFileSync(prodConfPath, prodConf, { encoding: 'utf-8' });
        return;
    };
    ConfigHelper.addRepoToJsons = function (repo) {
        logger_1.Logger.info('Parsing repository');
        var handyJsonPath = path_1.join(this.projectRootDir, 'handy.json');
        var handyData = fs_extra_1.readJSONSync(handyJsonPath);
        var credsReg = new RegExp(/https(.*)@/);
        if (repo.includes('@')) {
            repo = "https://" + repo.replace(credsReg, '');
        }
        handyData.repo = {
            val: repo
        };
        fs_extra_1.writeFileSync(handyJsonPath, JSON.stringify(handyData, null, 2));
        var packageJsonPath = path_1.join(this.projectRootDir, 'package.json');
        var packageJsonData = fs_extra_1.readJSONSync(packageJsonPath);
        packageJsonData.repository = {
            type: 'git',
            url: "git+" + repo
        };
        fs_extra_1.writeFileSync(packageJsonPath, JSON.stringify(packageJsonData, null, 2));
        return;
    };
    ConfigHelper.buildApp = function () {
        var _this = this;
        // return Promise.resolve();
        return new Promise(function (resolve, reject) {
            logger_1.Logger.info('Building app, might take a while ;)');
            var child = child_process_1.exec("cd " + _this.projectRootDir + " && npm run build");
            // @ts-ignore
            child.stdout.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.stderr.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.on('exit', function (code) {
                return resolve();
            });
        });
    };
    ConfigHelper.pushRepo = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            logger_1.Logger.info('Pushing repo');
            var child = child_process_1.exec("cd " + _this.projectRootDir + " && git add . && git commit -m \"Pre deploy\" && git push");
            // @ts-ignore
            child.stdout.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.stderr.on('data', function (data) {
                console.log(data.toString());
            });
            // @ts-ignore
            child.on('exit', function (code) {
                return resolve();
            });
        });
    };
    ConfigHelper.hubUrl = 'https://handyapps.dev/';
    return ConfigHelper;
}());
exports.ConfigHelper = ConfigHelper;
// let { name, repo, handyAppId, stagPort, prodPort } = appData;
