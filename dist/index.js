#!/usr/bin/env node
/**
 * index.ts
 *
 * Handles all cli commands
 */
'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = require("path");
var commander_1 = require("commander");
var fs_extra_1 = require("fs-extra");
require('./helpers/hbs');
/* ---------------------------- Version asigning ---------------------------- */
var CLI_VERSION = JSON.parse(fs_extra_1.readFileSync(path_1.join(__dirname, '../package.json'), 'utf-8')).version;
commander_1.program.version(CLI_VERSION, '-v, --vers', 'output the current version');
/* -------------------------------------------------------------------------- */
/*                           New project generating                           */
/* -------------------------------------------------------------------------- */
var new_1 = require("./commands/new/new");
commander_1.program
    .command('new')
    .alias('n')
    .description('Generates new handy project')
    .action(function () {
    var CommandHandler = new new_1.NewProjectCommand();
    CommandHandler.command();
});
/* -------------------------------------------------------------------------- */
/*                             Development running                            */
/* -------------------------------------------------------------------------- */
var run_1 = require("./commands/run/run");
commander_1.program
    .command('run')
    .alias('r')
    .description('Runs Handy project in development base on selected option')
    .action(function () {
    var CommandHandler = new run_1.RunCommand();
    CommandHandler.command();
});
/* -------------------------------------------------------------------------- */
/*                             Development running                            */
/* -------------------------------------------------------------------------- */
var generate_1 = require("./commands/generate/generate");
commander_1.program
    .command('generate')
    .alias('g')
    .option('--v --verify', 'Shows files to generate and asks for verifications before generating. Simmilar to ng "--dry-run"')
    .description('Generates handy project files, like modules, models, services, validators....')
    .action(function (params) {
    var _a = params.verify, verify = _a === void 0 ? false : _a;
    var CommandHandler = new generate_1.GenerateCommand(verify);
    CommandHandler.command();
});
/* -------------------------------------------------------------------------- */
/*                             Login                                          */
/* -------------------------------------------------------------------------- */
var config_1 = require("./helpers/config");
commander_1.program
    .command('login')
    .description('Log to handy hub')
    .action(function () {
    config_1.ConfigHelper.hubLogin();
});
/* -------------------------------------------------------------------------- */
/*                             Logout                                         */
/* -------------------------------------------------------------------------- */
commander_1.program
    .command('logout')
    .description('Log out from handy hub')
    .action(function () {
    config_1.ConfigHelper.hubLogout();
});
/* -------------------------------------------------------------------------- */
/*                             Deploy                                         */
/* -------------------------------------------------------------------------- */
var deploy_1 = require("./commands/deploy/deploy");
commander_1.program
    .command('deploy')
    .alias('d')
    .description('Deploys app to server')
    .action(function () {
    var CommandHandler = new deploy_1.DeployCommand();
    CommandHandler.command();
});
/* -------------------------------------------------------------------------- */
/*                             Update                                         */
/* -------------------------------------------------------------------------- */
var update_1 = require("./commands/update-project/update");
commander_1.program
    .command('update-project')
    .alias('u-p')
    .description('Updates handy project to latest version')
    .action(function () {
    var CommandHandler = new update_1.UpdateProjectCommand();
    CommandHandler.command();
});
/* -------------------------------------------------------------------------- */
/*                             Update -cli                                         */
/* -------------------------------------------------------------------------- */
var update_2 = require("./commands/update-cli/update");
commander_1.program
    .command('update')
    .alias('u')
    .description('Updates handy cli')
    .action(function () {
    var CommandHandler = new update_2.UpdateCliCommand();
    CommandHandler.command();
});
var save_exact_deps_1 = require("./commands/save-exact-dependencies/save-exact-deps");
commander_1.program
    .command('save-exact-deps')
    .description('Pulls installed dependencies versions and updates the package.json accordingly')
    .action(function () {
    var CommandHandler = new save_exact_deps_1.SaveExactDepsCommand();
    CommandHandler.command();
});
/* -------------------------- Listening for command ------------------------- */
commander_1.program.parse(process.argv);
