# Handy cli #

## Install ##
[Video tutorial](https://youtu.be/_Oqn2_1vWJE){:target="_blank"}

Clone the repo to your device, make sure you won't remove/replace/move the folder you clone it to.
cd to cloned folder and 

```npm link```

## Help ##
```handy -h```

## Update ##
Regular npm update handy wont work!!

```handy update```