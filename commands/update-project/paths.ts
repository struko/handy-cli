export type PathsList = 'serverCore' | 'serverTypes' | 'clientCore' | 
  'chartsModule' | 'formsModule' | 'dataTableModule' | 'btns' | 
  'confirmClickDialog' | 'fileThumb' | 'formActionsBar' | 
  'globalLoader' | 'handyExpander' | 'handyIcon' | 'trackingModule' |
  'handyNavlayot' | 'notifications' | 'trackingClientService' | 'trackingServerNonDefaultService'; 

export const srcPaths: { [key in PathsList]: string } = {
  serverCore: 'src/server/handy/core',
  serverTypes: 'src/server/handy/types',
  clientCore: 'src/client/web/src/app/handy/core',
  chartsModule: 'src/client/web/src/app/handy/modules/handy-charts',
  trackingModule: 'src/client/web/src/app/handy/modules/handy-tracking',
  formsModule: 'src/client/web/src/app/handy/modules/handy-form',
  dataTableModule: 'src/client/web/src/app/handy/modules/handy-table',
  btns: 'src/client/web/src/app/modules/shared/components/buttons',
  confirmClickDialog: 'src/client/web/src/app/modules/shared/components/confirm-click-dialog',
  fileThumb: 'src/client/web/src/app/modules/shared/components/file-thumb',
  formActionsBar: 'src/client/web/src/app/modules/shared/components/form-actions-bar',
  globalLoader: 'src/client/web/src/app/modules/shared/components/global-loader',
  handyExpander: 'src/client/web/src/app/modules/shared/components/handy-expander',
  handyIcon: 'src/client/web/src/app/modules/shared/components/handy-icon',
  handyNavlayot: 'src/client/web/src/app/modules/shared/components/handy-nav-layout',
  notifications: 'src/client/web/src/app/modules/shared/components/notifications',
  trackingClientService: 'src/client/web/src/app/handy/services/handy-ng-tracking.service.ts',
  trackingServerNonDefaultService: 'src/server/services/handy-services/handy-tracking/handy-tracking.service.ts',
}

export const srcPathsList: string[] = Object.keys(srcPaths);