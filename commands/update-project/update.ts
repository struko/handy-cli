import { exec } from 'child_process';
import { prompt, QuestionCollection } from 'inquirer';
import { join } from 'path';
import { writeFileSync, readFileSync, existsSync, copySync, promises } from 'fs-extra';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';
import { PathsList, srcPaths, srcPathsList } from './paths';

export class UpdateProjectCommand {

  private _baseRepo: string = 'https://bitbucket.org/struko/handy-base.git';

  private _tempPath: string = join(__dirname, 'tmp');
  private _tempHandyJsonPath: string = join(this._tempPath, 'handy.json');

  private _answers: Answers;
  private _questions: QuestionCollection = [
    {
      message: 'What would you like to update',
      type: 'checkbox',
      name: 'toUpdate',
      choices: [
        {
          name: 'Server Core',
          value: 'serverCore',
          checked: true,
        },
        {
          name: 'Server Types',
          value: 'serverTypes',
          checked: true,
        },
        {
          name: 'Client Core',
          value: 'clientCore',
          checked: true,
        },
        {
          name: 'Charts module (client)',
          value: 'chartsModule',
          checked: true,
        },
        {
          name: 'Forms module (client)',
          value: 'formsModule',
          checked: true,
        },
        {
          name: 'DataTale module (client)',
          value: 'dataTableModule',
          checked: true,
        },
        {
          name: 'Tracking module (client)',
          value: 'trackingModule',
          checked: true,
        },
      ]
    }
  ];

  private _answersComponents: Answers;
  private _questionsComponents: QuestionCollection = [
    {
      message: 'Which components would you like to update',
      type: 'checkbox',
      name: 'toUpdate',
      choices: [
        {
          name: 'Buttons',
          value: 'btns',
          checked: true,
        },
        {
          name: 'Confirm click dialog',
          value: 'confirmClickDialog',
          checked: true,
        },
        {
          name: 'File thumb',
          value: 'fileThumb',
          checked: true,
        },
        {
          name: 'Form actions bar',
          value: 'formActionsBar',
          checked: true,
        },
        {
          name: 'Global loader',
          value: 'globalLoader',
          checked: true,
        },
        {
          name: 'Handy expander',
          value: 'handyExpander',
          checked: true,
        },
        {
          name: 'Handy icon',
          value: 'handyIcon',
          checked: true,
        },
        {
          name: 'Notifications',
          value: 'notifications',
          checked: true,
        },
      ]
    }
  ];

  private _checkedUpdates: PathsList[] = [];

  constructor () {

  }

  public command(): void {

    if (!ConfigHelper.isInHandyProject) {
      Logger.error(`You are not in Handy project`);
      return;
    }

    this._prompt()
      .then(() => {
        return this._prompt('components');
      })
      .then(() => {

        this._checkedUpdates = [...this._answers.toUpdate, ...this._answersComponents.toUpdate];
        if (this._checkedUpdates.length < 1) {
          return Promise.reject('Nothing selected to update')
        }

        return this._commitGit();

      })
      .then(() => {
        return this._cloneBase();
      })
      .then(() => {
        return this._runUpdate();
      })
      .then(() => {

        // Just to add empty space in log
        console.log();
        Logger.success('Project updated');

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(type: 'base' | 'components' = 'base'): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<Answers>(type === 'base' ? this._questions : this._questionsComponents)
        .then(answers => {

          if (type === 'base') {
            this._answers = answers;
          } else {
            this._answersComponents = answers;
          }

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _cloneBase(): Promise<void> {

    let spinner = Logger.spinner('Cloning Handy base...');

    return new Promise((resolve, reject) => {

      spinner.start();

      let command: string = `git clone ${this._baseRepo} ${this._tempPath}`;

      if (existsSync(this._tempHandyJsonPath)) {
        command = `cd ${this._tempPath} && git stash && git pull`;
      }

      exec(command, (err, stdout, stderr) => {

        spinner.stop(true);

        if (err) {

          reject(err);
          return;

        }

        Logger.success('Handy base cloned');
        return resolve();

      })

    })

  }

  private _commitGit(): Promise<void> {

    let spinner = Logger.spinner('Making repo "Pre HANDY update" comit...');

    let notesFilePath: string = join(ConfigHelper.projectRootDir, 'notes');
    let notesContent: string = readFileSync(notesFilePath, { encoding: 'utf-8' }) + `\n\nProject updated via CLI at ${new Date().toUTCString()}`;
    writeFileSync(notesFilePath, notesContent, { encoding: 'utf-8' });

    return new Promise((resolve, reject) => {

      spinner.start();

      let command = `cd ${ConfigHelper.projectRootDir} && git add . && git commit -m "Pre HANDY update"`;

      exec(command, (err, stdout, stderr) => {

        spinner.stop(true);

        if (err) {
          reject(err);
          return;

        }

        Logger.success(`"Pre HANDY update" comitted`)
        return resolve();

      })

    })

  }

  private _runUpdate(): Promise<any> {

    // Just to add empty space in log
    console.log();

    let promisesArr: Promise<any>[] = [];

    let updatesLen: number = this._checkedUpdates.length;
    for (let i = 0; i < updatesLen; i++) {
      const singleUpdate = this._checkedUpdates[i];
      promisesArr.push(this._updateSingleEntity(singleUpdate));

      if (singleUpdate === 'trackingModule') {
        promisesArr.push(this.__sortTrackingService());
      }

    }

    return Promise.all(promisesArr);

  }

  private __sortTrackingService(): Promise<void> {

    Logger.warning('Make sure to update NON default handy layout client service and import Tracking server service to handy module on server');

    return new Promise((resolve, reject) => {

      let promisesArr: Promise<void>[] = [];

      promisesArr.push(this._updateSingleEntity('trackingClientService', false));
      promisesArr.push(this._updateSingleEntity('trackingServerNonDefaultService', false));
      promisesArr.push(this.__ensureIndexFilesExportsForTracking());
      promisesArr.push(this.__ensureHandyConfigTrackingEntries());

      Promise.all(promisesArr).then(() => {
        resolve();
      })
        .catch(err => {
          reject(err);
        })

    })

  }

  private __ensureIndexFilesExportsForTracking(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        let servicesIndexStringPath: string = 'src/client/web/src/app/handy/services/index.ts';
        let clientServicesPath: string = join(ConfigHelper.projectRootDir, servicesIndexStringPath);

        let searchLine: string = `export * from './handy-ng-tracking.service';`;
        let clientIndexContent: string = readFileSync(clientServicesPath, { encoding: 'utf-8' });

        if (!clientIndexContent.includes(searchLine)) {

          clientIndexContent += `\n\n${searchLine}`;
          writeFileSync(clientServicesPath, clientIndexContent, { encoding: 'utf-8' });
          Logger.success(`Updated: ${clientServicesPath.replace(ConfigHelper.projectRootDir, '')}`);

        }

        resolve();

      } catch (error) {
        reject(error);
      }

    })

  }

  private __ensureHandyConfigTrackingEntries(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        let handyJsonPath: string = join(ConfigHelper.projectRootDir, 'handy.json');
        let handyJsonData: any = JSON.parse(readFileSync(handyJsonPath, { encoding: 'utf-8' }));

        if (!handyJsonData['clientAnalytics']) {

          handyJsonData['clientAnalytics'] = {
            val: {
              tracking: true,
              abTesting: true
            },
            devVal: {
              tracking: true,
              abTesting: true
            },
            public: true
          };

          writeFileSync(handyJsonPath, JSON.stringify(handyJsonData, null, 4), { encoding: 'utf-8' });
          Logger.success(`Updated: ${handyJsonPath.replace(ConfigHelper.projectRootDir, '')}`);

        }

        resolve();

      } catch (error) {
        reject(error);
      }

    })

  }

  private _updateSingleEntity(entity: PathsList, overwrite: boolean = true): Promise<void> {

    let relativePath: string = srcPaths[entity];

    let srcPath: string = join(this._tempPath, relativePath);
    let destPath: string = join(ConfigHelper.projectRootDir, relativePath);

    return new Promise((resolve, reject) => {

      try {

        copySync(srcPath, destPath, { recursive: true, overwrite });
        Logger.success(`Updated: ${destPath.replace(ConfigHelper.projectRootDir, '')}`);

        return resolve();

      } catch (error) {

        Logger.error(`Update failed: ${destPath.replace(ConfigHelper.projectRootDir, '')}`);
        return reject(error);

      }

    })

  }

}

/* 

  "clientAnalytics": {
    "val": {
      "tracking": true,
      "abTesting": true
    },
    "devVal": {
      "tracking": true,
      "abTesting": true
    },
    "public": true
  }
*/

interface Answers {
  toUpdate: PathsList[]
}