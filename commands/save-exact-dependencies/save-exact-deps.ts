import { exec } from 'child_process';
import { prompt, QuestionCollection } from 'inquirer';
import { join } from 'path';
import { writeFileSync, readFileSync, existsSync, copySync, promises } from 'fs-extra';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';

export class SaveExactDepsCommand {

  constructor () {

  }

  public command(): void {
    
    let extecPath: string = ConfigHelper.cwd;
    let lockFile: string = join(extecPath, 'package-lock.json');
    let packageFile: string = join(extecPath, 'package.json');

    if (!existsSync(lockFile)) {
      
      Logger.error('No package-lock.json file is present, make sure you are running this command in a root folder of node project and all dependencies are installed.');
      return;

    }

    if (!existsSync(packageFile)) {
      
      Logger.error('No package.json file is present, make sure you are running this command in a root folder of node project.');
      return;

    }

    let packageContent: PackageContent = JSON.parse(readFileSync(packageFile, { encoding: 'utf-8' }));
    let lockContent: LockPackageContent = JSON.parse(readFileSync(lockFile, { encoding: 'utf-8' }));

    let deps: string[] = Object.keys(packageContent.dependencies);
    let devDeps: string[] = Object.keys(packageContent.devDependencies);
    let depsLen: number = deps.length;
    let devDepsLen: number = devDeps.length;

    for (let i = 0; i < depsLen; i++) {
      const depName = deps[i];
      const version: string = lockContent.dependencies[depName].version;

      packageContent.dependencies[depName] = version;

    }

    for (let i = 0; i < devDepsLen; i++) {
      const depName = devDeps[i];
      const version: string = lockContent.dependencies[depName].version;

      packageContent.devDependencies[depName] = version;

    }

    writeFileSync(packageFile, JSON.stringify(packageContent, null, 1));
    Logger.info('Done');

  }

}

interface PackageContent { dependencies: { [pckg: string]: string }, devDependencies: { [pckg: string]: string } };
interface LockPackageContent { dependencies: { [pckg: string]: { version: string } } };