import { exec, execSync } from 'child_process';
import { prompt, QuestionCollection } from 'inquirer';
import { join } from 'path';
import { writeFileSync, readFileSync, existsSync, copySync, promises } from 'fs-extra';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';

export class UpdateCliCommand {

  constructor () {

  }

  public command(): void {


    let path: string = join(__dirname, '../../../');

    execSync(`cd ${path} && git stash`);
    execSync(`cd ${path} && git pull`);
    execSync(`cd ${path} && npm install`);

    const CLI_VERSION: string = JSON.parse(readFileSync(join(path, 'package.json'), 'utf-8')).version;

    Logger.success(`Handy cli updated to v${CLI_VERSION}`);

  }

}