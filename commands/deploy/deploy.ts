import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../helpers/logger';
import { ConfigHelper, DoDeployData } from '../../helpers/config';
import IO from 'socket.io-client';

export class DeployCommand {

  constructor () {

  }

  public command(): void {

    if (!ConfigHelper.isInHandyProject) {
      Logger.notHandyProject();
      return;
    }

    if (ConfigHelper.projectEnv !== 'dev') {
      Logger.error('Command "handy deploy" is supported on development enviroment only');
      return;
    }

    let accessToken: string;
    let predeployData: DoDeployData;

    Logger.info('Getting developer creds');

    ConfigHelper.getAccessToken()
      .then(token => {

        return this.__deploy(token);

      })
      .catch(err => {

        Logger.error(err);
        return;

      })

  }

  private __deploy(accessToken: string): Promise<void> {

    return new Promise((resolve, reject) => {

      let deployData: DoDeployData = ConfigHelper.getPredeployData();

      let { domains = {}, repo = null, branch } = deployData;
      if (!domains.prod || !domains.stag) {
        return reject('Wrong domains setting in handy.json');
      }

      if (!repo) {
        return reject('Missing remote repository');
      }

      if (branch !== 'master') {
        return reject('You are not on a master branch');
      }

      let socketInstance = IO(`${ConfigHelper.hubUrl}cli`, {
        query: { accessToken },
      });

      let started: boolean = false;
      let initialDeploy: boolean = false;

      socketInstance.on('saveHandyAppId', (handyAppId: number) => {

        ConfigHelper.saveHandyAppId(handyAppId);

      })

      socketInstance.on('chooseServer', (serversToChooseFrom: ServerChoice[]) => {

        let choices: string[] = [];
        let serversToChooseFromLen: number = serversToChooseFrom.length;

        for (let i = 0; i < serversToChooseFromLen; i++) {
          const { ip } = serversToChooseFrom[i];

          if (ip) {
            choices.push(ip);
          }

        }

        initialDeploy = true;
        Logger.info('Initial deploy')

        let serverChoiceQuestion: QuestionCollection<{ choosenServerIp: string }> = {
          message: 'Choose server',
          type: 'list',
          name: 'choosenServerIp',
          choices,
        }

        prompt(serverChoiceQuestion)
          .then(result => {

            let selectedServerToSend: any = {};
            for (let i = 0; i < serversToChooseFromLen; i++) {
              const { ip, _id } = serversToChooseFrom[i];

              if (result.choosenServerIp === ip) {

                selectedServerToSend = {
                  selectedIp: ip,
                  selectedId: _id,
                };

                break;

              }

            }

            socketInstance.emit('selectedServer', {
              accessToken,
              eventData: selectedServerToSend
            });

          })

      })

      let handyAppId: number;

      socketInstance.on('preparePush', (appDbdata: HubAppDbData) => {

        let { stagPort, prodPort, repo, _id } = appDbdata;

        console.log('preparePush', appDbdata)

        handyAppId = _id;

        ConfigHelper.updatePorts(stagPort, prodPort);
        ConfigHelper.updateNginxConf(appDbdata);
        ConfigHelper.addRepoToJsons(repo);

        ConfigHelper.buildApp().then(() => {

          return ConfigHelper.simpleYesNoPrompt('Was build successfull?', true)

        })
          .then(buildResult => {

            if (!buildResult) {
              return Promise.reject('Build failed');
            }

            return ConfigHelper.pushRepo();

          })
          .then(() => {

            return ConfigHelper.simpleYesNoPrompt('Was push successfull?', true)

          })
          .then(pushResult => {

            if (!pushResult) {
              return Promise.reject('Push failed');
            }

            if (initialDeploy) {
              return Promise.resolve({ env: 'both' })
            }

            let envChoiceQuestion: QuestionCollection<{ env: 'prod' | 'stag' | 'both' }> = {
              message: 'Choose enviroment',
              type: 'list',
              name: 'env',
              choices: [
                {
                  value: 'both',
                  name: 'Both'
                },
                {
                  value: 'stag',
                  name: 'Staging'
                },
                {
                  value: 'prod',
                  name: 'Production'
                }
              ],
              default: 'stag'
            }

            return prompt(envChoiceQuestion)

          })
          .then(envToDeploy => {

            let builtAndPushedEvent = {
              accessToken,
              eventData: { env: envToDeploy.env, initialDeploy, handyAppId }
            };

            console.log({ builtAndPushedEvent });

            socketInstance.emit('builtAndPushed', builtAndPushedEvent);

          })
          // .then(() => {

          //   Logger.info('Command finished');

          //   socketInstance.disconnect();
          //   return resolve();

          // })
          .catch((err => {

            Logger.error(err);
            // socketInstance.disconnect();
            return resolve();

          }))

      })

      socketInstance.on('connect_error', () => {

        Logger.error('Connection error');
        // socketInstance.disconnect();
        return resolve();

      })

      socketInstance.on('error', (data: any) => {

        Logger.error('Connection error');
        console.log(data);
        // socketInstance.disconnect();
        return resolve();

      })
      
      socketInstance.on('reconnect', (data: any) => {

        Logger.info('Reconnected');
        // console.log(data);
        // socketInstance.disconnect();
        // return resolve();

      })

      socketInstance.on('commandProgress', (data: any) => {
        console.log(data);
      })

      socketInstance.on('commandError', (data: any) => {

        Logger.error('Command error');
        console.log(data);
        socketInstance.disconnect();
        return resolve();

      })

      socketInstance.on('commandFinish', () => {

        Logger.success('Command finsihed');
        socketInstance.disconnect();
        return resolve();

      })

      socketInstance.on('connect', () => {

        if (started) {
          return;
        }

        started = true;

        let deployEvent = {
          accessToken,
          eventData: deployData
        };

        socketInstance.emit('deploy', deployEvent);

      });

    })

  }

}

interface Answers {
  whatToRun: 'Server' | 'Client' | 'Both'
}

interface ServerChoice {
  _id?: number,
  ip?: string
}

export interface HubAppDbData {
  stagDomain: string,
  prodDomain: string,
  stagPort: number,
  prodPort: number,
  repo: string,
  _id: number
}