import { exec } from 'child_process';
import { prompt, QuestionCollection } from 'inquirer';
import { join } from 'path';
import { writeFileSync, readFileSync, existsSync, copy, emptydirSync, rmdirSync } from 'fs-extra';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';

export class NewProjectCommand {

  private _baseRepo: string = 'https://bitbucket.org/struko/handy-base.git';
  private _baseRepo24: string = 'https://bitbucket.org/struko/handy-24.git';

  private _tempPath: string = join(__dirname, 'tmp');
  private _tempHandyJsonPath: string = join(this._tempPath, 'handy.json');
  private _tempPackageJsonPath: string = join(this._tempPath, 'package.json');
  private _tempPackageLockJsonPath: string = join(this._tempPath, 'package-lock.json');

  private _cwd: string = join(process.cwd() + '/');

  private _questions: QuestionCollection = [
    {
      message: 'Use new version of Handy (v24)',
      type: 'confirm',
      name: 'newVersion',
      default: true
    },
    {
      message: 'Project name',
      type: 'input',
      name: 'projName',
    },
    {
      message: 'Super Admin email',
      type: 'input',
      name: 'adminEmail',
      default: (ConfigHelper.isLoggedIn()) ? ConfigHelper.getHubCreds().email : 'accounts@handyapps.ie'
    },
    {
      message: 'Author',
      type: 'input',
      name: 'author',
      default: 'HandyApps'
    },
    {
      message: 'Description',
      type: 'input',
      name: 'description',
      default: ''
    },
    {
      message: 'Install dependencies',
      type: 'confirm',
      name: 'installDependencies',
      default: true
    }
  ];

  private _answers: Answers;

  constructor () {

  }

  public command(): void {

    this._prompt()
      .then(() => {
        return this._cloneBase();
      })
      .then(() => {

        this._updateHandyJsonFile();
        this._updatePackageJsonFile();

        return Promise.resolve();

      })
      .then(() => {

        return this._copyProjectFiles();

      })
      .then(() => {
        
        return this._clearGit();

      })
      .then(() => {

        return this._initGit();

      })
      .then(() => {

        return this._installDependencies();

      })
      .then(() => {

        Logger.success('New project generated');

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<Answers>(this._questions)
        .then(answers => {

          this._answers = answers;

          if (!answers.projName) {

            return Promise.reject('Invalid project name');

          }
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _cloneBase(): Promise<void> {

    let spinner = Logger.spinner('Cloning Handy base...');

    return new Promise((resolve, reject) => {

      spinner.start();

      let command: string = `git clone ${this._answers.newVersion ? this._baseRepo24 : this._baseRepo} "${this._tempPath}"`;

      if (existsSync(this._tempHandyJsonPath)) {
        // command = `cd "${this._tempPath}" && git stash && git pull`;
        rmdirSync(this._tempPath, { recursive: true });
      }
      
      exec(command, (err, stdout, stderr) => {

        spinner.stop(true);

        if (err) {

          reject(err);
          return;

        }

        Logger.success('Handy base cloned');
        return resolve();

      })

    })

  }

  private _copyProjectFiles(): Promise<any> {

    if (existsSync(this._getFinalProjectPath())) {
      return Promise.reject('This project already exists in current folder');
    }

    return copy(this._tempPath, this._getFinalProjectPath(), { recursive: true })

  }

  private _updateHandyJsonFile(): void {

    let baseSettings = JSON.parse(readFileSync(this._tempHandyJsonPath, { encoding: 'utf-8' }));

    baseSettings.projectName.val = this._capitalizedProjectName();
    baseSettings.projectName.stagVal = this._capitalizedProjectName() + ' - Staging';
    baseSettings.projectName.devVal = this._capitalizedProjectName() + ' - Development';
    
    baseSettings.domain.devVal = 'localhost';
    baseSettings.domain.val = '';
    baseSettings.domain.stagVal = '';

    baseSettings.superAdminEmail.val = this._answers.adminEmail;
    baseSettings.errors.val.emailTo = this._answers.adminEmail;

    baseSettings.deviceIdCookieHash.val = this._randomHash();

    baseSettings.secret.val = this._randomHash();
    baseSettings.secret.stagVal = this._randomHash();
    baseSettings.secret.devVal = this._randomHash();

    baseSettings.cookieSecret.val = this._randomHash();
    baseSettings.cookieSecret.stagVal = this._randomHash();
    baseSettings.cookieSecret.devVal = this._randomHash();
   
    baseSettings.userPassSufixFlag.val = this._randomHash();
    baseSettings.userPassSufixFlag.stagVal = this._randomHash();
    baseSettings.userPassSufixFlag.devVal = this._randomHash();

    let tokensList: string[] = Object.keys(baseSettings.jwt.val.types);
    let tokensLen: number = tokensList.length;

    for (let i = 0; i < tokensLen; i++) {

      const tokenName = tokensList[i];
      baseSettings.jwt.val.types[tokenName].secret = this._randomHash();
      
    }

    writeFileSync(this._tempHandyJsonPath, JSON.stringify(baseSettings, null, 2));

  }

  private _updatePackageJsonFile(): void {

    let packageJson = JSON.parse(readFileSync(this._tempPackageJsonPath, { encoding: 'utf-8' }));

    packageJson.name = this._answers.projName.toLowerCase().replace(/\s/g, '_');
    packageJson.author = this._answers.author;
    packageJson.repository = {};
    packageJson.description = this._answers.description;
    packageJson.homepage = '';

    writeFileSync(this._tempPackageJsonPath, JSON.stringify(packageJson, null, 2));

  }

  private _randomHash(len: number = 8): string {

    return Math.random().toString(36).substring(2, 2 + len);

  }

  private _clearGit(): Promise<void> {

    let spinner = Logger.spinner('Removing base repo...');

    return new Promise((resolve, reject) => {

      spinner.start();

      let command = `cd "${this._getFinalProjectPath()}" && rm -rf .git`;

      exec(command, (err, stdout, stderr) => {

        spinner.stop(true);

        if (err) {

          reject(err);
          return;

        }

        Logger.success('Handy base repository removed')
        return resolve();

      })

    })

  }
  
  private _initGit(): Promise<void> {

    let spinner = Logger.spinner('Initializing new project repository...');

    return new Promise((resolve, reject) => {

      spinner.start();

      let command = `cd "${this._getFinalProjectPath()}" && git init`;

      exec(command, (err, stdout, stderr) => {

        spinner.stop(true);

        if (err) {

          reject(err);
          return;

        }

        Logger.success('New project repository initialized')
        return resolve();

      })

    })

  }
  
  private _installDependencies(): Promise<void> {

    if (!this._answers.installDependencies) {

      Logger.info('Skipping dependencies install');
      return Promise.resolve();

    }

    Logger.info('Installing dependencies ...');

    return new Promise((resolve, reject) => {

      let command = `cd "${this._getFinalProjectPath()}" && npm run install-all`;

      // exec(command, (err, stdout, stderr) => {
        
      //   if (err) {
          
      //     console.log(stderr);
      //     reject(err);
      //     return;
          
      //   }
        
      //   console.log(stdout);
      //   Logger.info('Finished installing dependencies')
      //   return resolve();

      // })

      let child = exec(command);

      // @ts-ignore
      child.stdout.on('data', data => {
        console.log(data.toString());
      });
      // @ts-ignore
      child.stderr.on('data', data => {
        console.log(data.toString());
      });

      // @ts-ignore
      child.on('exit', code => {
        Logger.info('Finished installing dependencies')
        return resolve();
      });

    })

  }

  private _getFinalProjectPath(): string {

    return join(this._cwd, this._answers.projName as string);

  }

  private _capitalizedProjectName(): string {
    return (this._answers.projName as string).charAt(0).toUpperCase() + (this._answers.projName as string).slice(1);
  }

}

interface Answers {
  projName: string,
  adminEmail: string,
  author: string,
  description: string,
  installDependencies: boolean,
  newVersion: boolean
}