import { exec } from 'child_process';
import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';

export class RunCommand {

  private _questions: QuestionCollection = [
    {
      message: 'What to run',
      type: 'list',
      name: 'whatToRun',
      choices: ['Server', 'Client', 'Both'],
      default: 'Both'
    }
  ];

  private _answers: Answers;

  constructor () {

  }

  public command(): void {

    if (!ConfigHelper.isInHandyProject) {

      Logger.notHandyProject();
      return;

    }

    if (ConfigHelper.projectEnv !== 'dev') {

      Logger.error('Command "handy run" is supported on development enviroment only');
      return;

    }

    this._prompt()
      .then(() => {

        let clientCommand: string = `cd ${ConfigHelper.projectRootDir} && concurrently -r "npm run watch-ng" "npm run serve-build"`;
        let serverCommand: string = `cd ${ConfigHelper.projectRootDir} && nodemon --exec npm run ${(ConfigHelper.nodeMainVersion > 10) ? 'dev-start-12' : 'dev-start'}`
        let both: string = `cd ${ConfigHelper.projectRootDir} && npm run ${(ConfigHelper.nodeMainVersion > 10) ? 'dev-12' : 'dev'}`

        switch (this._answers.whatToRun) {

          case 'Client':
            
            Logger.info('Building and running client');
            this._execute(clientCommand);

            break;
          
          case 'Server':
            
            Logger.info('Building and running server');
            this._execute(serverCommand);

            break;
          
          default:
            
            Logger.info('Building and running server with client');
            this._execute(both);
            // this._execute(clientCommand);

            break;

        }

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<Answers>(this._questions)
        .then(answers => {

          this._answers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _execute(command: string): void {

    let execution = exec(command);

    // @ts-ignore
    execution.stdout.on('data', data => {
      console.log(data.toString());
    });

    // @ts-ignore
    execution.stderr.on('data', data => {
      Logger.error(data.toString());
    });
    
    // @ts-ignore
    execution.on('exit', code => {
      Logger.info('Handy client process exited with code ' + code);
      return Promise.resolve();
    });

  }

}

interface Answers {
  whatToRun: 'Server' | 'Client' | 'Both'
}