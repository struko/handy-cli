import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../helpers/logger';
import { ConfigHelper } from '../../helpers/config';
import { ServerModelGenerator } from './generators/server/model';
import { ServerServiceGenerator } from './generators/server/service';
import { ServerModuleGenerator } from './generators/server/module';
import { ServerControllersGenerator } from './generators/server/controllers';
import { ServerMongooseValidatorGenerator } from './generators/server/mongooseValidator';
import { CrudTableComponentGenerator } from './generators/webAppClient/crud.table.component';
import { WebAppModelGenerator } from './generators/webAppClient/model.service';
import { CrudFormComponentGenerator } from './generators/webAppClient/crud.form.component';
import { CrudModuleGenerator } from './generators/webAppClient/crud-module';
import { WebAppFormValidatorGenerator } from './generators/webAppClient/form-validator';
import { WebAppEmptyFormComponentGenerator } from './generators/webAppClient/empty-form';

export class GenerateCommand {

  private _platformQuestions: QuestionCollection<PlatformAnswers> = [
    {
      message: 'What would you like to generate for',
      type: 'list',
      name: 'whatToGen',
      choices: [
        {
          name: 'Server',
          value: 'server'
        },
        {
          name: 'Web client',
          value: 'webApp'
        },
      ],
    }
  ];

  private _serverQuestions: QuestionCollection<ServerAnswers> = [
    {
      message: 'What would you like to generate',
      type: 'list',
      name: 'whatToGen',
      choices: [
        {
          name: 'Model',
          value: 'serverModel'
        },
        {
          name: 'Service',
          value: 'serverService'
        },
        {
          name: 'Module',
          value: 'serverModule'
        },
        {
          name: 'Controller',
          value: 'serverController'
        },
        {
          name: 'Mongoose validator',
          value: 'serverMongooseValidator'
        },
      ],
    }
  ];

  private _webAppQuestions: QuestionCollection<WebAppClientAnswers> = [
    {
      message: 'What would you like to generate',
      type: 'list',
      name: 'whatToGen',
      choices: [
        {
          name: 'Crud table component',
          value: 'crudTable'
        },
        {
          name: 'Crud form component',
          value: 'crudForm'
        },
        {
          name: 'Crud module',
          value: 'crudModule'
        },
        {
          name: 'Ng Model service',
          value: 'modelService'
        },
        {
          name: 'Empty form component',
          value: 'emptyForm'
        },
        {
          name: 'Form validator',
          value: 'formValidator'
        },
      ],
    }
  ];

  private _platformAnswers: PlatformAnswers;
  private _serverAnswers: ServerAnswers;
  private _webAppAnswers: WebAppClientAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    if (!ConfigHelper.isInHandyProject) {

      Logger.notHandyProject();
      return;

    }

    if (ConfigHelper.projectEnv !== 'dev') {

      Logger.error('Command "handy run" is supported on development enviroment only');
      return;

    }

    this._platformPrompt()
      .then(() => {

        switch (this._platformAnswers.whatToGen) {

          case 'server':
            return this._serverPrompt();

          case 'webApp':
            return this._webAppClientPrompt();

          default:
            break;
        }

        return Promise.reject('No matching generator found');

      })
      .then(() => {

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _platformPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<PlatformAnswers>(this._platformQuestions)
        .then(answers => {

          this._platformAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _serverPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<ServerAnswers>(this._serverQuestions)
        .then(answers => {

          this._serverAnswers = answers;
          this._handleServerGenerators();

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _handleServerGenerators(): void {

    let Generator: any = undefined;

    switch (this._serverAnswers.whatToGen) {

      case 'serverModel':

        Generator = new ServerModelGenerator(this._verifyFiles);

        break;

      case 'serverService':

        Generator = new ServerServiceGenerator(this._verifyFiles);

        break;

      case 'serverModule':

        Generator = new ServerModuleGenerator(this._verifyFiles);

        break;

      case 'serverController':

        Generator = new ServerControllersGenerator(this._verifyFiles);

        break;

      case 'serverMongooseValidator':

        Generator = new ServerMongooseValidatorGenerator(this._verifyFiles);

        break;

      default:
        break;

    }

    if (Generator !== undefined) {
      Generator.command();
      return;
    }

  }

  private _webAppClientPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<WebAppClientAnswers>(this._webAppQuestions)
        .then(answers => {

          this._webAppAnswers = answers;
          this._handleWebAppGenerators();

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _handleWebAppGenerators(): void {

    let Generator: any = undefined;

    switch (this._webAppAnswers.whatToGen) {

      case 'crudTable':

        Generator = new CrudTableComponentGenerator(this._verifyFiles);

        break;

      case 'modelService':

        Generator = new WebAppModelGenerator(this._verifyFiles);

        break;

      case 'crudForm':

        Generator = new CrudFormComponentGenerator(this._verifyFiles);

        break;
      
      case 'crudModule':

        Generator = new CrudModuleGenerator(this._verifyFiles);

        break;
      
      case 'formValidator':

        Generator = new WebAppFormValidatorGenerator(this._verifyFiles);

        break;
      
      case 'emptyForm':

        Generator = new WebAppEmptyFormComponentGenerator(this._verifyFiles);

        break;

      default:
        break;

    }

    if (Generator !== undefined) {
      Generator.command();
      return;
    }

  }

}

interface ServerAnswers {
  whatToGen: 'serverModel' | 'serverService' | 'serverModule' | 'serverController' | 'serverMongooseValidator'
}

interface WebAppClientAnswers {
  whatToGen: 'crudTable' | 'crudForm' | 'crudModule' | 'modelService' | 'emptyForm' | 'formValidator'
}

interface PlatformAnswers {
  whatToGen: 'server' | 'webApp'
}