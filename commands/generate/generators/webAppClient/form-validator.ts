import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars' 
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class WebAppFormValidatorGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/form-validators');
  protected _projectTemplatesPaths: any = {
    sync: join(this._projectTemplatesDir, 'sync.hbs'),
    async: join(this._projectTemplatesDir, 'async.hbs'),
  };
  
  protected _hbsTemplates: FilesPaths = {
    sync: '',
    async: '',
  };

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/client/web/src/app/modules/shared/form-validators/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'What type of validator would you like to generate',
      type: 'list',
      name: 'type',
      choices: [
        {
          name: `Synchronous`,
          value: 'sync'
        },
        {
          name: `Asynchronous`,
          value: 'async'
        },
      ],
      default: 'sync'
    },
    {
      message: 'Validator name',
      type: 'input',
      name: 'validatorName',
    }
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.validatorName.trim() === '') {
          return Promise.reject('Invalid validator name')
        }

        return Promise.resolve();

      })
      .then(() => {

        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.validatorName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          this._defaultAnswers.validatorName = ConfigHelper.getPrintableString(this._defaultAnswers.validatorName);
          this._getFilesPaths();
          
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled validator generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled validator generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();
    
    Logger.info(this._savedFilesPath);
    console.log();
    Logger.info('Files to be updated:')
    console.log();
    Logger.info(this._indexFileToUpdate);

    console.log();

  }

  private _savedFilesPath: string;
  private _getFilesPaths(): string {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    this._savedFilesPath = join(ConfigHelper.projectRootDir, 'src/client/web/src/app/modules/shared/form-validators/', ConfigHelper.getFileNameString(this._defaultAnswers.validatorName) + '.validator.ts');
    return this._savedFilesPath;

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      if (existsSync(this._savedFilesPath)) {
        this._existingFiles.push(this._savedFilesPath);
      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        let type: 'sync' | 'async' = (this._defaultAnswers.type === 'sync') ? 'sync' : 'async';
        let source: string = readFileSync(this._projectTemplatesPaths[type], { encoding: 'utf-8' });

        this._hbsTemplates[type] = compile(source)(this._defaultAnswers);

        writeFileSync(this._savedFilesPath, this._hbsTemplates[type], { encoding: 'utf-8' });

        let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
        let validatorName: string = ConfigHelper.getFileNameString(this._defaultAnswers.validatorName);
        let exportToAdd: string = `export * from './${validatorName}.validator';`

        if (!originalIndex.includes(exportToAdd)) {
          writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
        }

        return resolve();

      } catch (error) {
        
        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  type: 'sync' | 'async',
  validatorName: string,

}

interface PublicRoutableAnswer {
  publicRoutable: boolean,
}

interface FilesPaths {
  sync: string,
  async: string,
}