import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars'
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class WebAppEmptyTableComponentGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/empty-table-component');
  protected _projectTemplatesPaths: FilesPaths = {
    ts: join(this._projectTemplatesDir, 'tableName.logic.hbs'),
    scss: join(this._projectTemplatesDir, 'tableName.styles.hbs'),
    html: join(this._projectTemplatesDir, 'tableName.html.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    ts: '',
    scss: '',
    html: '',
  };

  protected _indexFileToUpdate: string;

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Table name',
      type: 'input',
      name: 'tableName',
    },
    {
      message: 'Would you like to remember this table state for user',
      type: 'list',
      name: 'rememberTableState',
      choices: [
        {
          name: `Yes`,
          value: true
        },
        {
          name: `No`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Would you like to generate resolver for this table',
      type: 'list',
      name: 'includeResolver',
      choices: [
        {
          name: `Yes`,
          value: true
        },
        {
          name: `No`,
          value: false
        },
      ],
      default: true
    },
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.tableName.trim() === '') {
          return Promise.reject('Invalid table name')
        }

        return Promise.resolve();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.tableName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          this._defaultAnswers.tableName = ConfigHelper.getPrintableString(this._defaultAnswers.tableName);

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        ConfigHelper.generateFolders([ConfigHelper.magicBoxPath]);
        
        let componentFileName: string = ConfigHelper.getFileNameString(this._defaultAnswers.tableName);
        let resultDir: string = join(ConfigHelper.magicBoxPath, componentFileName);
        ensureDirSync(resultDir);

        let templatesKeys: (keyof FilesPaths)[] = Object.keys(this._hbsTemplates) as (keyof FilesPaths)[];
        let templatesLen: number = templatesKeys.length;

        for (let i = 0; i < templatesLen; i++) {
          const fileType = templatesKeys[i];

          let source: string = readFileSync(this._projectTemplatesPaths[fileType], { encoding: 'utf-8' });
          this._hbsTemplates[fileType] = compile(source)(this._defaultAnswers);

          let finalFilePath: string = join(resultDir, `${componentFileName}.component.${fileType}`);

          writeFileSync(finalFilePath, this._hbsTemplates[fileType], { encoding: 'utf-8' });

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  includeResolver: boolean,
  tableName: string,
  rememberTableState: boolean

}

interface FilesPaths {
  ts: string,
  scss: string,
  html: string,
}