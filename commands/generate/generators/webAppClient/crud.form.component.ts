import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { ServerModelHelper } from '../../../../helpers/server.models.helper';

export class CrudFormComponentGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-form-component');
  protected _projectTemplatesPaths: FilesPaths = {
    styles: join(this._projectTemplatesDir, 'formName.styles.hbs'),
    html: join(this._projectTemplatesDir, 'formName.html.hbs'),
    logic: join(this._projectTemplatesDir, 'formName.logic.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    styles: '',
    html: '',
    logic: '',
  };

  constructor (private _verifyFiles: boolean) {

  }

  public command(asPromise: boolean = false): Promise<void> {

    return new Promise((resolve, reject) => {

      ServerModelHelper.selectModelPrompt()
        .then(() => {
          return ServerModelHelper.formTypePrompt();
        })
        .then(() => {
          return ServerModelHelper.includeResolver();
        })
        .then(() => {
          return ServerModelHelper.selectedFormFieldsPrompt();
        })
        .then(() => {
          return ServerModelHelper.rememberFormStatePrompt();
        })
        .then(() => {
          return ServerModelHelper.formNamePrompt();
        })  
        .then(() => {
          return ServerModelHelper.generateFormComponentFiles();
        })
        .then(() => {

          return resolve();

        })
        .catch(err => {

          if (!asPromise) {
            Logger.error(err);
          }

          return reject(err)

        })

    })

  }



}

interface FilesPaths {
  styles: string,
  html: string,
  logic: string,
}