import { prompt } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars'
import { readFileSync, writeFileSync, existsSync } from 'fs-extra';
import { ServerModelHelper } from '../../../../helpers/server.models.helper';

export class WebAppModelGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/model-service');
  protected _projectTemplatesPaths: FilesPaths = {
    model: join(this._projectTemplatesDir, 'modelName.ng-model.hbs'),
  };

  protected _destinationFolder: string = join(ConfigHelper.projectRootDir, 'src/client/web/src/app/handy/models');
  protected _modelName: string;
  protected _modelFileDestinationPath: string;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    ServerModelHelper.selectModelPrompt()
      .then(() => {

        this._modelName = ConfigHelper.LcFirst(ServerModelHelper.getSelectedModelData().modelName);
        this._modelFileDestinationPath = join(this._destinationFolder, this._modelName + '.ng-model.ts');
        return this._filesVerifyPrompt();

      })
      .then(() => {

        this._checkFilesExistency();
        return this._overWritePrompt()

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`Ng model service was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled model generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled model generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    console.log();
    Logger.info('Files to be generated:')
    console.log();

    Logger.info(this._modelFileDestinationPath);

    console.log();

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): void {

    if (existsSync(this._modelFileDestinationPath)) {
      this._existingFiles.push(this._modelFileDestinationPath);
    }

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        let source: string = readFileSync(this._projectTemplatesPaths.model, { encoding: 'utf-8' });
        let compiledModle = compile(source)({ modelName: this._modelName });
        writeFileSync(this._modelFileDestinationPath, compiledModle, { encoding: 'utf-8' });

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface FilesPaths {
  model: string,
}