import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars' 
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class WebAppEmptyFormComponentGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/empty-form-component');
  protected _projectTemplatesPaths: FilesPaths = {
    ts: join(this._projectTemplatesDir, 'formName.logic.hbs'),
    scss: join(this._projectTemplatesDir, 'formName.styles.hbs'),
    html: join(this._projectTemplatesDir, 'formName.html.hbs'),
  };
  
  protected _hbsTemplates: FilesPaths = {
    ts: '',
    scss: '',
    html: '',
  };

  protected _indexFileToUpdate: string;

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Form name',
      type: 'input',
      name: 'formName',
    },
    {
      message: 'Would you like to remember this form state for user',
      type: 'list',
      name: 'rememberFormState',
      choices: [
        {
          name: `Yes`,
          value: true
        },
        {
          name: `No`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Would you like to generate resolver for this form',
      type: 'list',
      name: 'includeResolver',
      choices: [
        {
          name: `Yes`,
          value: true
        },
        {
          name: `No`,
          value: false
        },
      ],
      default: true
    },    
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.formName.trim() === '') {
          return Promise.reject('Invalid form name')
        }

        return Promise.resolve();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.formName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          this._defaultAnswers.formName = ConfigHelper.getPrintableString(this._defaultAnswers.formName);
          
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        ConfigHelper.generateFolders([ConfigHelper.magicBoxPath]);
        
        let componentFileName: string = ConfigHelper.getFileNameString(this._defaultAnswers.formName);
        let resultDir: string = join(ConfigHelper.magicBoxPath, componentFileName);
        ensureDirSync(resultDir);

        let templatesKeys: (keyof FilesPaths)[] = Object.keys(this._hbsTemplates) as (keyof FilesPaths)[];
        let templatesLen: number = templatesKeys.length;

        for (let i = 0; i < templatesLen; i++) {
          const fileType = templatesKeys[i];

          let source: string = readFileSync(this._projectTemplatesPaths[fileType], { encoding: 'utf-8' });
          this._hbsTemplates[fileType] = compile(source)(this._defaultAnswers);

          let finalFilePath: string = join(resultDir, `${componentFileName}.component.${fileType}`);

          writeFileSync(finalFilePath, this._hbsTemplates[fileType], { encoding: 'utf-8' });
          
        }

        return resolve();

      } catch (error) {
        
        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  includeResolver: boolean,
  formName: string,
  rememberFormState: boolean

}

interface FilesPaths {
  ts: string,
  scss: string,
  html: string,
}