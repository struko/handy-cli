import { Logger } from '../../../../helpers/logger';
import { ServerModelHelper } from '../../../../helpers/server.models.helper';

export class CrudModuleGenerator {

  constructor (private _verifyFiles: boolean) {

  }

  public command(asPromise: boolean = false): Promise<void> {

    return new Promise((resolve, reject) => {

      ServerModelHelper.selectModelPrompt()

        // Module
        .then(() => {
          return ServerModelHelper.moduleNamePrompt();
        })
        .then(() => {
          return ServerModelHelper.moduleEntryTypePrompt();
        })

        // Table
        .then(() => {
          return ServerModelHelper.selectTableDisplayColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableOptionalDisplayColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableSearchColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableFilterColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.rememberTableStatePrompt();
        })
        .then(() => {
          return ServerModelHelper.tableNamePrompt();
        })

        // Form
        .then(() => {
          return ServerModelHelper.selectedFormFieldsPrompt();
        })
        .then(() => {
          return ServerModelHelper.rememberFormStatePrompt();
        })
        .then(() => {
          return ServerModelHelper.formNamePrompt();
        })

        // Files generating
        // .then(() => {

        //   // TODO not yet
        //   return ServerModelHelper.generateFormComponentFiles();
        // })
        .then(() => {

          return this._generateModule();

        })
        .then(() => { 
          // console.log(ServerModelHelper.getStandAloneFormGenerationData())
          // console.log(ServerModelHelper.getStandAloneTableGenerationData())
          return resolve();

        })
        .catch(err => {

          if (!asPromise) {
            Logger.error(err);
          }

          return reject(err)

        })

    })

  }

  private _generateModule(): Promise<void> {

    return new Promise((resolve, reject) => {

      ServerModelHelper.generateModuleFiles();
      ServerModelHelper.generateFormComponentFiles();
      ServerModelHelper.generateTableComponentFiles();
      resolve();
    })

  }

}

interface FilesPaths {
  styles: string,
  html: string,
  logic: string,
  module: string,
  routing: string
}