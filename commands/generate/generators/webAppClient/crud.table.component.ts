import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { ServerModelHelper } from '../../../../helpers/server.models.helper';

export class CrudTableComponentGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-table-component');
  protected _projectTemplatesPaths: FilesPaths = {
    styles: join(this._projectTemplatesDir, 'tableName.styles.hbs'),
    html: join(this._projectTemplatesDir, 'tableName.html.hbs'),
    logic: join(this._projectTemplatesDir, 'tableName.logic.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    styles: '',
    html: '',
    logic: '',
  };

  protected _foldersToGenerate: string[] = [];

  constructor (private _verifyFiles: boolean) {

  }

  public command(asPromise: boolean = false): Promise<void> {

    return new Promise((resolve, reject) => {

      ServerModelHelper.selectModelPrompt()
        .then(() => {
          return ServerModelHelper.selectTableDisplayColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableOptionalDisplayColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableSearchColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.selectTableFilterColumnsPrompt();
        })
        .then(() => {
          return ServerModelHelper.rememberTableStatePrompt();
        })
        .then(() => {
          return ServerModelHelper.tableNamePrompt();
        })
        .then(() => {

          return ServerModelHelper.generateTableComponentFiles();
          
        })
        .then(() => {
          
          return resolve();
          
        })
        .catch(err => {
          
          if (!asPromise) {
            Logger.error(err);
          }

          return reject(err)
  
        })

    })

  }

  

}

interface FilesPaths {
  styles: string,
  html: string,
  logic: string,
}