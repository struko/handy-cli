import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars'
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class ServerMongooseValidatorGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'server/validator');
  protected _projectTemplatesPaths: FilesPaths = {
    validator: join(this._projectTemplatesDir, 'validatorName.validator.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    validator: '',
  };

  protected _extraEmptyFilesToGenerate: string[] = [];

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/server/validators/mongoose/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Where would you like to generate validator',
      type: 'list',
      name: 'generateInValidatorsFolder',
      choices: [
        {
          name: `Default Handy validators directory -> ~project/handy/src/server/validators/mongoose/NEW_VALIDATOR`,
          value: true
        },
        {
          name: `Current directory -> ${ConfigHelper.cwd}NEW_VALIDATOR`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Validator name',
      type: 'input',
      name: 'validatorName',
    }
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.validatorName.trim() === '') {
          return Promise.reject('Invalid validator name')
        }

        this._defaultAnswers.validatorName = this._getPrintableValidatorName();

        return Promise.resolve();

      })
      .then(() => {

        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.validatorName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled validator generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled validator generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();

    for (let i = 0; i < filesLen; i++) {
      const fileName = filesList[i];
      Logger.info(this._getFilesPaths()[fileName]);
    }

    if (this._defaultAnswers.generateInValidatorsFolder) {

      console.log();
      Logger.info('Files to be updated:')
      console.log();
      Logger.info(this._indexFileToUpdate);

    }

    console.log();

  }

  private _savedPrintableValidatorName: string;
  private _getPrintableValidatorName(): string {

    if (this._savedPrintableValidatorName) {
      return this._savedPrintableValidatorName;
    }

    let validatorName: string = this._defaultAnswers.validatorName.trim();
    let splitted: string[] = validatorName.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      if (splitted[i] === 'Validator' || splitted[i] === 'validator') {
        continue;
      }

      result += ConfigHelper.UcFirst(splitted[i]).trim();

    }

    this._savedPrintableValidatorName = result;
    return this._savedPrintableValidatorName;

  }

  private _savedFilesPath: FilesPaths;
  private _getFilesPaths(): FilesPaths {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    let location: string = join((this._defaultAnswers.generateInValidatorsFolder) ? ConfigHelper.projectRootDir + 'src/server/validators/mongoose' : ConfigHelper.cwd, ConfigHelper.LcFirst(this._getPrintableValidatorName()));
    this._foldersToGenerate.push(location);

    return {
      validator: join(location, ConfigHelper.LcFirst(this._getPrintableValidatorName()) + '.validator.ts')
    }

  }

  private _generateFolders(): void {

    let foldersLen: number = this._foldersToGenerate.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(this._foldersToGenerate[i]);
    }

  }

  private _generateEmptyFiles(): void {

    let filesLen: number = this._extraEmptyFilesToGenerate.length;
    for (let i = 0; i < filesLen; i++) {
      writeFileSync(this._extraEmptyFilesToGenerate[i], '', { encoding: 'utf-8' });
    }

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      for (let i = 0; i < templatesLen; i++) {

        const templateName = templatesList[i];
        let pathToCheck = this._getFilesPaths()[templateName];

        if (existsSync(pathToCheck) && pathToCheck !== this._indexFileToUpdate) {
          this._existingFiles.push(pathToCheck);
        }

      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      try {

        this._generateFolders();
        this._generateEmptyFiles();

        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesList[i];
          let source: string = readFileSync(this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
          this._hbsTemplates[templateName] = compile(source)(this._defaultAnswers);
          writeFileSync(this._getFilesPaths()[templateName], this._hbsTemplates[templateName], { encoding: 'utf-8' });

        }

        if (this._defaultAnswers.generateInValidatorsFolder) {

          let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
          let printableValidatorName: string = ConfigHelper.LcFirst(this._getPrintableValidatorName());
          let exportToAdd: string = `export * from './${printableValidatorName}/${printableValidatorName}.validator';`

          if (!originalIndex.includes(exportToAdd)) {
            writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
          }

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  generateInValidatorsFolder: boolean,
  validatorName: string,

}

interface FilesPaths {
  validator: string,
}