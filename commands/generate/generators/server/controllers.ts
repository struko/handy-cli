import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ServerRouteControllerGenerator } from './controllers/route'
import { ServerMiddlewareGenerator } from './controllers/middleware';
import { ServerSocketControllerGenerator } from './controllers/socket';

export class ServerControllersGenerator {

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'What king of controller woyuld you like to generate',
      type: 'list',
      name: 'controller',
      choices: [
        {
          name: `Route controller`,
          value: 'route'
        },
        {
          name: `Middleware controller`,
          value: 'middleware'
        },
        {
          name: `Socket controller`,
          value: 'socket'
        },
      ],
      default: true
    }
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        let Generator: any = undefined;

        switch (this._defaultAnswers.controller) {

          case 'route':

            Generator = new ServerRouteControllerGenerator(this._verifyFiles);

            break;

          case 'middleware':

            Generator = new ServerMiddlewareGenerator(this._verifyFiles);

            break;

          case 'socket':

            Generator = new ServerSocketControllerGenerator(this._verifyFiles);

            break;

          default:
            break;

        }

        if (Generator !== undefined) {
          Generator.command();
          return;
        }

        return Promise.reject('No matching controller type found')

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

}

interface DefaultAnswers {

  controller: 'route' | 'socket' | 'middleware',

}