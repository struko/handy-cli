import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars'
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class ServerModuleGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'server/module');
  protected _projectTemplatesPaths: FilesPaths = {
    module: join(this._projectTemplatesDir, 'moduleName.module.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    module: '',
  };

  protected _extraEmptyFilesToGenerate: string[] = [];

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/server/modules/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Where would you like to generate module',
      type: 'list',
      name: 'generateInModulesFolder',
      choices: [
        {
          name: `Default Handy modules directory -> ~project/handy/src/server/modules/NEW_MODULE`,
          value: true
        },
        {
          name: `Current directory -> ${ConfigHelper.cwd}NEW_MODULE`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Module name',
      type: 'input',
      name: 'moduleName',
    }
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.moduleName.trim() === '') {
          return Promise.reject('Invalid module name')
        }

        this._defaultAnswers.moduleName = this._getPrintableModuleName();

        return Promise.resolve();

      })
      .then(() => {

        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.moduleName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled module generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled module generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();

    for (let i = 0; i < filesLen; i++) {
      const fileName = filesList[i];
      Logger.info(this._getFilesPaths()[fileName]);
    }

    if (this._defaultAnswers.generateInModulesFolder) {

      console.log();
      Logger.info('Files to be updated:')
      console.log();
      Logger.info(this._indexFileToUpdate);

    }

    console.log();

  }

  private _savedPrintableModuleName: string;
  private _getPrintableModuleName(): string {

    if (this._savedPrintableModuleName) {
      return this._savedPrintableModuleName;
    }

    let moduleName: string = this._defaultAnswers.moduleName.trim();
    let splitted: string[] = moduleName.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      if (splitted[i] === 'Module' || splitted[i] === 'module') {
        continue;
      }

      result += ConfigHelper.UcFirst(splitted[i]).trim();

    }

    this._savedPrintableModuleName = result;
    return this._savedPrintableModuleName;

  }

  private _savedFilesPath: FilesPaths;
  private _getFilesPaths(): FilesPaths {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    let location: string = join((this._defaultAnswers.generateInModulesFolder) ? ConfigHelper.projectRootDir + 'src/server/modules' : ConfigHelper.cwd, ConfigHelper.LcFirst(this._getPrintableModuleName()));
    this._foldersToGenerate.push(location);

    let extraFolders: string[] = ['models', 'services', 'validators', 'controllers', 'controllers/middlewares', 'controllers/routes', 'controllers/sockets'];
    let extraFoldersLen: number = extraFolders.length;

    for (let i = 0; i < extraFoldersLen; i++) {
      
      const singleExtraFolderPath: string = join(location, extraFolders[i]);
      this._foldersToGenerate.push(singleExtraFolderPath);
      this._extraEmptyFilesToGenerate.push(join(singleExtraFolderPath, 'index.ts'));

    }

    return {
      module: join(location, ConfigHelper.LcFirst(this._getPrintableModuleName()) + '.module.ts')
    }

  }

  private _generateFolders(): void {

    let foldersLen: number = this._foldersToGenerate.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(this._foldersToGenerate[i]);
    }

  }
  
  private _generateEmptyFiles(): void {

    let filesLen: number = this._extraEmptyFilesToGenerate.length;
    for (let i = 0; i < filesLen; i++) {
      writeFileSync(this._extraEmptyFilesToGenerate[i], '', { encoding: 'utf-8' });
    }

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      for (let i = 0; i < templatesLen; i++) {

        const templateName = templatesList[i];
        let pathToCheck = this._getFilesPaths()[templateName];

        if (existsSync(pathToCheck) && pathToCheck !== this._indexFileToUpdate) {
          this._existingFiles.push(pathToCheck);
        }

      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      try {

        this._generateFolders();
        this._generateEmptyFiles();

        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesList[i];
          let source: string = readFileSync(this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
          this._hbsTemplates[templateName] = compile(source)(this._defaultAnswers);
          writeFileSync(this._getFilesPaths()[templateName], this._hbsTemplates[templateName], { encoding: 'utf-8' });

        }

        if (this._defaultAnswers.generateInModulesFolder) {

          let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
          let printableModuleName: string = ConfigHelper.LcFirst(this._getPrintableModuleName());
          let exportToAdd: string = `export * from './${printableModuleName}/${printableModuleName}.module';`

          if (!originalIndex.includes(exportToAdd)) {
            writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
          }

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  generateInModulesFolder: boolean,
  moduleName: string,

}

interface FilesPaths {
  module: string,
}