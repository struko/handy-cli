import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars' 
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class ServerServiceGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'server/service');
  protected _projectTemplatesPaths: FilesPaths = {
    service: join(this._projectTemplatesDir, 'serviceName.service.hbs'),
  };
  
  protected _hbsTemplates: FilesPaths = {
    service: '',
  };

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/server/services/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Where would you like to generate service',
      type: 'list',
      name: 'generateInServicesFolder',
      choices: [
        {
          name: `Default Handy services directory -> ~project/handy/src/server/services/NEW_SERVICE`,
          value: true
        },
        {
          name: `Current directory -> ${ConfigHelper.cwd}NEW_SERVICE`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Service name',
      type: 'input',
      name: 'serviceName',
    },
    {
      message: 'Is this going to be a SINGLETON service',
      type: 'list',
      name: 'singleton',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
  ];
  
  private _publicRoutableQuestion: QuestionCollection<PublicRoutableAnswer> = [
    {
      message: 'Access service via api',
      type: 'list',
      name: 'routable',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
  ];

  private _defaultAnswers: DefaultAnswers;
  private _publicRoutableAnswers: PublicRoutableAnswer = { routable: false };
  private _completeAnswers: DefaultAnswers & PublicRoutableAnswer;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.serviceName.trim() === '') {
          return Promise.reject('Invalid service name')
        }

        if (this._defaultAnswers.singleton) {
          return this._publicRoutablePrompt();
        }

        return Promise.resolve();

      })
      .then(() => {

        this._completeAnswers = { ...this._defaultAnswers, ...this._publicRoutableAnswers };
        this._completeAnswers.serviceName = this._getPrintableServiceName();
        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._completeAnswers.serviceName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }
  
  private _publicRoutablePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<PublicRoutableAnswer>(this._publicRoutableQuestion)
        .then(answers => {

          this._publicRoutableAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled service generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled service generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();
    
    for (let i = 0; i < filesLen; i++) {
      const fileName = filesList[i];
      Logger.info(this._getFilesPaths()[fileName]);
    }
    
    
    if (this._defaultAnswers.generateInServicesFolder) {
      
      console.log();
      Logger.info('Files to be updated:')
      console.log();
      Logger.info(this._indexFileToUpdate);
      
    }
    
    console.log();

  }
  
  private _savedPrintableServiceName: string;
  private _getPrintableServiceName(): string {

    if (this._savedPrintableServiceName) {
      return this._savedPrintableServiceName;
    }

    let serviceName: string = this._completeAnswers.serviceName.trim();
    let splitted: string[] = serviceName.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      if (splitted[i] === 'Service' || splitted[i] === 'service') {
        continue;
      }

      result += ConfigHelper.UcFirst(splitted[i]).trim();   

    }

    this._savedPrintableServiceName = result;
    return this._savedPrintableServiceName;

  }

  private _savedFilesPath: FilesPaths;
  private _getFilesPaths(): FilesPaths {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    let location: string = join((this._defaultAnswers.generateInServicesFolder) ? ConfigHelper.projectRootDir + 'src/server/services' : ConfigHelper.cwd, ConfigHelper.LcFirst(this._getPrintableServiceName()));
    this._foldersToGenerate.push(location);

    return {
      service: join(location, ConfigHelper.LcFirst(this._getPrintableServiceName()) + '.service.ts')
    }

  }

  private _generateFolders(): void {

    let foldersLen: number = this._foldersToGenerate.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(this._foldersToGenerate[i]);     
    }

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      for (let i = 0; i < templatesLen; i++) {

        const templateName = templatesList[i];
        let pathToCheck = this._getFilesPaths()[templateName];

        if (existsSync(pathToCheck) && pathToCheck !== this._indexFileToUpdate) {
          this._existingFiles.push(pathToCheck);
        }

      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      try {

        this._generateFolders();
        
        for (let i = 0; i < templatesLen; i++) {
  
          const templateName = templatesList[i];
          let source: string = readFileSync(this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
          this._hbsTemplates[templateName] = compile(source)(this._completeAnswers);
          writeFileSync(this._getFilesPaths()[templateName], this._hbsTemplates[templateName], { encoding: 'utf-8' });
  
        }

        if (this._defaultAnswers.generateInServicesFolder) {

          let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
          let printableServiceName: string = ConfigHelper.LcFirst(this._getPrintableServiceName());
          let exportToAdd: string = `export * from './${printableServiceName}/${printableServiceName}.service';`

          if (!originalIndex.includes(exportToAdd)) {
            writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
          }

        }

        return resolve();

      } catch (error) {
        
        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  generateInServicesFolder: boolean,
  serviceName: string,

  singleton: boolean,

}

interface PublicRoutableAnswer {
  routable: boolean,
}

interface FilesPaths {
  service: string,
}