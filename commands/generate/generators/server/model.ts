import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../helpers/logger';
import { ConfigHelper } from '../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars' 
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class ServerModelGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'server/model');
  protected _projectTemplatesPaths: FilesPaths = {
    interface: join(this._projectTemplatesDir, 'model.interface.hbs'),
    declaration: join(this._projectTemplatesDir, 'model.declaration.hbs'),
    model: join(this._projectTemplatesDir, 'modelName.model.hbs'),
    index: join(this._projectTemplatesDir, 'index.hbs'),
  };
  
  protected _hbsTemplates: FilesPaths = {
    interface: '',
    declaration: '',
    model: '',
    index: ''
  };

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/server/models/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Where would you like to generate model',
      type: 'list',
      name: 'generateInModelsFolder',
      choices: [
        {
          name: `Default Handy models directory -> ~project/handy/src/server/models/NEW_MODEL`,
          value: true
        },
        {
          name: `Current directory -> ${ConfigHelper.cwd}NEW_MODEL`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Model name',
      type: 'input',
      name: 'modelName',
    },
    {
      message: 'Use autoincrement',
      type: 'list',
      name: 'autoIncrement',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Use soft delete',
      type: 'list',
      name: 'softDelete',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Track document Created at',
      type: 'list',
      name: 'createdAt',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Track document Created by',
      type: 'list',
      name: 'createdBy',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Track document Changes history',
      type: 'list',
      name: 'changesHistory',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Access model via api',
      type: 'list',
      name: 'routable',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
  ];
  
  private _publicRoutableQuestion: QuestionCollection<PublicRoutableAnswer> = [
    {
      message: 'Access model via PUBLIC api',
      type: 'list',
      name: 'publicRoutable',
      choices: [
        {
          name: 'Yes',
          value: true
        },
        {
          name: 'No',
          value: false
        },
      ],
      default: true
    },
  ];

  private _defaultAnswers: DefaultAnswers;
  private _publicRoutableAnswers: PublicRoutableAnswer = { publicRoutable: false };
  private _completeAnswers: DefaultAnswers & PublicRoutableAnswer;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.modelName.trim() === '') {
          return Promise.reject('Invalid model name')
        }

        if (this._defaultAnswers.routable) {
          return this._publicRoutablePrompt();
        }

        return Promise.resolve();

      })
      .then(() => {

        this._completeAnswers = { ...this._defaultAnswers, ...this._publicRoutableAnswers };
        this._completeAnswers.modelName = this._getPrintableModelName();
        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._completeAnswers.modelName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }
  
  private _publicRoutablePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<PublicRoutableAnswer>(this._publicRoutableQuestion)
        .then(answers => {

          this._publicRoutableAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled model generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }
 
  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled model generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();
    
    for (let i = 0; i < filesLen; i++) {
      const fileName = filesList[i];
      Logger.info(this._getFilesPaths()[fileName]);
    }

    if (this._defaultAnswers.generateInModelsFolder) {

      console.log();
      Logger.info('Files to be updated:')
      console.log();
      Logger.info(this._indexFileToUpdate);

    }
    
    console.log();

  }
  
  private _savedPrintableModelName: string;
  private _getPrintableModelName(): string {

    if (this._savedPrintableModelName) {
      return this._savedPrintableModelName;
    }

    let modelName: string = this._completeAnswers.modelName.trim();
    let splitted: string[] = modelName.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      if (splitted[i] === 'Model' || splitted[i] === 'model') {
        continue;
      }

      result += ConfigHelper.UcFirst(splitted[i]).trim();   

    }

    this._savedPrintableModelName = result;
    return this._savedPrintableModelName;

  }

  private _savedFilesPath: FilesPaths;
  private _getFilesPaths(): FilesPaths {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    let location: string = join((this._defaultAnswers.generateInModelsFolder) ? ConfigHelper.projectRootDir + 'src/server/models' : ConfigHelper.cwd, ConfigHelper.LcFirst(this._getPrintableModelName()));
    this._foldersToGenerate.push(location);

    return {
      interface: join(location, 'model.interface.ts'),
      declaration: join(location, 'model.declaration.ts'),
      model: join(location, ConfigHelper.LcFirst(this._getPrintableModelName()) + '.model.ts'),
      index: join(location, 'index.ts'),
    }

  }

  private _generateFolders(): void {

    let foldersLen: number = this._foldersToGenerate.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(this._foldersToGenerate[i]);     
    }

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      for (let i = 0; i < templatesLen; i++) {

        const templateName = templatesList[i];
        let pathToCheck = this._getFilesPaths()[templateName];

        if (existsSync(pathToCheck) && pathToCheck !== this._indexFileToUpdate) {
          this._existingFiles.push(pathToCheck);
        }

      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      try {

        this._generateFolders();
        
        for (let i = 0; i < templatesLen; i++) {
  
          const templateName = templatesList[i];
          let source: string = readFileSync(this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
          this._hbsTemplates[templateName] = compile(source)(this._completeAnswers);
          writeFileSync(this._getFilesPaths()[templateName], this._hbsTemplates[templateName], { encoding: 'utf-8' });
  
        }

        if (this._defaultAnswers.generateInModelsFolder) {

          let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
          let printableModelName: string = ConfigHelper.LcFirst(this._getPrintableModelName());
          let exportToAdd: string = `export * from './${printableModelName}';`

          if (!originalIndex.includes(exportToAdd)) {
            writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
          }

        }

        return resolve();

      } catch (error) {
        
        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  generateInModelsFolder: boolean,
  modelName: string,
  autoIncrement: boolean,
  softDelete: boolean,

  createdAt: boolean,
  createdBy: boolean,
  changesHistory: boolean,

  routable: boolean,
}

interface PublicRoutableAnswer {
  publicRoutable: boolean,
}

interface FilesPaths {
  declaration: string,
  model: string,
  interface: string,
  index: string
}