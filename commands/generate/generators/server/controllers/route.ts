import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from '../../../../../helpers/logger';
import { ConfigHelper } from '../../../../../helpers/config';
import { join } from 'path';
import { compile } from 'handlebars'
import { readFileSync, writeFileSync, ensureDirSync, existsSync } from 'fs-extra';

export class ServerRouteControllerGenerator {

  protected _projectTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'server/route-controller');
  protected _projectTemplatesPaths: FilesPaths = {
    controller: join(this._projectTemplatesDir, 'controllerName.route.controller.hbs'),
  };

  protected _hbsTemplates: FilesPaths = {
    controller: '',
  };

  protected _indexFileToUpdate: string = join(ConfigHelper.projectRootDir, 'src/server/controllers/routes/index.ts');

  protected _foldersToGenerate: string[] = [];

  private _defaultQuestions: QuestionCollection<DefaultAnswers> = [
    {
      message: 'Where would you like to generate controller',
      type: 'list',
      name: 'generateInControllersFolder',
      choices: [
        {
          name: `Default Handy controllers directory -> ~project/handy/src/server/controllers/routes/NEW_CONTROLLER`,
          value: true
        },
        {
          name: `Current directory -> ${ConfigHelper.cwd}NEW_CONTROLLER`,
          value: false
        },
      ],
      default: true
    },
    {
      message: 'Controller name',
      type: 'input',
      name: 'controllerName',
    },
    {
      message: 'Controller root url domain.com/',
      type: 'input',
      name: 'rootUrl',
    },
  ];

  private _defaultAnswers: DefaultAnswers;

  constructor (private _verifyFiles: boolean) {

  }

  public command(): void {

    this._prompt()
      .then(() => {

        if (this._defaultAnswers.controllerName.trim() === '') {
          return Promise.reject('Invalid controller name')
        }

        this._defaultAnswers.controllerName = this._getPrintableControllerName();
        
        if (this._defaultAnswers.rootUrl.trim() === '') {
          this._defaultAnswers.rootUrl = '/';
        }

        if (!this._defaultAnswers.rootUrl.startsWith('/')) {
          this._defaultAnswers.rootUrl = '/' + this._defaultAnswers.rootUrl;
        }

        return this._filesVerifyPrompt();

      })
      .then(() => {

        return this._checkFilesExistency();

      })
      .then(() => {

        return this._overWritePrompt();

      })
      .then(() => {

        return this._generateFiles();

      })
      .then(() => {

        Logger.success(`${ConfigHelper.UcFirst(this._defaultAnswers.controllerName)} was generated`);

      })
      .catch(err => {

        Logger.error(err);

      })

  }

  private _prompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<DefaultAnswers>(this._defaultQuestions)
        .then(answers => {

          this._defaultAnswers = answers;
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _filesVerifyPrompt(): Promise<void> {

    if (!this._verifyFiles) {
      return Promise.resolve();
    }

    this._logFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Generate files',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Yes',
              value: true
            },
            {
              name: 'No',
              value: false
            },
          ],
          default: true
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled controller generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _overWritePrompt(): Promise<void> {

    if (this._existingFiles.length === 0) {
      return Promise.resolve();
    }

    this._logExistingFilesToGenerate();

    return new Promise((resolve, reject) => {

      prompt([
        {
          message: 'Overwrite files anyhow',
          type: 'list',
          name: 'generate',
          choices: [
            {
              name: 'Owerwrite',
              value: true
            },
            {
              name: 'Cancel',
              value: false
            },
          ],
          default: false
        },
      ])
        .then(answers => {

          if (answers.generate) {
            return resolve();
          }

          return reject('User canceled controller generation');

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  private _logFilesToGenerate(): void {

    let filesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
    let filesLen: number = filesList.length;

    console.log();
    Logger.info('Files to be generated:')
    console.log();

    for (let i = 0; i < filesLen; i++) {
      const fileName = filesList[i];
      Logger.info(this._getFilesPaths()[fileName]);
    }


    if (this._defaultAnswers.generateInControllersFolder) {

      console.log();
      Logger.info('Files to be updated:')
      console.log();
      Logger.info(this._indexFileToUpdate);

    }

    console.log();

  }

  private _savedPrintableControllerName: string;
  private _getPrintableControllerName(): string {

    if (this._savedPrintableControllerName) {
      return this._savedPrintableControllerName;
    }

    let controllerName: string = this._defaultAnswers.controllerName.trim();
    let splitted: string[] = controllerName.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      if (splitted[i] === 'Controller' || splitted[i] === 'controller') {
        continue;
      }

      result += ConfigHelper.UcFirst(splitted[i]).trim();

    }

    this._savedPrintableControllerName = result;
    return this._savedPrintableControllerName;

  }

  private _savedFilesPath: FilesPaths;
  private _getFilesPaths(): FilesPaths {

    if (this._savedFilesPath) {
      return this._savedFilesPath;
    }

    let location: string = join((this._defaultAnswers.generateInControllersFolder) ? ConfigHelper.projectRootDir + 'src/server/controllers/routes' : ConfigHelper.cwd, ConfigHelper.LcFirst(this._getPrintableControllerName()));
    this._foldersToGenerate.push(location);

    return {
      controller: join(location, ConfigHelper.LcFirst(this._getPrintableControllerName()) + '.route.controller.ts')
    }

  }

  private _generateFolders(): void {

    let foldersLen: number = this._foldersToGenerate.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(this._foldersToGenerate[i]);
    }

  }

  private _existingFiles: string[] = [];
  private _checkFilesExistency(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      for (let i = 0; i < templatesLen; i++) {

        const templateName = templatesList[i];
        let pathToCheck = this._getFilesPaths()[templateName];

        if (existsSync(pathToCheck) && pathToCheck !== this._indexFileToUpdate) {
          this._existingFiles.push(pathToCheck);
        }

      }

      return resolve();

    })

  }

  private _logExistingFilesToGenerate(): void {

    let filesLen: number = this._existingFiles.length;

    console.log();
    Logger.warning('It seems like some of the to be generated files already exists:')
    console.log();

    let msg: string = '';

    for (let i = 0; i < filesLen; i++) {
      msg += this._existingFiles[i] + '\n';
    }

    Logger.warning(msg);

  }

  private _generateFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      let templatesList: (keyof FilesPaths)[] = Object.keys(this._getFilesPaths()) as (keyof FilesPaths)[];
      let templatesLen: number = templatesList.length;

      try {

        this._generateFolders();

        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesList[i];
          let source: string = readFileSync(this._projectTemplatesPaths[templateName], { encoding: 'utf-8' });
          this._hbsTemplates[templateName] = compile(source)(this._defaultAnswers);
          writeFileSync(this._getFilesPaths()[templateName], this._hbsTemplates[templateName], { encoding: 'utf-8' });

        }

        if (this._defaultAnswers.generateInControllersFolder) {

          let originalIndex: string = readFileSync(this._indexFileToUpdate, { encoding: 'utf-8' });
          let printableControllerName: string = ConfigHelper.LcFirst(this._getPrintableControllerName());
          let exportToAdd: string = `export * from './${printableControllerName}/${printableControllerName}.route.controller';`            

          if (!originalIndex.includes(exportToAdd)) {
            writeFileSync(this._indexFileToUpdate, `${originalIndex}\n${exportToAdd}`, { encoding: 'utf-8' });
          }

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface DefaultAnswers {

  generateInControllersFolder: boolean,
  controllerName: string,
  rootUrl: string

}

interface FilesPaths {
  controller: string,
}