import { prompt, QuestionCollection } from 'inquirer';
import { readFileSync, writeFileSync, ensureDirSync, existsSync, readdirSync } from 'fs-extra';
import { ConfigHelper } from './config';
import { join } from 'path';
import { compile } from 'handlebars';

let InnerHelper: ServerModelHelper;

export class ServerModelHelper {

  private _modelCliDataPath: string = join(ConfigHelper.projectRootDir, 'src/cli/server-ref-files/models');
  private _projectMagicBoxPath: string = join(ConfigHelper.projectRootDir, 'magic-box');
  private _generatingForModule: boolean = false;
  private _moduleName: string;

  private _modelsList: string[] = [];
  private _selectedModelName: string;
  private _selectedModelData: ModelData;

  /* ---------------------------------- Table --------------------------------- */
  protected _projectTableTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-table-component');
  protected _projectTableTemplatesPaths: any = {
    styles: join(this._projectTableTemplatesDir, 'tableName.styles.hbs'),
    html: join(this._projectTableTemplatesDir, 'tableName.html.hbs'),
    logic: join(this._projectTableTemplatesDir, 'tableName.logic.hbs'),
  };

  /* ---------------------------------- Form ---------------------------------- */
  protected _projectFormTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-form-component');
  protected _projectFormTemplatesPaths: any = {
    styles: join(this._projectFormTemplatesDir, 'formName.styles.hbs'),
    html: join(this._projectFormTemplatesDir, 'formName.html.hbs'),
    logic: join(this._projectFormTemplatesDir, 'formName.logic.hbs'),
  };

  protected _hbsFormTemplates = {
    styles: '',
    html: '',
    logic: '',
  };

  /* --------------------------------- Module --------------------------------- */
  protected _projectModuleTemplatesDir: string = join(ConfigHelper.projectCliTemplatesPath(), 'webApp/crud-module');
  protected _projectModuleTemplatesPaths: any = {
    styles: join(this._projectModuleTemplatesDir, 'moduleName.component.styles.hbs'),
    html: join(this._projectModuleTemplatesDir, 'moduleName.component.html.hbs'),
    logic: join(this._projectModuleTemplatesDir, 'moduleName.component.logic.hbs'),
    module: join(this._projectModuleTemplatesDir, 'moduleName.module.hbs'),
    routing: join(this._projectModuleTemplatesDir, 'moduleName.routing.hbs'),
  };

  protected _hbsModuleTemplates: any = {
    styles: '',
    html: '',
    logic: '',
    module: '',
    routing: ''
  };

  private _modelsSelectionQuestion: QuestionCollection<SelectModelAnswes>;

  constructor () {

    this._getModelsList();

  }

  private _getModelsList(): void {

    let modelsFilesList: string[] = readdirSync(this._modelCliDataPath);

    let filesLen: number = modelsFilesList.length;

    for (let i = 0; i < filesLen; i++) {
      const fileName = modelsFilesList[i];

      if (!fileName.endsWith('modelData.json')) {
        continue;
      }

      this._modelsList.push(fileName.split('.')[0]);

    }

    let choices: { name: string, value: string }[] = [];
    let modelsLen: number = this._modelsList.length;
    for (let i = 0; i < modelsLen; i++) {
      const modelName = this._modelsList[i];

      choices.push({
        name: ConfigHelper.UcFirst(modelName),
        value: modelName,
      })

    }

    this._modelsSelectionQuestion = [
      {
        message: 'Select model',
        type: 'list',
        name: 'modelName',
        choices
      }
    ];

  }

  private _parseSelectedModelData(): void {

    let modelDataFileContent = readFileSync(join(this._modelCliDataPath, `${this._selectedModelName}.modelData.json`), { encoding: 'utf-8' });
    this._selectedModelData = JSON.parse(modelDataFileContent);

  }

  public static getIntance(): ServerModelHelper {

    if (!InnerHelper) {
      InnerHelper = new ServerModelHelper()
    }

    return InnerHelper;

  }

  /* ----------------------------- Model selecting ---------------------------- */
  private _selectModelPrompt(): Promise<void> {

    if (this._selectedModelName) {
      return Promise.resolve();
    }

    return new Promise((resolve, reject) => {

      prompt<SelectModelAnswes>(this._modelsSelectionQuestion)
        .then(answers => {

          this._selectedModelName = answers.modelName;
          this._parseSelectedModelData();
          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static selectModelPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectModelPrompt();
  }

  /* -------------------------------------------------------------------------- */
  /*                             Crud table related                             */
  /* -------------------------------------------------------------------------- */

  /* --------------------- Table display columns selecting -------------------- */
  private _selectedTableDisplayColumns: ModelFieldData[] = [];
  private _selectedTableDisplayColumnsUnionStr: string = '';
  private _selectedTableDisplayColumnsArrStr: string = '';
  private _selectedTableFieldsToQueryArrStr: string = '';
  private _selectTableDisplayColumnsPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      let actionsField: ModelFieldData = {
        name: 'actions',
        inputType: 'none',
        publicName: 'Actions',
        unique: false,
        required: false
      }

      let choices: { value: ModelFieldData, name: string, checked: boolean }[] = [

      ];

      let fieldsLen: number = this._selectedModelData.fields.length;
      for (let i = 0; i < fieldsLen; i++) {
        let field = this._selectedModelData.fields[i];

        choices.push({
          value: field,
          name: field.publicName,
          checked: true
        })

      }

      choices.push({
        value: actionsField,
        name: 'Actions',
        checked: true
      })

      let promtpQuestion: QuestionCollection<{ columns: ModelFieldData[] }> = [
        {
          message: 'Select table fields to include as a display columns',
          name: 'columns',
          type: 'checkbox',
          choices,
          default: []
        }
      ];

      prompt(promtpQuestion)
        .then(answers => {

          this._selectedTableDisplayColumns = answers.columns;

          let selecteColumnsLen: number = this._selectedTableDisplayColumns.length;
          for (let i = 0; i < selecteColumnsLen; i++) {
            const element = this._selectedTableDisplayColumns[i];
            this._selectedTableDisplayColumnsUnionStr += `'${element.name}'`;
            this._selectedTableDisplayColumnsArrStr += `'${element.name}'`;

            if (element.name !== 'actions') {
              this._selectedTableFieldsToQueryArrStr += `'${element.name}'`;
            }

            if (i + 1 < selecteColumnsLen) {
              this._selectedTableDisplayColumnsUnionStr += ` | `;
              this._selectedTableDisplayColumnsArrStr += `, `;

              if (element.name !== 'actions') {
                this._selectedTableFieldsToQueryArrStr += `, `;
              }
            }

          }

          // this._selectedTableDisplayColumnsUnionStr = this._selectedTableDisplayColumnsUnionStr.trim();

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static selectTableDisplayColumnsPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectTableDisplayColumnsPrompt();
  }

  /* -------------------- Optional dislay columns selecting ------------------- */

  private _selectedTableOptionalDisplayColumns: ModelFieldData[] = [];
  private _hasTableOptionalDisplayColumns: boolean;
  private _selectedTableOptionalDisplayColumnsUnionStr: string = '';
  private _selectTableOptionalDisplayColumnsPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Make some table columns optional')
        .then(result => {

          this._hasTableOptionalDisplayColumns = result;
          if (!result) {
            resolve();
            return;
          }

          let choices: { value: ModelFieldData, name: string, checked: boolean }[] = [];

          let fieldsLen: number = this._selectedTableDisplayColumns.length;
          for (let i = 0; i < fieldsLen; i++) {
            let { name, publicName } = this._selectedTableDisplayColumns[i];
            choices.push({
              value: this._selectedTableDisplayColumns[i],
              name: publicName,
              checked: (name !== 'actions' && name !== '_id')
            })

          }

          let promtpQuestion: QuestionCollection<{ columns: ModelFieldData[] }> = [
            {
              message: 'Select table columns that can be optiona',
              name: 'columns',
              type: 'checkbox',
              choices,
              default: []
            }
          ];

          prompt(promtpQuestion)
            .then(answers => {

              this._selectedTableOptionalDisplayColumns = answers.columns;

              let selecteColumnsLen: number = this._selectedTableOptionalDisplayColumns.length;
              for (let i = 0; i < selecteColumnsLen; i++) {
                const element = this._selectedTableOptionalDisplayColumns[i];
                this._selectedTableOptionalDisplayColumnsUnionStr += `'${element.name}'`;

                if (i + 1 < selecteColumnsLen) {
                  this._selectedTableOptionalDisplayColumnsUnionStr += ` | `;
                }

              }

              this._selectedTableOptionalDisplayColumnsUnionStr = this._selectedTableOptionalDisplayColumnsUnionStr.trim();

              return resolve();

            })
            .catch(err => {

              reject(err);

            })

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static selectTableOptionalDisplayColumnsPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectTableOptionalDisplayColumnsPrompt();
  }

  /* ------------------------- Table searchable fields ------------------------ */
  private _selectedTableSearchColumns: ModelData['searchableFields'] = [];
  private _includeSearch: boolean;
  private _selectedTableSearchColumnsUnionStr: string = '';
  private _selectTableSearchColumnsPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Include search in table')
        .then(result => {

          this._includeSearch = result;
          if (!result) {
            this._selectedTableSearchColumns = []
            resolve();
            return;
          }

          let choices: { value: ModelData['searchableFields'][number], name: string, checked: boolean }[] = [];

          let fieldsLen: number = this._selectedModelData.searchableFields.length;
          for (let i = 0; i < fieldsLen; i++) {
            let field = this._selectedModelData.searchableFields[i];
            choices.push({
              value: field,
              name: field.displayValue,
              checked: true
            })

          }

          let promtpQuestion: QuestionCollection<{ columns: ModelData['searchableFields'] }> = [
            {
              message: 'Select fields to search through while using table search',
              name: 'columns',
              type: 'checkbox',
              choices,
              default: []
            }
          ];

          prompt(promtpQuestion)
            .then(answers => {

              this._selectedTableSearchColumns = answers.columns;
              resolve();
              return;

            })
            .catch(err => {

              reject(err);
              return;

            })

        })
        .catch(err => {

          reject(err);
          return;

        })

    })

  }

  public static selectTableSearchColumnsPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectTableSearchColumnsPrompt();
  }

  /* ------------------------- Table searchable fields ------------------------ */
  private _selectedTableFilterColumns: ModelData['fields'] = [];
  private _includeFilter: boolean;
  private _selectedTableFilterColumnsUnionStr: string = '';
  private _selectTableFilterColumnsPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Include extra table filters')
        .then(result => {

          this._includeFilter = result;
          if (!result) {
            this._selectedTableFilterColumns = []
            resolve();
            return;
          }

          let choices: { value: ModelData['fields'][number], name: string, checked: boolean }[] = [];

          let fieldsLen: number = this._selectedModelData.fields.length;
          for (let i = 0; i < fieldsLen; i++) {
            let field = this._selectedModelData.fields[i];
            choices.push({
              value: field,
              name: field.publicName,
              checked: true
            })

          }

          let promtpQuestion: QuestionCollection<{ columns: ModelData['fields'] }> = [
            {
              message: 'Select fields for additional table filters',
              name: 'columns',
              type: 'checkbox',
              choices,
              default: []
            }
          ];

          prompt(promtpQuestion)
            .then(answers => {

              this._selectedTableFilterColumns = answers.columns;
              resolve();
              return;

            })
            .catch(err => {

              reject(err);
              return;

            })

        })

        .catch(err => {

          reject(err);
          return;

        })

    })

  }

  public static selectTableFilterColumnsPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectTableFilterColumnsPrompt();
  }

  private _tableName: string;
  private _tableNamePrompt(defaultVal?: string): Promise<void> {

    if (!defaultVal) {
      defaultVal = `${ConfigHelper.UcFirst(this._selectedModelName)} crud table`;
    }

    return new Promise((resolve, reject) => {

      ConfigHelper.simpletInputPrompt('Table name', defaultVal)
        .then(result => {

          this._tableName = ConfigHelper.getPrintableString(result);
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static tableNamePrompt(defaultVal?: string): Promise<void> {
    return ServerModelHelper.getIntance()._tableNamePrompt(defaultVal);
  }

  private _rememberTableState: boolean;
  private _rememberTableStatePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Remember table in user state')
        .then(result => {

          this._rememberTableState = result;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static rememberTableStatePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._rememberTableStatePrompt();
  }

  public static getStandAloneTableGenerationData(): StandAloneTableGenerationData {

    let instance: ServerModelHelper = ServerModelHelper.getIntance();

    let {
      _tableName,
      _selectedModelName,
      _rememberTableState,
      _selectedTableDisplayColumnsArrStr,
      _selectedTableFieldsToQueryArrStr,
      _hasTableOptionalDisplayColumns,
      _selectedTableOptionalDisplayColumns,
      _includeSearch,
      _selectedTableSearchColumns,
      _includeFilter,
      _selectedTableFilterColumns,
      _selectedTableDisplayColumnsUnionStr,
      _selectedTableDisplayColumns,
      _generatingForModule,
      _moduleName,
      _moduleEntryType,
      _selectedModelData,
      _formName
    } = instance;

    return {
      modelName: _selectedModelName,
      tableName: (_tableName === '') ? ConfigHelper.getPrintableString(_selectedModelName + 's table') : ConfigHelper.getPrintableString(_tableName),
      forModule: _generatingForModule,
      moduleName: _moduleName,
      formName: ConfigHelper.getPrintableString(_formName),
      entryType: _moduleEntryType,
      pageEntry: _moduleEntryType === 'page',
      modalEntry: _moduleEntryType === 'modal',
      autoIncrement: _selectedModelData.autoIncrement,
      rememberTableState: _rememberTableState,
      fieldsToQueryStr: _selectedTableFieldsToQueryArrStr,
      displayedCols: _selectedTableDisplayColumns,
      displayedColsStr: _selectedTableDisplayColumnsArrStr,
      displayColumnsUnionStr: _selectedTableDisplayColumnsUnionStr,
      hasOptionalCols: _hasTableOptionalDisplayColumns,
      optionalCols: _selectedTableOptionalDisplayColumns,
      hasSearch: _includeSearch,
      searchableFields: _selectedTableSearchColumns,
      hasAdditionalFilters: _includeFilter,
      aditionalFilters: _selectedTableFilterColumns,
      hasActionsCol: _selectedTableDisplayColumnsArrStr.includes(`'actions'`),
      hasSearchOrFilter: _includeSearch || _includeFilter || _hasTableOptionalDisplayColumns,
      hasOptionalColsOrFilter: _includeFilter || _hasTableOptionalDisplayColumns
    }

  }

  public static getSelectedModelData(): ModelData {
    return ServerModelHelper.getIntance()._selectedModelData;
  }

  public static generateTableComponentFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        ConfigHelper.generateFolders([ConfigHelper.magicBoxPath]);

        let instance = this.getIntance();
        let printableTableName: string = (instance._tableName !== '') ? ConfigHelper.getPrintableString(instance._tableName) : ConfigHelper.getPrintableString(instance._selectedModelName + 's table');
        let tableNameFilePathStr: string = ConfigHelper.getFileNameString(printableTableName);

        let folders: string[] = [];
        if (instance._generatingForModule) {

          let generatedModulePath: string = join(instance._projectMagicBoxPath, ConfigHelper.getFileNameString(instance._moduleName));
          folders = [
            generatedModulePath,
            join(generatedModulePath, tableNameFilePathStr),
          ];

        } else {

          folders = [
            join(instance._projectMagicBoxPath, tableNameFilePathStr)
          ];

        }

        ConfigHelper.generateFolders(folders);

        let templatesNames: any[] = Object.keys(instance._projectTableTemplatesPaths);

        let templatesLen: number = templatesNames.length;
        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesNames[i] as any;
          let source: string = readFileSync(instance._projectTableTemplatesPaths[templateName], { encoding: 'utf-8' });
          let compiledTemplate = compile(source)(this.getStandAloneTableGenerationData());

          let filePath: string;
          let rootPath: string = (instance._generatingForModule) ? join(instance._projectMagicBoxPath, ConfigHelper.getFileNameString(instance._moduleName)) : instance._projectMagicBoxPath;

          switch (templateName) {
            case 'styles':
              filePath = join(rootPath, tableNameFilePathStr, `${tableNameFilePathStr}.component.scss`)
              break;

            case 'html':
              filePath = join(rootPath, tableNameFilePathStr, `${tableNameFilePathStr}.component.html`)
              break;

            default:
              filePath = join(rootPath, tableNameFilePathStr, `${tableNameFilePathStr}.component.ts`)
              break;
          }

          // console.log(filePath)
          writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

  /* -------------------------------------------------------------------------- */
  /*                              Crud form related                             */
  /* -------------------------------------------------------------------------- */

  /* -------------------------- Form fields selecting ------------------------- */
  private _selectedFormFields: ModelFieldData[] = [];
  private _selectedFormFieldsUnionStr: string = '';
  private _selectedFormFieldsArrStr: string = '';
  private _selectedFormFieldsPrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      let choices: { value: ModelFieldData, name: string, checked: boolean }[] = [

      ];

      let fieldsLen: number = this._selectedModelData.fields.length;
      for (let i = 0; i < fieldsLen; i++) {
        let field = this._selectedModelData.fields[i];

        choices.push({
          value: field,
          name: field.publicName,
          checked: (field.name !== '_id')
        })

      }

      let promtpQuestion: QuestionCollection<{ fields: ModelFieldData[] }> = [
        {
          message: 'Select fields to include in crud form',
          name: 'fields',
          type: 'checkbox',
          choices,
          default: []
        }
      ];

      prompt(promtpQuestion)
        .then(answers => {

          this._selectedFormFields = answers.fields;

          let selectedFormFieldsLen: number = this._selectedFormFields.length;
          for (let i = 0; i < selectedFormFieldsLen; i++) {
            const element = this._selectedFormFields[i];
            this._selectedFormFieldsUnionStr += `'${element.name}'`;
            this._selectedFormFieldsArrStr += `'${element.name}'`;

            if (i + 1 < selectedFormFieldsLen) {
              this._selectedFormFieldsUnionStr += ` | `;
              this._selectedFormFieldsArrStr += `, `;
            }

          }

          return resolve();

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static selectedFormFieldsPrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._selectedFormFieldsPrompt();
  }

  /* ------------------------------- Form state ------------------------------- */
  private _rememberFormState: boolean;
  private _rememberFormStatePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Remember form in user state')
        .then(result => {

          this._rememberFormState = result;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static rememberFormStatePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._rememberFormStatePrompt();
  }

  /* -------------------------------- Form name ------------------------------- */
  private _formName: string;
  private _formNamePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      ConfigHelper.simpletInputPrompt('Form name', ConfigHelper.UcFirst(this._selectedModelName + ' crud form'))
        .then(result => {

          if (!result) {
            result = this._selectedModelName + ' form';
          }

          this._formName = ConfigHelper.getPrintableString(result);
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static formNamePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._formNamePrompt();
  }

  /* -------------------------------- Form type ------------------------------- */
  private _formType: 'create' | 'update' | 'both' = 'both';
  private _formTypePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<{ formType: 'create' | 'update' | 'both' }>({
        name: 'formType',
        message: 'What type of form would you like to create',
        type: 'list',
        choices: [
          {
            name: 'Create',
            value: 'create'
          },
          {
            name: 'Update',
            value: 'update'
          },
          {
            name: 'Both',
            value: 'both'
          }
        ],
        default: 'both'
      })
        .then(result => {

          this._formType = result.formType;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static formTypePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._formTypePrompt();
  }

  /* ---------------------------- Include resolver ---------------------------- */
  private _includeResolver: boolean = true;
  private _includeResolverPrompt(): Promise<void> {

    if (this._formType === 'create') {
      return Promise.resolve();
    }

    return new Promise((resolve, reject) => {

      ConfigHelper.simpleYesNoPrompt('Would you like to create a resolver for update form', true)
        .then(result => {

          this._includeResolver = result;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static includeResolver(): Promise<void> {
    return ServerModelHelper.getIntance()._includeResolverPrompt();
  }

  public static getStandAloneFormGenerationData(): StandAloneFormGenerationData {

    let instance: ServerModelHelper = ServerModelHelper.getIntance();

    let {
      _selectedModelName,
      _formName,
      _rememberFormState,
      _selectedFormFieldsUnionStr,
      _selectedFormFieldsArrStr,
      _selectedFormFields,
      _formType,
      _includeResolver,
      _selectedModelData,
      _generatingForModule,
      _moduleName, 
      _moduleEntryType
    } = instance;

    let formArraysFields: string[] = [];
    let hasFormArraysFields: boolean = false;
    let selectOptionsFields: string[] = [];
    let hasSelectOptionsFields: boolean = false;
    let radioGroupOptionsFields: string[] = [];
    let hasRadioGroupOptionsFields: boolean = false;

    let fieldsLen: number = _selectedFormFields.length;
    for (let index = 0; index < fieldsLen; index++) {
      const { name, inputType } = _selectedFormFields[index];

      if (inputType.includes('[') && inputType !== '[Files]') {
        hasFormArraysFields = true;
        formArraysFields.push(name);
      }

      if (inputType.includes('radio-group')) {
        hasRadioGroupOptionsFields = true;
        radioGroupOptionsFields.push(name);
      }

      if (inputType.includes('select')) {
        hasSelectOptionsFields = true;
        selectOptionsFields.push(name);
      }

    }

    return {
      modelName: _selectedModelName,
      formName: _formName,
      forModule: _generatingForModule,
      moduleName: _moduleName,
      rememberFormState: _rememberFormState,
      fieldsUnionStr: _selectedFormFieldsUnionStr,
      fieldsArrStr: _selectedFormFieldsArrStr,
      fields: _selectedFormFields,
      formType: _formType,
      includeResolver: _includeResolver,
      includeCreate: (_formType !== 'update'),
      includeUpdate: (_formType !== 'create'),
      formArraysFields,
      hasFormArraysFields,
      selectOptionsFields,
      hasSelectOptionsFields,
      radioGroupOptionsFields,
      hasRadioGroupOptionsFields,
      autoIncrement: _selectedModelData.autoIncrement,
      separateCrudMethods: (_formType === 'both'),
      updateOnly: (_formType === 'update'),
      createOnly: (_formType === 'create'),
      entryType: _moduleEntryType,
      pageEntry: _moduleEntryType === 'page',
      modalEntry: _moduleEntryType === 'modal',
    }

  }

  public static generateFormComponentFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        console.log(ConfigHelper.magicBoxPath)

        ConfigHelper.generateFolders([ConfigHelper.magicBoxPath]);

        let instance = this.getIntance();
        let printableFormName: string = ConfigHelper.getPrintableString(instance._formName);
        let formNameFilePathStr: string = ConfigHelper.getFileNameString(printableFormName);

        let folders: string[] = [];
        if (instance._generatingForModule) {

          let generatedModulePath: string = join(instance._projectMagicBoxPath, ConfigHelper.getFileNameString(instance._moduleName));
          folders = [
            generatedModulePath,
            join(generatedModulePath, formNameFilePathStr),
          ];

        } else {

          folders = [
            join(instance._projectMagicBoxPath, formNameFilePathStr)
          ];

        }

        ConfigHelper.generateFolders(folders);

        let templatesNames: any[] = Object.keys(instance._projectFormTemplatesPaths);

        let templatesLen: number = templatesNames.length;
        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesNames[i] as any;
          let source: string = readFileSync(instance._projectFormTemplatesPaths[templateName], { encoding: 'utf-8' });
          let compiledTemplate = compile(source)(this.getStandAloneFormGenerationData());

          let filePath: string;
          let rootPath: string = (instance._generatingForModule) ? join(instance._projectMagicBoxPath, ConfigHelper.getFileNameString(instance._moduleName)) : instance._projectMagicBoxPath;

          switch (templateName) {
            case 'styles':
              filePath = join(rootPath, formNameFilePathStr, `${formNameFilePathStr}.component.scss`)
              break;

            case 'html':
              filePath = join(rootPath, formNameFilePathStr, `${formNameFilePathStr}.component.html`)
              break;

            default:
              filePath = join(rootPath, formNameFilePathStr, `${formNameFilePathStr}.component.ts`)
              break;
          }

          writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

  /* -------------------------------------------------------------------------- */
  /*                             Crud Module related                            */
  /* -------------------------------------------------------------------------- */

  private _moduleNamePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      let defaultName: string = ConfigHelper.UcFirst(this._selectedModelName + ' crud');

      ConfigHelper.simpletInputPrompt('Module name', defaultName)
        .then(result => {

          if (!result) {
            result = defaultName;
          }

          this._moduleName = ConfigHelper.getPrintableString(result);
          this._generatingForModule = true;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static moduleNamePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._moduleNamePrompt();
  }

  /* -------------------------- Entry type model/page ------------------------- */
  private _moduleEntryType: 'modal' | 'page' = 'page';
  private _moduleEntryTypePrompt(): Promise<void> {

    return new Promise((resolve, reject) => {

      prompt<{ type: 'modal' | 'page' }>([
        {
          name: 'type',
          message: 'What kind of data entry would you like to have',
          type: 'list',
          choices: [
            {
              value: 'page',
              name: 'Dedicated pages for update and creating entries'
            },
            {
              value: 'modal',
              name: 'Modal dialog for update and creating entries'
            },
          ]
        }
      ])
        .then(result => {

          this._moduleEntryType = result.type;
          return resolve();

        })
        .catch(err => {
          return reject(err);
        })


    })

  }

  public static moduleEntryTypePrompt(): Promise<void> {
    return ServerModelHelper.getIntance()._moduleEntryTypePrompt();
  }

  public static getStandAloneModuleGenerationData(): StandAloneModuleGenerationData {

    let instance: ServerModelHelper = this.getIntance();

    let {
      _selectedModelName,
      _formName,
      _moduleEntryType,
      _moduleName,
      _tableName
    } = instance;

    return {
      modelName: _selectedModelName,
      formName: _formName,
      tableName: _tableName,
      moduleName: _moduleName,
      entryType: _moduleEntryType,
      pageEntry: (_moduleEntryType == 'page'),
      modalEntry: (_moduleEntryType == 'modal'),
    }

  }

  public static generateModuleFiles(): Promise<void> {

    return new Promise((resolve, reject) => {

      try {

        let instance = this.getIntance();

        let moduleNameFilePathStr: string = ConfigHelper.getFileNameString(instance._moduleName);
        let generatedModulePath: string = join(instance._projectMagicBoxPath, moduleNameFilePathStr);

        ConfigHelper.generateFolders([generatedModulePath]);

        let templatesNames: any[] = Object.keys(instance._projectModuleTemplatesPaths);

        let templatesLen: number = templatesNames.length;
        for (let i = 0; i < templatesLen; i++) {

          const templateName = templatesNames[i] as any;
          let source: string = readFileSync(instance._projectModuleTemplatesPaths[templateName], { encoding: 'utf-8' });
          let compiledTemplate = compile(source)(this.getStandAloneModuleGenerationData());

          let filePath: string;

          switch (templateName) {
            case 'styles':
              filePath = join(generatedModulePath, `${moduleNameFilePathStr}.component.scss`)
              break;

            case 'html':
              filePath = join(generatedModulePath, `${moduleNameFilePathStr}.component.html`)
              break;
            
            case 'module':
              filePath = join(generatedModulePath, `${moduleNameFilePathStr}.module.ts`)
              break;
            
            case 'routing':
              filePath = join(generatedModulePath, `${moduleNameFilePathStr}-routing.module.ts`)
              break;

            default:
              filePath = join(generatedModulePath, `${moduleNameFilePathStr}.component.ts`)
              break;
          }

          writeFileSync(filePath, compiledTemplate, { encoding: 'utf-8' });

        }

        return resolve();

      } catch (error) {

        return reject(error)

      }

    })

  }

}

interface SelectModelAnswes {
  modelName: string
}

export interface ModelFieldData {
  name: string,
  publicName: string,
  inputType: string,
  unique: 'all' | 'active' | boolean,
  required: boolean
}

interface ModelData {
  modelName: string,
  autoIncrement: boolean,
  fields: ModelFieldData[],
  searchableFields: { name: string, displayValue: string }[]
}

interface StandAloneTableGenerationData {
  modelName: string,
  tableName: string,
  formName: string,
  forModule: boolean,
  moduleName: string,
  entryType: 'page' | 'modal',
  pageEntry: boolean,
  modalEntry: boolean,
  autoIncrement: boolean,
  rememberTableState: boolean,
  fieldsToQueryStr: string,
  displayedCols: ModelFieldData[],
  displayedColsStr: string,
  displayColumnsUnionStr: string,
  hasOptionalCols: boolean,
  optionalCols: ModelFieldData[],
  hasSearch: boolean,
  searchableFields: ModelData['searchableFields'],
  hasAdditionalFilters: boolean,
  aditionalFilters: ModelFieldData[],
  hasActionsCol: boolean,
  hasSearchOrFilter: boolean,
  hasOptionalColsOrFilter: boolean,
}

interface StandAloneFormGenerationData {
  modelName: string,
  formName: string,
  forModule: boolean,
  moduleName: string,
  rememberFormState: boolean,
  fieldsUnionStr: string,
  fieldsArrStr: string,
  fields: ModelFieldData[],
  includeResolver: boolean,
  formType: 'create' | 'update' | 'both',
  includeCreate: boolean,
  includeUpdate: boolean,
  formArraysFields: string[],
  hasFormArraysFields: boolean,
  selectOptionsFields: string[],
  hasSelectOptionsFields: boolean,
  radioGroupOptionsFields: string[],
  hasRadioGroupOptionsFields: boolean,
  autoIncrement: boolean,
  separateCrudMethods: boolean,
  updateOnly: boolean,
  createOnly: boolean,
  entryType: 'page' | 'modal',
  pageEntry: boolean,
  modalEntry: boolean
}

interface StandAloneModuleGenerationData {
  modelName: string,
  moduleName: string,
  formName: string,
  tableName: string,
  entryType: 'page' | 'modal',
  pageEntry: boolean,
  modalEntry: boolean
}