import { ModelFieldData } from './server.models.helper';
import { ConfigHelper } from './config';

type FieldTypes =
  'text' | 'number' | 'password' | 'rich-text' | 'none' | 'text-area' | 'time-number' | 'time-string' |
  'multi-select' | 'select' | 'date' | 'date-range' | 'slider' | 'slide-toggle' |
  'check-box' | 'radio-group' | '[number]' | '[text]' | '[password]' | '[text-area]' |
  '[time-number]' | '[time-string]' | '[rich-text]' | '[date]' | '[slider]' |
  '[select]' | '[radio-group]' | '[slide-toggle]' | '[date-range]' | '[Files]';

export const fieldValueRenderer = (fieldData: ModelFieldData, formName: string = ''): string => {

  let { inputType, name, publicName } = fieldData;
  let fieldType: FieldTypes = inputType as FieldTypes;

  let result: string = '';
  switch (fieldType) {

    case 'text':

      result = `<handy-text-input class="handy-form-input" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-text-input>`;

      break;

    case '[text]':

      result = wrapInArray(fieldData, `<handy-text-input [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-text-input>`);

      break;

    case 'number':

      result = `<handy-number-input class="handy-form-input" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-number-input>`;

      break;

    case '[number]':

      result = wrapInArray(fieldData, `<handy-number-input [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-number-input>`);

      break;

    case 'password':

      result = `<handy-password-input class="handy-form-input" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-password-input>`;

      break;

    case '[password]':

      result = wrapInArray(fieldData, `<handy-password-input [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-password-input>`);

      break;

    case 'rich-text':

      result = `<handy-rte-input fxFlex="100%" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" placeholder="${publicName}"></handy-rte-input>`;

      break;

    case '[rich-text]':

      result = wrapInArray(fieldData, `<handy-rte-input fxFlex="100%" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" placeholder="${publicName}"></handy-rte-input>`);

      break;

    case 'none':

      result = `<p>Create custom input for ${publicName}</p>`;

      break;

    case 'text-area':

      result = `<handy-textarea-input class="handy-form-input" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-textarea-input>`;

      break;

    case '[text-area]':

      result = wrapInArray(fieldData, `<handy-textarea-input [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-textarea-input>`);

      break;

    case 'time-number':

      result = `<handy-time-input class="handy-form-input" valueType="seconds" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-time-input>`;

      break;

    case '[time-number]':

      result = wrapInArray(fieldData, `<handy-time-input valueType="seconds" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-time-input>`);

      break;

    case 'time-string':

      result = `<handy-time-input class="handy-form-input" valueType="string" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-time-input>`;

      break;

    case '[time-string]':

      result = wrapInArray(fieldData, `<handy-time-input valueType="string" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-time-input>`);

      break;

    case 'multi-select':

      result = `<handy-multi-select-input class="handy-form-input" [options]="${name}SelectOptions" [hasEmptyOption]="true" emptyOptionLabel="None" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-multi-select-input>`;

      break;

    case 'select':

      result = `<handy-select-input class="handy-form-input" [options]="${name}SelectOptions" [hasEmptyOption]="true" emptyOptionLabel="None" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-select-input>`;

      break;

    case '[select]':

      result = wrapInArray(fieldData, `<handy-select-input [options]="${name}SelectOptions" [hasEmptyOption]="true" emptyOptionLabel="None" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-select-input>`);

      break;

    case 'date':

      result = `<handy-date-input class="handy-form-input" [getTime]="false" [showTimeZone]="true" [selectableTimezone]="false" [multiSelect]="false" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-date-input>`;

      break;

    case '[date]':

      result = wrapInArray(fieldData, `<handy-date-input [getTime]="false" [showTimeZone]="true" [selectableTimezone]="false" [multiSelect]="false" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-date-input>`);

      break;

    case 'date-range':

      result = `<handy-date-range-input class="handy-form-input" [getTime]="false" [showTimeZone]="true" [selectableTimezone]="false" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-date-range-input>`;

      break;

    case '[date-range]':

      result = wrapInArray(fieldData, `<handy-date-range-input [getTime]="false" [showTimeZone]="true" [selectableTimezone]="false" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}" placeholder="${publicName}"></handy-date-range-input>`);

      break;

    case 'slider':

      result = `<handy-slider-input class="handy-form-input" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"></handy-slider-input>`;

      break;

    case '[slider]':

      result = wrapInArray(fieldData, `<handy-slider-input [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"></handy-slider-input>`);

      break;

    case 'slide-toggle':

      result = `<handy-slide-toggle class="handy-form-input" labelPosition="after" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"> ${publicName} </handy-slide-toggle>`;

      break;
    
    case '[slide-toggle]':

      result = wrapInArray(fieldData, `<handy-slide-toggle labelPosition="after" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"> ${publicName} </handy-slide-toggle>`);

      break;

    case 'check-box':

      result = `<handy-check-box class="handy-form-input" labelPosition="after" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"> ${publicName} </handy-check-box>`;

      break;

    case 'radio-group':

      result = `<handy-radio-group-input class="handy-form-input" [groupOptions]="${name}RadioBtns" labelPosition="after" [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}"></handy-radio-group-input>`;

      break;

    case '[radio-group]':

      result = wrapInArray(fieldData, `<handy-radio-group-input [groupOptions]="${name}RadioBtns" labelPosition="after" [formControl]="fControl" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}" label="${publicName}"></handy-radio-group-input>`);

      break;

    case '[Files]':

      result = `<handy-file-input [formControl]="form.get('${name}')" fieldName="${ConfigHelper.getPrintableString(formName)}_${name}"></handy-file-input>`;

      break;

    default:

      break;
  }

  return result;

}

const wrapInArray = (fieldData: ModelFieldData, singleFieldToRender: string): string => {

  let result = `<div fxFlex="100%">\n\n`;
  result += `  <div *ngFor="let fControl of getArrayControls('${fieldData.name}'), let i = index">\n\n`;
  result += `    ${singleFieldToRender}\n\n`;

  result += `    <stroked-btn icon="times" color="warn" (click)="removeControlFromArray('${fieldData.name}', i)">Remove</stroked-btn>\n\n`;
  
  result += `  </div>\n\n`;

  result += `  <stroked-btn icon="plus" color="primary" (click)="add${ConfigHelper.UcFirst(fieldData.name)}Control()">Add ${fieldData.publicName} control</stroked-btn>\n\n`;

  result += `</div>`;

  return result;

}

export const fieldFormControlRenderer = (fieldData: ModelFieldData): string => {

  let { inputType, name, publicName, unique = false, required = false } = fieldData;

  let syncValidator: string = (required) ? `required('${ConfigHelper.UcFirst(publicName)} is required')` : `/* Sync validators */`;
  let asyncValidator: string = `/* Async validators */`;

  if (unique !== false) {
    
    switch (unique) {
      case true:
        
        asyncValidator = `this._model.uniqueValidator('${name}', 'This ${ConfigHelper.LcFirst(publicName)} is taken', this.isUpdate ? formInitData.${name}) : null`;

        break;
    
      default:

        asyncValidator = `this._model.uniqueValidator('${name}', 'This ${ConfigHelper.LcFirst(publicName)} is taken', this.isUpdate ? formInitData.${name} : null, '${unique}')`

        break;
    }

  }

  return ((inputType.includes('[')) && inputType !== '[Files]') ? `${name}: new FormArray([]),` : `${name}: new FormControl(formInitData.${name}, [${syncValidator}], [${asyncValidator}]),`

}