import { red, green, keyword, blue, bold } from 'chalk';
let orange = keyword('orange');
import { Spinner } from 'cli-spinner'

export class Logger {

  constructor() {

  }

  public static info(msg: any): void {

    console.log(blue(msg));

  }

  public static success(msg: any): void {

    console.log(green(msg));

  }
  
  public static error(msg: any): void {

    console.log(red.bold(`! Error`));
    console.log(red(msg));

  }

  public static warning(msg: any): void {

    console.log(orange.bold(`! Warning`));
    console.log(orange(msg));

  }

  public static spinner(msg: any): Spinner {

    let spin = new Spinner({
      text: `%s ${msg}`,
      stream: process.stderr,
    });

    spin.setSpinnerString('|/-\\');

    return spin;

  }

  public static notHandyProject(): void {

    return this.error('Looks like you are not executing this command in handy project directory');

  }

}