import { execSync, exec, spawn } from 'child_process';
import { join } from 'path';
import { readdirSync, ensureDirSync, existsSync, readJSONSync, writeJSONSync, writeFileSync, readFileSync, writeFile } from 'fs-extra';
import { prompt, QuestionCollection } from 'inquirer';
import { Logger } from './logger';
import { get as getRequest } from 'https';
import { stringify } from 'querystring';

import IO from 'socket.io-client';
import { HubAppDbData } from '../commands/deploy/deploy';

let InnerHelper: ConfigHelper;

export class ConfigHelper {

  private _isInHandyProject: boolean;
  private _projectRootDir: string;
  private _projectClientWebAppDir: string;
  private _projectEnv: 'dev' | 'stag' | 'prod';
  private _nodeVersion: string;
  private _nodeMainVersion: number;
  public static hubUrl: string = 'https://handyapps.dev/';

  private _cwd: string = join(process.cwd() + '/');

  public static get isInHandyProject(): boolean {
    return this.getIntance()._isInHandyProject;
  }

  public static get cwd(): string {
    return this.getIntance()._cwd;
  }

  public static get projectRootDir(): string {
    return this.getIntance()._projectRootDir;
  }

  public static get projectClientWebAppDir(): string {
    return this.getIntance()._projectClientWebAppDir;
  }

  public static get projectEnv(): 'dev' | 'stag' | 'prod' {
    return this.getIntance()._projectEnv;
  }

  public static get nodeVersion(): string {
    return this.getIntance()._nodeVersion;
  }

  public static get nodeMainVersion(): number {
    return this.getIntance()._nodeMainVersion;
  }

  public static get magicBoxPath(): string {
    return join(this.projectRootDir, 'magic-box');
  }

  private _lastCheckedPath: string;

  public static get crdFile(): string {
    return join(__dirname, '../../', 'cr.json');
  }

  constructor () {

    this._resolveRoot();
    this._resolveNodeVersion();

  }

  public static getHubCreds(): DeveloperData {

    let creds: any = readJSONSync(ConfigHelper.crdFile);
    return creds;

  }

  public static isLoggedIn(): boolean {

    return ConfigHelper.getHubCreds().loggedIn;

  }

  public static hubLogin() {

    prompt([
      {
        message: 'Email',
        name: 'email',
        type: 'input',
      },
      {
        message: 'Password',
        name: 'password',
        type: 'password',
      },
    ])
      .then(answers => {

        let { email, password } = answers;

        if (email) {
          email = <string>email.toLowerCase().trim();
        }

        getRequest(`${this.hubUrl}api/v1/service/cliAuth/login?${stringify({ email, password })}`, response => {

          let data = '';

          // A chunk of data has been recieved.
          response.on('data', (chunk) => {
            data += chunk;
          });

          // The whole response has been received. Print out the result.
          response.on('end', () => {

            let resResult: any = JSON.parse(data);
            if (!resResult.success) {
              console.error(resResult);
              return;
            }

            let finalData = { act: resResult.data.accessTokenData, rft: resResult.data.refreshTokenData, email, loggedIn: true };
            writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));

            Logger.success('Logged in');
            return;

          });

        }).on('error', reqErr => {

          let finalData = { loggedIn: false };
          writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));

          console.error(reqErr);
          return;

        })

      })
      .catch(err => {

        let finalData = { loggedIn: false };
        writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));

        console.error(err);
        return;

      })

  }

  public static hubLogout() {

    let finalData = { loggedIn: false };
    writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));
    Logger.success('Logged out');

  }

  public static getAccessToken(): Promise<string> {

    if (!ConfigHelper.isLoggedIn()) {
      return Promise.reject('You have to login first');
    }

    let now: number = new Date().getTime() + (1000 * 60 * 60);
    let credsData: DeveloperData = ConfigHelper.getHubCreds();

    let { token, expiryMoment } = credsData.act;

    if (expiryMoment > now) {
      return Promise.resolve(token);
    }

    return ConfigHelper.refreshToken();

  }

  protected static refreshToken(): Promise<string> {

    if (!ConfigHelper.isLoggedIn()) {
      return Promise.reject('You have to login first');
    }

    let now: number = new Date().getTime() + (1000 * 60 * 60);
    let credsData: DeveloperData = ConfigHelper.getHubCreds();
    let { token, expiryMoment } = credsData.rft;

    if (expiryMoment < now) {

      writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
      return Promise.reject('Refresh token expired. Log in again');

    }

    return new Promise((resolve, reject) => {

      const socketInstance: SocketIOClient.Socket = IO(`${ConfigHelper.hubUrl}`);
      socketInstance.once(`cli_token_refresh`, (payload: any) => {

        socketInstance.disconnect()

        if (payload.success) {

          let finalData = { act: payload.accessTokenData, rft: payload.refreshTokenData, loggedIn: true };
          writeFileSync(ConfigHelper.crdFile, JSON.stringify(finalData, null, 2));

          return resolve(payload.accessTokenData.token);

        } else {

          writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
          return reject('Token refreshing error. Log in again');

        }

      })

      socketInstance.once(`cli_token_refresh_failed`, (payload: any) => {

        socketInstance.disconnect()

        writeFileSync(ConfigHelper.crdFile, JSON.stringify({ loggedIn: false }, null, 2));
        return reject('Token refreshing error. Log in again');

      })

      let event = {
        accessToken: null,
        eventData: { email: credsData.email, rft: token }
      };

      socketInstance.emit('cliTokenRefresh', event);

    })

  }

  public static testRefresh(): any {

    ConfigHelper.getAccessToken().then(result => {
      console.log({ result })
    })
      .catch(err => {
        console.log({ err })
      })

  }

  public static getIntance(): ConfigHelper {

    if (!InnerHelper) {
      InnerHelper = new ConfigHelper()
    }

    return InnerHelper;

  }

  public static UcFirst(str: string): string {

    if (!str) {
      return 'undefined';
    }
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  public static LcFirst(str: string): string {
    if (!str) {
      return 'undefined';
    }
    return str.charAt(0).toLowerCase() + str.slice(1);
  }

  public static getFileNameString(str: string): string {

    let result: string = '';
    let chars: string[] = this.LcFirst(str).split('');
    let charsLen: number = chars.length;

    for (let i = 0; i < charsLen; i++) {
      const singleChar = chars[i];

      if (singleChar.toUpperCase() === singleChar) {
        result += `-${singleChar.toLowerCase()}`;
      } else {
        result += singleChar;
      }

    }
    return this.LcFirst(result.replace(/ /g, ''));

  }

  public static projectCliTemplatesPath(): string {

    return join(this.getIntance()._projectRootDir, '/src/cli/templates');

  }

  public static simpleYesNoPrompt(message: string, defaultVal: boolean = true): Promise<boolean> {

    return new Promise((resolve, reject) => {

      prompt({
        message,
        name: 'result',
        type: 'list',
        choices: [
          {
            name: 'Yes',
            value: true
          },
          {
            name: 'No',
            value: false
          }
        ],
        default: defaultVal
      })
        .then(answers => {

          return resolve(answers.result);

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static simpletInputPrompt(message: string, defaultVal?: string): Promise<string> {

    return new Promise((resolve, reject) => {

      prompt({
        message,
        name: 'result',
        type: 'input',
        default: defaultVal
      })
        .then(answers => {

          return resolve(answers.result);

        })
        .catch(err => {

          reject(err);

        })

    })

  }

  public static getPrintableString(value: string = 'unknown'): string {

    value = value.trim();

    let splitted: string[] = value.split(' ');
    let splittedLen: number = splitted.length;

    let result: string = '';

    for (let i = 0; i < splittedLen; i++) {

      result += this.UcFirst(splitted[i]).trim();

    }

    return this.LcFirst(result.replace(/ /g, ''));

  }

  public static generateFolders(foldersPaths: string[]): void {

    let foldersLen: number = foldersPaths.length;
    for (let i = 0; i < foldersLen; i++) {
      ensureDirSync(foldersPaths[i]);
    }

  }

  public static getPredeployData(): DoDeployData {

    let rootPath: string = this.getIntance()._projectRootDir;
    let deployFilePath: string = join(rootPath, 'src/cli/deploy/deploy.config.json');
    let deployData: { handyAppId: number } = (existsSync(deployFilePath)) ? readJSONSync(deployFilePath) : { handyAppId: null };

    let handyConfigFilePath: string = join(rootPath, 'handy.json');
    let handyConfigData: any = readJSONSync(handyConfigFilePath);

    let { domain = {}, serverPort = {}, projectName, superAdminEmail } = handyConfigData;

    let repo: string = execSync(`cd ${join(rootPath)} && git config --get remote.origin.url`).toString('utf-8');

    let repoExtractReg: RegExp = new RegExp(/https:\/\/(.*).git/);
    // @ts-ignore 
    let extract: string = repo.match(repoExtractReg)[1];
    // @ts-ignore 
    repo = `https://${repo.match(repoExtractReg)[1]}.git`;

    if (repo.includes('@') && !extract.includes(':')) {
      let splitted: string[] = repo.split('@');
      splitted.shift();

      repo = `https://${splitted.join('@')}`;

    }

    console.log(repo);

    let branch: string = execSync(`cd ${join(rootPath)} && git branch`).toString('utf-8');
    let branches: string[] = branch.split('\n');

    let branchesLen: number = branches.length;
    for (let i = 0; i < branchesLen; i++) {
      const singleBranch = branches[i];

      if (singleBranch.includes('*')) {
        let splittedBranch: string[] = singleBranch.split(' ');
        branch = splittedBranch[1];
        break;
      }

    }

    let result: DoDeployData = {
      name: projectName.val,
      superAdminEmail: {
        prod: superAdminEmail.val,
        stag: (superAdminEmail.stagVal) ? superAdminEmail.stagVal : superAdminEmail.val,
      },
      handyAppId: deployData.handyAppId,
      domains: {
        prod: domain.val,
        stag: domain.stagVal,
      },
      ports: {
        prod: serverPort.val,
        stag: serverPort.stagVal,
      },
      repo,
      branch
    }

    return result;


  }
  
  public static saveHandyAppId(handyAppId: number): void {

    let rootPath: string = this.getIntance()._projectRootDir;
    let deployFilePath: string = join(rootPath, 'src/cli/deploy/deploy.config.json');

    ensureDirSync(join(rootPath, 'src/cli/deploy/'));

    writeFile(deployFilePath, JSON.stringify({ handyAppId }, null, 2))
    .then(result => {
      Logger.success('Handy app id was saved');
    })
    .catch(err => {
      Logger.error('Handy app id saving failed');
      console.log(err);
    })

  }

  private _resolveRoot(path: string = this._cwd): void {

    if (path === this._lastCheckedPath) {

      this._isInHandyProject = false;
      return;

    }

    this._lastCheckedPath = path;

    let content: string[] = readdirSync(path);
    let contentLen: number = content.length;

    let isRoot: boolean = false;

    for (let i = 0; i < contentLen; i++) {
      const entityName: string = content[i];

      if (entityName === 'handy.json') {

        isRoot = true;

        this._isInHandyProject = true;
        this._projectRootDir = path;
        this._projectClientWebAppDir = join(path, 'src/client/web');

        this._projectEnv = 'dev';

        if (content.includes('prod.flag')) {
          this._projectEnv = 'prod';
          break;
        }

        if (content.includes('stag.flag')) {
          this._projectEnv = 'stag';
          break;
        }

        break;

      }

    }

    if (!isRoot) {
      this._resolveRoot(join(path, '../'));
    }

  }

  private _resolveNodeVersion(): void {

    let version: string = execSync('node -v').toString();
    this._nodeVersion = version;
    this._nodeMainVersion = parseInt(version.split('.')[0].replace('v', ''));

  }

  public static updatePorts(stag: number, prod: number): void {

    Logger.info('Updating app ports');

    let handyJsonPath: string = join(this.projectRootDir, 'handy.json');
    let configData: any = readJSONSync(handyJsonPath);

    configData.serverPort.val = prod;
    configData.serverPort.stagVal = stag;

    writeFileSync(handyJsonPath, JSON.stringify(configData, null, 2));
    return;

  }

  public static updateNginxConf(appData: HubAppDbData): void {

    Logger.info('Updating Nginx conf files');

    let stagConfPath: string = join(this.projectRootDir, 'stag.nginx.conf');
    let prodConfPath: string = join(this.projectRootDir, 'prod.nginx.conf');

    let stagConf: string = readFileSync(stagConfPath, { encoding: 'utf-8' });
    let prodConf: string = readFileSync(prodConfPath, { encoding: 'utf-8' });

    let handyJsonPath: string = join(this.projectRootDir, 'handy.json');
    let maxUploadData: any = readJSONSync(handyJsonPath).fileUpload;

    let prodMaxUpload: string = maxUploadData.val.maxFileSizeInMB;
    let stagMaxUpload: string = maxUploadData.val.maxFileSizeInMB;

    if (maxUploadData.stagVal && maxUploadData.stagVal.maxFileSizeInMB) {
      stagMaxUpload = maxUploadData.stagVal.maxFileSizeInMB;
    }

    let { stagDomain, prodDomain, stagPort, prodPort } = appData;
    let domainReplacerReg: RegExp = new RegExp(/server_name(.*);#domain/);
    let portReplacerReg: RegExp = new RegExp(/localhost:(.*);#port/);
    let uploadReplacerReg: RegExp = new RegExp(/client_max_body_size(.*);#maxUpload/);
    let wwwDomainReplacerReg: RegExp = new RegExp(/server_name(.*);#wwwDomain/);
    let wwwSchemeReplacerReg: RegExp = new RegExp(/\$scheme(.*);#domainScheme/);

    stagConf = stagConf.replace(domainReplacerReg, `server_name ${stagDomain};#domain`);
    stagConf = stagConf.replace(portReplacerReg, `localhost:${stagPort};#port`);
    stagConf = stagConf.replace(uploadReplacerReg, `client_max_body_size ${stagMaxUpload}M;#maxUpload`);
    stagConf = stagConf.replace(wwwDomainReplacerReg, `server_name www.${stagDomain};#wwwDomain`);
    stagConf = stagConf.replace(wwwSchemeReplacerReg, `$scheme://${stagDomain}$request_uri;#domainScheme`);
    writeFileSync(stagConfPath, stagConf, { encoding: 'utf-8' });

    prodConf = prodConf.replace(domainReplacerReg, `server_name ${prodDomain};#domain`);
    prodConf = prodConf.replace(portReplacerReg, `localhost:${prodPort};#port`);
    prodConf = prodConf.replace(uploadReplacerReg, `client_max_body_size ${prodMaxUpload}M;#maxUpload`);
    prodConf = prodConf.replace(wwwDomainReplacerReg, `server_name www.${prodDomain};#wwwDomain`);
    prodConf = prodConf.replace(wwwSchemeReplacerReg, `$scheme://${prodDomain}$request_uri;#domainScheme`);
    writeFileSync(prodConfPath, prodConf, { encoding: 'utf-8' });
    return;

  }
  
  public static addRepoToJsons(repo: string): void {

    Logger.info('Parsing repository');

    let handyJsonPath: string = join(this.projectRootDir, 'handy.json');
    let handyData: any = readJSONSync(handyJsonPath);

    let credsReg: RegExp = new RegExp(/https(.*)@/);
    if (repo.includes('@')) {
      repo = `https://${repo.replace(credsReg, '')}`;
    }

    handyData.repo = {
      val: repo
    }

    writeFileSync(handyJsonPath, JSON.stringify(handyData, null, 2));

    let packageJsonPath: string = join(this.projectRootDir, 'package.json');
    let packageJsonData: any = readJSONSync(packageJsonPath);

    packageJsonData.repository = {
      type: 'git',
      url: `git+${repo}`
    }

    writeFileSync(packageJsonPath, JSON.stringify(packageJsonData, null, 2));
    return;

  }

  public static buildApp(): Promise<void> {

    // return Promise.resolve();
    return new Promise((resolve, reject) => {

      Logger.info('Building app, might take a while ;)');

      let child = exec(`cd ${this.projectRootDir} && npm run build`);
      
      // @ts-ignore
      child.stdout.on('data', data => {
        console.log(data.toString());
      });
      // @ts-ignore
      child.stderr.on('data', data => {
        console.log(data.toString());
      });

      // @ts-ignore
      child.on('exit', code => {
        return resolve();
      });

    })


  }
  
  public static pushRepo(): Promise<void> {

    return new Promise((resolve, reject) => {

      Logger.info('Pushing repo');

      let child = exec(`cd ${this.projectRootDir} && git add . && git commit -m "Pre deploy" && git push`);
      
      // @ts-ignore
      child.stdout.on('data', data => {
        console.log(data.toString());
      });
      // @ts-ignore
      child.stderr.on('data', data => {
        console.log(data.toString());
      });

      // @ts-ignore
      child.on('exit', code => {
        return resolve();
      });

    })


  }

}

export interface DoDeployData {

  name: string,
  domains?: {
    prod?: string,
    stag?: string,
  },
  ports: {
    prod: number,
    stag: number,
  },
  superAdminEmail: {
    prod: number,
    stag: number,
  }
  handyAppId: number,
  repo?: string,
  branch?: string

}

export interface DeveloperData {
  loggedIn: boolean,
  email: string,
  act: {
    token: string,
    expiryMoment: number
  },
  rft: {
    token: string,
    expiryMoment: number
  }
}

// let { name, repo, handyAppId, stagPort, prodPort } = appData;