import { registerHelper } from 'handlebars';
import { ConfigHelper } from './config';
import { fieldValueRenderer, fieldFormControlRenderer } from './form.field.hbs.helper';

registerHelper('LcFirst', (value: string) => {
  return ConfigHelper.LcFirst(value);
});

registerHelper('UcFirst', (value: string) => {
  return ConfigHelper.UcFirst(value);
});

registerHelper('LcAll', (value: string) => {
  return value.toLowerCase()
});

registerHelper('filePath', (value: string) => {
  return ConfigHelper.getFileNameString(value);
});

registerHelper('moduleTableActions', (value: any, type: 'page' | 'modal') => {

  let result: string = '';

  if (value.name !== 'actions') {

    result += `<mat-header-cell *matHeaderCellDef cdkDrag cdkDragLockAxis="x" mat-sort-header><span *cdkDragPreview><basic-btn icon="swap_horiz">${value.publicName}</basic-btn></span>${value.publicName}</mat-header-cell>\n\n`;

    result += `          <mat-cell *matCellDef="let element">{{element.${value.name}}}</mat-cell>`;

  } else {

    result += `<mat-header-cell *matHeaderCellDef class="crud-table-actions-col" cdkDrag cdkDragLockAxis="x" mat-sort-header><span *cdkDragPreview><basic-btn icon="swap_horiz">${value.publicName}</basic-btn></span>${value.publicName}</mat-header-cell>\n\n`;

    result += `          <mat-cell class="crud-table-actions-col" *matCellDef="let element">\n\n`;

    result += `            <div fxLayout="row" fxLayoutAlign="flex-end center" fxLayoutGap="8px">\n`;
    
    result += `              <stroked-btn icon="edit" color="primary" ${(type === 'page') ? '[routerLink]="[\'./edit/\' + element._id]"' : '(click)="editOrCreateAction(element._id)"'}>Edit</stroked-btn>\n`;
    result += `              <stroked-btn icon="delete_otl" color="warn" (confirmClick)="removeEntryAction(element._id)">Remove</stroked-btn>\n\n`;

    result += `            </div>\n\n`;

    result += `          </mat-cell>`;
  }

  return result;

});

registerHelper('TableFilterDataObjDestroy', (value: { name: string }[]) => {
  
  let result = '';

  let valLen: number = value.length;
  for (let i = 0; i < valLen; i++) {
    const filterName = value[i].name;
    result += ` ${filterName},`;
  }

  if (result !== '') {
    result = `let {${result} } = filterData;`;
  }

  return result;

});

registerHelper('TableMarkupElement', (value: string) => {
  
  return `{{element.${value}}}`;

});

registerHelper('TableFiltersToggleBtn', (value: string) => {
  
  return `<stroked-btn #filtersToggle color="primary" icon="filter_list">{{ (filtersToggleState) ? 'Hide' : 'Show'}} filters</stroked-btn>  `;

});

registerHelper('TableMarkupElement', (value: string) => {
  
  return `{{element.${value}}}`;

});

registerHelper('FormHTMLField', (value: any, formName) => {
  return fieldValueRenderer(value, formName);
});

registerHelper('FormFieldControl', (value: any) => {
  return fieldFormControlRenderer(value);
});
